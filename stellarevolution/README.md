# Stellar evolution test

The following code setup is intended to put to test a Stellar evolution module. Verifications
of primary interest are the conservation of mass, momentum and energy (kinetic plus thermal)
in the entire simulation box. However, we are also interested in verifying that the mass lost
by star particles is in accordance with what is predicted by stellar evolution tables, and in
verifying the chemical enrichment of gas particles as well.

Consequently, the setup consists of 5 star particles randomly distributed in the simulation box,
which are surrounded by gas particles positioned at the vertices of a regular mesh, with 5
particles per dimension, amounting to 125 gas particles in total. Small random initial velocities
are given to all particles, and to the gas particles a fixed value of initial thermal energy
as well. All gas particles have zero metal content initially.

Furthermore, the energy input from unresolved physics (e.g., ejecta kinetic energy and SNIa
thermal energy injection) is turned OFF so that we can indeed assess energy conservation in the
resolved physics.

Gravity is turned OFF and the simulations run from t = 0 to t ~ 15 Gyrs.


## Part 1: Conservation properties and distribution of mass

In this part, two simulations are run. The setup is the same in both with the exception of the
parameter bChemEnrich, which sets whether or not chemical enrichment will take place in the
simulation, with the resulting mass, momentum and kinetic energy loss in star particles, and
the corresponding changes in gas particles. Total mass, momentum and energy (kinetic plus
thermal) conservation is assessed by plotting the fractional difference of each with respect to
their values at t = 0, for all available snapshots.

![totalMass](imgs/totalMass_ref.png "Total mass conservation")

![totalMomentum](imgs/totalMom_ref.png "Total momentum conservation")

![totalEnergy](imgs/totalEnergy_ref.png "Total energy conservation")



Moreover, we check that the mass loss (gain) in star (gas) particles is within reason. For gas
particles, additionally, we check the abundances of the chemical species tracked in the code.
This is assessed by plotting the median of these quantities for all star/gas particles, along
with the range spanned by their 8th to 92th percentiles as a shaded region, for all available
snapshots. Note that different star particles lose different amounts of mass due to their
different initial metallicities, which for this part of the test are drawn randomly from a
uniform distribution in the range [0.0,0.05].

![starMass](imgs/starMass_ref.png "Star particles' mass")

![gasMass](imgs/gasMass_ref.png "Gas particles' mass")


![abunHydrogen](imgs/abunHydrogen_ref.png "Hydrogen abundance")

![abunHelium](imgs/abunHelium_ref.png "Helium abundance")

![abunCarbon](imgs/abunCarbon_ref.png "Carbon abundance")

![abunNitrogen](imgs/abunNitrogen_ref.png "Nitrogen abundance")

![abunOxygen](imgs/abunOxygen_ref.png "Oxygen abundance")

![abunNeon](imgs/abunNeon_ref.png "Neon abundance")

![abunMagnesium](imgs/abunMagnesium_ref.png "Magnesium abundance")

![abunSilicon](imgs/abunSilicon_ref.png "Silicon abundance")

![abunIron](imgs/abunIron_ref.png "Iron abundance")

![metallicity](imgs/metallicity_ref.png "Metallicity")



## Part 2: Individual species and total metal ejecta

In this part we check that the code's calculation of the ejecta is being done correctly. To
achieve this, we run simulations with the same setup as before, but we set all star particles'
initial metallicities to the same value. In this way, by looking at the enrichment of all gas
particles, we can compute what each star particle has ejected. We do this for metallicities in
the set {0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.05}, and compare the results with what is
obtained from the python implementation in the [StellarYields repo](https://bitbucket.org/iacsimgro/stellaryields), for the same set of input
parameters and stellar evolution tables. Plots show results from the code as a solid line and
results from the StellarYields repo as a dotted line.

![ejectaHydrogen](imgs/ejectaHydrogen_ref.png "Hydrogen ejecta")

![ejectaHelium](imgs/ejectaHelium_ref.png "Helium ejecta")

![ejectaCarbon](imgs/ejectaCarbon_ref.png "Carbon ejecta")

![ejectaNitrogen](imgs/ejectaNitrogen_ref.png "Nitrogen ejecta")

![ejectaOxygen](imgs/ejectaOxygen_ref.png "Oxygen ejecta")

![ejectaNeon](imgs/ejectaNeon_ref.png "Neon ejecta")

![ejectaMagnesium](imgs/ejectaMagnesium_ref.png "Magnesium ejecta")

![ejectaSilicon](imgs/ejectaSilicon_ref.png "Silicon ejecta")

![ejectaIron](imgs/ejectaIron_ref.png "Iron ejecta")

![ejectaMetals](imgs/ejectaMetals_ref.png "Total metal ejecta")

