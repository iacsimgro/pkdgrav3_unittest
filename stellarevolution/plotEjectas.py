def run(snapString, tablePath, savePath, ident):
    import numpy as np
    import h5py
    import matplotlib.pyplot as plt
    from glob import glob
    from stellaryields.stev import Standard

    elemNames = ['Hydrogen', 'Helium', 'Carbon', 'Nitrogen', 'Oxygen', 'Neon',
                 'Magnesium', 'Silicon', 'Iron']

    # Should be equal to the array at testEjecta.sh
    Z  = np.array(["0.001","0.002","0.004","0.008","0.016","0.032","0.050"])
    nZ = Z.size


    # Read snapshots
    paths = glob(snapString+"/snaps_Z" + Z[0] + '_' + ident + "/STEVTest.*.0")
    nSnap = len(paths)

    snap  = h5py.File(paths[0], mode='r')
    nGas  = snap['PartType0/Masses'].size
    nStar = snap['PartType4/Masses'].size
    nElem = snap['PartType0/Abundances'].shape[1]
    timeInGyr = snap['Units'].attrs.get('SecUnit') / 31557600.0 / 1e9
    massInMsol = snap['Units'].attrs.get('MsolUnit')
    starInitMass = snap['PartType4/Masses'][0]
    starBirthTime = snap['PartType4/StellarFormationTime'][0]
    snap.close()

    if nElem != len(elemNames): raise Exception('Incorrect number of elements')

    time        = np.empty((nZ, nSnap))
    elemEjecta  = np.empty((nZ, nSnap, nElem))
    metalEjecta = np.empty((nZ, nSnap))
    starAbun    = np.empty((nZ, nElem))
    starZ       = np.asarray(Z, dtype='float')
    for i in range(nZ):
        paths = sorted(glob(snapString+"/snaps_Z" + Z[i] + '_' + ident + "/STEVTest.*.0"))

        snap = h5py.File(paths[0], mode='r')

        gasMass           = snap['PartType0/Masses'][...]
        gasInitHydrogMass = (snap['PartType0/Abundances'][:,0] * gasMass).sum()
        gasInitHeliumMass = (snap['PartType0/Abundances'][:,1] * gasMass).sum()
        starAbun[i,:]     = snap['PartType4/Abundances'][0,:]

        snap.close()

        for j in range(nSnap):
            snap = h5py.File(paths[j], mode='r')

            time[i,j]         = snap['Header'].attrs.get('Time')
            gasMass           = snap['PartType0/Masses'][...]
            elemEjecta[i,j,:] = (gasMass * snap['PartType0/Abundances'][...].T).sum(axis=1)
            metalEjecta[i,j]  = (gasMass * snap['PartType0/Metallicity'][...]).sum()

            snap.close()

        # To get the ejecta we need to substract the initially present elements
        elemEjecta[i,:,0] -= gasInitHydrogMass
        elemEjecta[i,:,1] -= gasInitHeliumMass

    # Convert units
    time *= timeInGyr
    starBirthTime *= timeInGyr

    elemEjecta   *= massInMsol
    metalEjecta  *= massInMsol
    starInitMass *= massInMsol

    # Convert to ejecta per stellar particle
    elemEjecta  /= nStar
    metalEjecta /= nStar


    # Verify ejecta using the StellarYields repo
    stevModel  = Standard(tablePath)
    stevEjecta = np.empty((nZ, nSnap, nElem + 1))
    for i in range(nZ):
        stevModel.ComputeEjecta(time[i] * 1e9, starZ[i], starAbun[i,:],
                                starBirthTime * 1e9, starInitMass)
        stevEjecta[i,:,:] = stevModel.EjectaCCSN + stevModel.EjectaAGB + stevModel.EjectaSNIa
    stevEjecta = np.cumsum(stevEjecta, axis=1)


    # Plotting
    legendSize = 'xx-large'
    labelSize = 'x-large'

    fig = plt.figure(figsize=(6,4), dpi=300)
    ax  = fig.add_axes([0,0,1,1])
    ax.grid(which='both', axis='both')
    ax.set_yscale('log')
    ax.set_xlim(0,np.ceil(time[0,-1]))
    ax.set_xlabel('Time [Gyrs]', size=labelSize)
    ax.set_ylabel('Cumulative ejecta [M$_\odot$]', size=labelSize)

    colors = plt.cm.jet(np.linspace(0, 1, nZ))
    ylims = [(1e-1,4e-1),    # Hydrogen
             (7e-2,3e-1),    # Helium
             (2e-3,2e-2),    # Carbon
             (5e-4,5e-3),    # Nitrogen
             (1e-2,3e-2),    # Oxygen
             (1e-3,6e-3),    # Neon
             (5e-4,2e-3),    # Magnesium
             (1e-3,4e-3),    # Silicon
             (1e-3,5e-3),    # Iron
             (2e-2,6e-2)]    # Metals

    ax.set_ylim(ylims[-1])
    for i in range(nZ):
        ax.plot(time[i], metalEjecta[i], c=colors[i], label=Z[i], alpha=0.75)
        ax.plot(time[i], stevEjecta[i,:,-1], c=colors[i], ls=':')
    ax.legend(title='Metals', title_fontsize=legendSize)
    fig.savefig(savePath + '/ejectaMetals_'+ident+'.png', bbox_inches='tight')
    for line in ax.get_lines(): line.remove()
    ax.get_legend().remove()

    for j in range(nElem):
        ax.set_ylim(ylims[j])
        for i in range(nZ):
            ax.plot(time[i], elemEjecta[i,:,j], c=colors[i], label=Z[i], alpha=0.75)
            ax.plot(time[i], stevEjecta[i,:,j], c=colors[i], ls=':')
        ax.legend(title=elemNames[j], title_fontsize=legendSize)
        fig.savefig(savePath + '/ejecta' + elemNames[j] + '_' + ident + '.png', bbox_inches='tight')
        for line in ax.get_lines(): line.remove()
        ax.get_legend().remove()
