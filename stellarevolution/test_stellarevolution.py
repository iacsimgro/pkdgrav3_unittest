from pkdtest import PKDGRAV3TestCase


class StellarEvolutionTest(PKDGRAV3TestCase):
    title = "Stellar yields"
    description = ("Test for the correct evolution of ejecta and abundances over cosmic time."
                    "More details can be found at README.md")
    plots = [ {"file":"imgs/totalMass",
               "caption":"Total mass conservation"},
              {"file":"imgs/totalMom",
               "caption":"Total momentum conservation"},
              {"file":"imgs/totalEnergy",
               "caption":"Total energy conservation"},
              {"file":"imgs/starMass",
               "caption":"Stellar mass evolution"},
              {"file":"imgs/gasMass",
               "caption":"Gas mass evolution"},
              {"file":"imgs/abunHydrogen",
               "caption":"Hydrogen abundance evolution"},
              {"file":"imgs/abunHelium",
               "caption":"Helium abundance evolution"},
              {"file":"imgs/abunCarbon",
               "caption":"Carbon abundance evolution"},
              {"file":"imgs/abunNitrogen",
               "caption":"Nitrogen abundance evolution"},
              {"file":"imgs/abunOxygen",
               "caption":"Oxygen abundance evolution"},
              {"file":"imgs/abunNeon",
               "caption":"Neon abundance evolution"},
              {"file":"imgs/abunMagnesium",
               "caption":"Magnesium abundance evolution"},
              {"file":"imgs/abunSilicon",
               "caption":"Silicon abundance evolution"},
              {"file":"imgs/abunIron",
               "caption":"Iron abundance evolution"},
              {"file":"imgs/metallicity",
               "caption":"Metallcity evolution"},
              {"file":"imgs/ejectaHydrogen",
               "caption":"Hydrogen ejecta for different metallicites"},
              {"file":"imgs/ejectaHelium",
               "caption":"Helium ejecta for different metallicites"},
              {"file":"imgs/ejectaCarbon",
               "caption":"Carbon ejecta for different metallicites"},
              {"file":"imgs/ejectaNitrogen",
               "caption":"Nitrogen ejecta for different metallicites"},
              {"file":"imgs/ejectaOxygen",
               "caption":"Oxygen ejecta for different metallicites"},
              {"file":"imgs/ejectaNeon",
               "caption":"Neon ejecta for different metallicites"},
              {"file":"imgs/ejectaMagnesium",
               "caption":"Magnesium ejecta for different metallicites"},
              {"file":"imgs/ejectaSilicon",
               "caption":"Silicon ejecta for different metallicites"},
              {"file":"imgs/ejectaIron",
               "caption":"Iron ejecta for different metallicites"},
              {"file":"imgs/ejectaMetals",
               "caption":"Total metal ejecta for different metallicities"}]

    Zs= [0.001,0.002,0.004,0.008,0.016,0.032,0.05]
    schemes = ["MFM"]
    __name__ = "StellarEvolution"
    folder_name = "stellarevolution"

    max_threads = 4
    compile_flags = {'STELLAR_EVOLUTION':True}

    def generateIC(self,z=None):
        import numpy as np
        from pykdgrav3_utils.hdf5 import save

        random_state = np.random.RandomState(seed=12345)

        nElem      = 9
        nStar      = 5

        nGasPerDim = 5
        nGas       = nGasPerDim**3

        gasPos      = np.empty((nGas, 3))
        x           = np.linspace(-0.5, 0.5, num=nGasPerDim, endpoint=False)
        mesh        = np.meshgrid(x, x, x)
        gasPos[:,0] = mesh[0].flatten()
        gasPos[:,1] = mesh[1].flatten()
        gasPos[:,2] = mesh[2].flatten()

        gasMass = np.full(nGas, 1.0)
        gasVel  = (random_state.random((nGas, 3)) - 0.5) * 4e-3
        gasUint = np.full(nGas, 1e-2)


        starMass = np.full(nStar, 1.0)
        starPos  = random_state.random((nStar, 3)) - 0.5
        starVel  = (random_state.random((nStar, 3)) - 0.5) * 4e-3

        if z is not None:
            starZ = np.full(nStar, z)
        else:
            starZ = random_state.random(nStar) * 0.05

        starAbun = np.zeros((nStar, nElem))
        refZ     = 0.0127
        for i in range(nStar):
            starAbun[i,1] = 0.23 + 2.25 * starZ[i]
            starAbun[i,0] = 1.0 - starAbun[i,1] - starZ[i]

            f = starZ[i] / refZ
            starAbun[i,2] = 2.07e-3 * f    # C
            starAbun[i,3] = 8.36e-4 * f    # N
            starAbun[i,4] = 5.49e-3 * f    # O
            starAbun[i,5] = 1.41e-3 * f    # Ne
            starAbun[i,6] = 5.91e-4 * f    # Mg
            starAbun[i,7] = 6.83e-4 * f    # Si
            starAbun[i,8] = 1.10e-3 * f    # Fe

        dataDict = {
            'PartType0':{'Masses':gasMass,
                         'Coordinates':gasPos,
                         'Velocities':gasVel,
                         'InternalEnergy':gasUint},
            'PartType4':{'Masses':starMass,
                         'Coordinates':starPos,
                         'Velocities':starVel,
                         'Abundances':starAbun,
                         'Metallicity':starZ}
            }

        save(dataDict, self.input_file)


    def test_stellarevolution(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testEjecta.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()
        
        self.set_scheme('MFM')
        self.compile()
        
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)

        # Reference run without chemical enrichement
        self.output_file = self.output_dir + '/STEVTest_ref'
        self.test_args = '-bChemEnrich'
        self.run_pkdgrav3()


        # Now we enrich
        self.output_file = self.output_dir + '/STEVTest_cmp'
        self.test_args = '+bChemEnrich'
        self.run_pkdgrav3()

        for z in self.Zs:
            self.generateIC(z=z)
            self.output_dir = self.pkdgrav3_test_dir + '/snaps_Z%.3f_%s'%(z,self.identifier)
            self.output_file = self.output_dir + '/STEVTest'
            self.run_pkdgrav3()

        self.plot()


    def plot(self):
        import pathlib
        pathlib.Path(self.pkdgrav3_test_dir+"/imgs").mkdir(exist_ok=True)

        from stellarevolution.plotChecks import run
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        run(self.output_dir+"/STEVTest_ref.*.0",            \
            self.output_dir+"/STEVTest_cmp.*.0",            \
            self.pkdgrav3_test_dir+"/imgs", self.identifier)


        from stellarevolution.plotEjectas import run
        run(self.pkdgrav3_test_dir, self.stev_dir, self.pkdgrav3_test_dir+"/imgs", self.identifier)
