def run(refGlob, cmpGlob, savePath, ident):
    import numpy as np
    import h5py
    import matplotlib.pyplot as plt
    from glob import glob

    refPaths = sorted(glob(refGlob))
    cmpPaths = sorted(glob(cmpGlob))
    nSnap    = len(refPaths)

    elemNames = ['Hydrogen', 'Helium', 'Carbon', 'Nitrogen', 'Oxygen', 'Neon',
                 'Magnesium', 'Silicon', 'Iron']


    # Read snapshots
    ref   = h5py.File(refPaths[0], mode='r')
    nGas  = ref['PartType0/Masses'].size
    nStar = ref['PartType4/Masses'].size
    nElem = ref['PartType0/Abundances'].shape[1]
    timeInGyr = ref['Units'].attrs.get('SecUnit') / 31557600.0 / 1e9
    ref.close()

    if nElem != len(elemNames): raise Exception('Incorrect number of elements')

    time         = np.empty(nSnap)

    refStarMass  = np.empty((nStar, nSnap))
    refStarVel   = np.empty((nStar, nSnap, 3))
    refGasMass   = np.empty((nGas, nSnap))
    refGasVel    = np.empty((nGas, nSnap, 3))
    refGasUint   = np.empty((nGas, nSnap))

    cmpStarMass  = np.empty((nStar, nSnap))
    cmpStarVel   = np.empty((nStar, nSnap, 3))
    cmpStarMetal = np.empty((nStar, nSnap))
    cmpGasMass   = np.empty((nGas, nSnap))
    cmpGasVel    = np.empty((nGas, nSnap, 3))
    cmpGasUint   = np.empty((nGas, nSnap))
    cmpGasAbun   = np.empty((nGas, nSnap, nElem))
    cmpGasMetal  = np.empty((nGas, nSnap))

    for i in range(nSnap):
        ref = h5py.File(refPaths[i], mode='r')

        time[i]           = ref['Header'].attrs.get('Time')
        refStarMass[:,i]  = ref['PartType4/Masses']
        refStarVel[:,i,:] = ref['PartType4/Velocities']
        refGasMass[:,i]   = ref['PartType0/Masses']
        refGasVel[:,i,:]  = ref['PartType0/Velocities']
        refGasUint[:,i]   = ref['PartType0/InternalEnergy']

        ref.close()


        cmp = h5py.File(cmpPaths[i], mode='r')

        cmpStarMass[:,i]  = cmp['PartType4/Masses']
        cmpStarVel[:,i,:] = cmp['PartType4/Velocities']
        cmpStarMetal[:,i] = cmp['PartType4/Metallicity']
        cmpGasMass[:,i]   = cmp['PartType0/Masses']
        cmpGasVel[:,i,:]  = cmp['PartType0/Velocities']
        cmpGasUint[:,i]   = cmp['PartType0/InternalEnergy']
        cmpGasAbun[:,i,:] = cmp['PartType0/Abundances']
        cmpGasMetal[:,i]  = cmp['PartType0/Metallicity']

        cmp.close()

    time *= timeInGyr
    refGasUint *= refGasMass
    cmpGasUint *= cmpGasMass

    gasInitialMass  = refGasMass[0,0]
    starInitialMass = refStarMass[0,0]


    # Quantities that should be conserved
    # Total mass
    refTotalMass = np.sum(refStarMass, axis=0) + np.sum(refGasMass, axis=0)
    cmpTotalMass = np.sum(cmpStarMass, axis=0) + np.sum(cmpGasMass, axis=0)

    # Total momentum
    refTotalMom = np.sum(refStarMass[:,:,None] * refStarVel, axis=0) + \
                  np.sum(refGasMass[:,:,None] * refGasVel, axis=0)
    refTotalMom = np.sqrt(refTotalMom[:,0]**2 + refTotalMom[:,1]**2 + refTotalMom[:,2]**2)
    cmpTotalMom = np.sum(cmpStarMass[:,:,None] * cmpStarVel, axis=0) + \
                  np.sum(cmpGasMass[:,:,None] * cmpGasVel, axis=0)
    cmpTotalMom = np.sqrt(cmpTotalMom[:,0]**2 + cmpTotalMom[:,1]**2 + cmpTotalMom[:,2]**2)

    # Total thermal/internal energy
    refTotalUint = np.sum(refGasUint, axis=0)
    cmpTotalUint = np.sum(cmpGasUint, axis=0)

    # Total kinetic energy
    refStarVelSq = refStarVel[:,:,0]**2 + refStarVel[:,:,1]**2 + refStarVel[:,:,2]**2
    refGasVelSq  = refGasVel[:,:,0]**2 + refGasVel[:,:,1]**2 + refGasVel[:,:,2]**2
    refTotalEkin = np.sum(0.5 * refStarMass * refStarVelSq, axis=0) + \
                   np.sum(0.5 * refGasMass * refGasVelSq, axis=0)
    cmpStarVelSq = cmpStarVel[:,:,0]**2 + cmpStarVel[:,:,1]**2 + cmpStarVel[:,:,2]**2
    cmpGasVelSq  = cmpGasVel[:,:,0]**2 + cmpGasVel[:,:,1]**2 + cmpGasVel[:,:,2]**2
    cmpTotalEkin = np.sum(0.5 * cmpStarMass * cmpStarVelSq, axis=0) + \
                   np.sum(0.5 * cmpGasMass * cmpGasVelSq, axis=0)

    # Total energy (kinetic + thermal)
    refTotalE = refTotalUint + refTotalEkin
    cmpTotalE = cmpTotalUint + cmpTotalEkin


    # Distribution of mass and abundances in the particles
    qLower, qUpper = 0.08, 0.92

    cmpStarMassMedian = np.median(cmpStarMass, axis=0)
    cmpStarMassLower  = np.quantile(cmpStarMass, qLower, axis=0)
    cmpStarMassUpper  = np.quantile(cmpStarMass, qUpper, axis=0)

    cmpGasMassMedian = np.median(cmpGasMass, axis=0)
    cmpGasMassLower  = np.quantile(cmpGasMass, qLower, axis=0)
    cmpGasMassUpper  = np.quantile(cmpGasMass, qUpper, axis=0)

    cmpGasAbunMedian = np.median(cmpGasAbun, axis=0)
    cmpGasAbunLower  = np.quantile(cmpGasAbun, qLower, axis=0)
    cmpGasAbunUpper  = np.quantile(cmpGasAbun, qUpper, axis=0)

    cmpGasMetalMedian = np.median(cmpGasMetal, axis=0)
    cmpGasMetalLower  = np.quantile(cmpGasMetal, qLower, axis=0)
    cmpGasMetalUpper  = np.quantile(cmpGasMetal, qUpper, axis=0)


    # Plotting
    legendSize = 'xx-large'
    labelSize = 'x-large'
    refLabel = 'bChemEnrich OFF'
    cmpLabel = 'bChemEnrich ON'

    fig = plt.figure(figsize=(6,4), dpi=300)
    ax  = fig.add_axes([0,0,1,1])

    ax.grid(which='both', axis='both')
    ax.set_yscale('log')
    ax.set_xlabel('Time [Gyrs]', size=labelSize)
    ax.set_xlim(0,np.ceil(time[-1]))


    # plt.rcParams['axes.prop_cycle'].by_key()['color']
    ax.set_prop_cycle(color=['#1f77b4', '#ff7f0e'])
    ax.set_ylabel('Fractional difference', size=labelSize)

    ax.plot(time, np.abs(refTotalMass - refTotalMass[0]) / refTotalMass[0], label=refLabel)
    ax.plot(time, np.abs(cmpTotalMass - cmpTotalMass[0]) / cmpTotalMass[0], label=cmpLabel)
    ax.legend(title='Total mass', title_fontsize=legendSize)
    fig.savefig(savePath + '/totalMass_'+ident+'.png', bbox_inches='tight')

    for line in ax.get_lines(): line.remove()
    ax.get_legend().remove()
    ax.plot(time, np.abs(refTotalMom - refTotalMom[0]) / refTotalMom[0], label=refLabel)
    ax.plot(time, np.abs(cmpTotalMom - cmpTotalMom[0]) / cmpTotalMom[0], label=cmpLabel)
    ax.legend(title='Total momentum', title_fontsize=legendSize)
    fig.savefig(savePath + '/totalMom_'+ident+'.png', bbox_inches='tight')

    for line in ax.get_lines(): line.remove()
    ax.get_legend().remove()
    ax.plot(time, np.abs(refTotalE - refTotalE[0]) / refTotalE[0], label=refLabel)
    ax.plot(time, np.abs(cmpTotalE - cmpTotalE[0]) / cmpTotalE[0], label=cmpLabel)
    ax.legend(title='Total energy (kinetic + thermal)', title_fontsize=legendSize)
    fig.savefig(savePath + '/totalEnergy_'+ident+'.png', bbox_inches='tight')


    ax.set_prop_cycle(color=['#1f77b4'])
    ax.set_ylabel('Median mass', size=labelSize)

    for line in ax.get_lines(): line.remove()
    ax.get_legend().remove()
    ax.set_ylim(0.5 * starInitialMass, starInitialMass)
    ax.plot(time, cmpStarMassMedian)
    coll = ax.fill_between(time, cmpStarMassUpper, cmpStarMassLower, alpha=0.25)
    ax.legend([], [], title='Star particles', title_fontsize=legendSize)
    fig.savefig(savePath + '/starMass_'+ident+'.png', bbox_inches='tight')
    coll.remove()

    for line in ax.get_lines(): line.remove()
    ax.get_legend().remove()
    ax.set_ylim(0.9 * gasInitialMass, 1.2 * gasInitialMass)
    ax.plot(time, cmpGasMassMedian)
    coll = ax.fill_between(time, cmpGasMassUpper, cmpGasMassLower, alpha=0.25)
    ax.legend([], [], title='Gas particles', title_fontsize=legendSize)
    fig.savefig(savePath + '/gasMass_'+ident+'.png', bbox_inches='tight')
    coll.remove()


    ylims = [(6e-1,8e-1),    # Hydrogen
             (2e-1,4e-1),    # Helium
             (1e-8,1e-2),    # Carbon
             (1e-8,1e-3),    # Nitrogen
             (1e-8,1e-2),    # Oxygen
             (1e-8,1e-3),    # Neon
             (1e-8,1e-3),    # Magnesium
             (1e-8,1e-3),    # Silicon
             (1e-8,1e-3),    # Iron
             (1e-8,1e-2)]    # Metals

    ax.set_ylabel('Median abundance', size=labelSize)
    for i, elem in enumerate(elemNames):
        for line in ax.get_lines(): line.remove()
        ax.get_legend().remove()
        ax.set_ylim(ylims[i])
        ax.plot(time, cmpGasAbunMedian[:,i])
        coll = ax.fill_between(time, cmpGasAbunUpper[:,i], cmpGasAbunLower[:,i], alpha=0.25)
        ax.legend([], [], title=elem, title_fontsize=legendSize)
        fig.savefig(savePath + '/abun' + elem + '_' + ident + '.png', bbox_inches='tight')
        coll.remove()

    for line in ax.get_lines(): line.remove()
    ax.get_legend().remove()
    ax.set_ylim(ylims[-1])
    ax.plot(time, cmpGasMetalMedian)
    coll = ax.fill_between(time, cmpGasMetalUpper, cmpGasMetalLower, alpha=0.25)
    ax.legend([], [], title='Metallicity', title_fontsize=legendSize)
    fig.savefig(savePath + '/metallicity_'+ident+'.png', bbox_inches='tight')
    coll.remove()
