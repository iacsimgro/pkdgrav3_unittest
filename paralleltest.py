import unittest, asyncio, concurrent, sys


def _isnotsuite(test):
    "A crude way to tell apart testcases and suites with duck-typing"
    try:
      iter(test)
    except TypeError:
      return True
    return False


class ParallelTextTestResult(unittest.TestResult):
    """A test result class that can print formatted text results to a stream.
    Used by TextTestRunner.

    Basically just copied and modified to not print anything at start, but rather
    at the end of the tests
    """
    separator1 = '=' * 70
    separator2 = '-' * 70
    def __init__(self, stream, descriptions, verbosity):
        super(ParallelTextTestResult, self).__init__(stream, descriptions, verbosity)
        self.stream = stream
        self.showAll = verbosity > 1
        self.dots = verbosity == 1
        self.descriptions = descriptions

    def startTest(self,test):
        super(ParallelTextTestResult, self).startTest(test)

    def getDescription(self, test):
        doc_first_line = test.shortDescription()
        if self.descriptions and doc_first_line:
            return '\n'.join((str(test), doc_first_line))
        else:
            return str(test)

    # Add to this result object an individual, external result
    def accumulateOne(self, test, result):
        self.testsRun += result.testsRun
        if result.wasSuccessful():
            self.addSuccess(test)
        else:
            for t,err in result.failures:
                self.failures.append((t,err))
                if self.showAll:
                    self.stream.writeln( self.buildMessage(t,"FAIL") )
                    self.stream.flush()

            for t,err in result.errors:
                self.errors.append((t,err))
                if self.showAll:
                    self.stream.writeln( self.buildMessage(t,"ERROR") )
                    self.stream.flush()
            # TODO rest of cases

    def buildMessage(self, test, status):
        return self.getDescription(test) + " ... " + status

    def addSuccess(self, test):
        super(ParallelTextTestResult, self).addSuccess(test)
        if self.showAll:
            self.stream.writeln( self.buildMessage(test,"ok") )
            self.stream.flush()
        elif self.dots:
            self.stream.write('.')
            self.stream.flush()

    def addError(self, test, err):
        super(ParallelTextTestResult, self).addError(test, err)
        test.tearDown()
        if self.showAll:
            self.stream.writeln( self.buildMessage(test,"ERROR") )
            self.stream.flush()
        elif self.dots:
            self.stream.write('E')
            self.stream.flush()

    def addFailure(self, test, err):
        super(ParallelTextTestResult, self).addFailure(test, err)
        test.tearDown()
        if self.showAll:
            self.stream.writeln( self.buildMessage(test, "FAIL") )
            self.stream.flush()
        elif self.dots:
            self.stream.write('F')
            self.stream.flush()

    def addSkip(self, test, reason):
        super(ParallelTextTestResult, self).addSkip(test, reason)
        test.tearDown()
        if self.showAll:
            self.stream.writeln( self.buildMessage(test, "skipped {0!r}".format(reason)) )
            self.stream.flush()
        elif self.dots:
            self.stream.write("s")
            self.stream.flush()

    def addExpectedFailure(self, test, err):
        super(ParallelTextTestResult, self).addExpectedFailure(test, err)
        test.tearDown()
        if self.showAll:
            self.stream.writeln( self.buildMessage(test, "expected failure") )
            self.stream.flush()
        elif self.dots:
            self.stream.write("x")
            self.stream.flush()

    def addUnexpectedSuccess(self, test):
        super(ParallelTextTestResult, self).addUnexpectedSuccess(test)
        test.tearDown()
        if self.showAll:
            self.stream.writeln( self.buildMessage(test, "unexpected success") )
            self.stream.flush()
        elif self.dots:
            self.stream.write("u")
            self.stream.flush()

    def printErrors(self):
        if self.dots or self.showAll:
            self.stream.writeln()
            self.stream.flush()
        self.printErrorList('ERROR', self.errors)
        self.printErrorList('FAIL', self.failures)

    def printErrorList(self, flavour, errors):
        for test, err in errors:
            self.stream.writeln(self.separator1)
            self.stream.writeln("%s: %s" % (flavour,self.getDescription(test)))
            self.stream.writeln(self.separator2)
            self.stream.writeln("%s" % err)
            self.stream.flush()

class ParallelTextTestRunner(unittest.TextTestRunner):
    resultclass = ParallelTextTestResult

    def __init__(self, nworkers=None, stream=None, descriptions=True, verbosity=1,
                 failfast=False, buffer=False, resultclass=None, warnings=None,
                 *, tb_locals=False):
        super(ParallelTextTestRunner, self).__init__(stream, descriptions, verbosity,
                 failfast, buffer, resultclass, warnings, tb_locals=tb_locals)
        self.nworkers = nworkers

    def run(self, test):
        test.nworkers = self.nworkers
        super(ParallelTextTestRunner,self).run(test)

def _runOne(test):
    oneresult = test()
    # We need to disconnect from the IO as it is non-pickable
    oneresult._original_stderr = None
    oneresult._original_stdout = None
    return oneresult


class ParallelTestSuite(unittest.TestSuite):

    def run(self, result, debug=False):
        import multiprocessing as mp
        topLevel = False
        if getattr(result, '_testRunEntered', False) is False:
            result._testRunEntered = topLevel = True

        if not debug:
            if result.shouldStop:
                return False

            # Accumulate here (test, asyncResults) from calling apply_async
            self.asyncResults = []
            mp.set_start_method('spawn')
            with mp.Pool(processes=self.nworkers) as pool:

                for index, test in enumerate(self):
                    if _isnotsuite(test):
                        self._tearDownPreviousClass(test, result)
                        self._handleModuleFixture(test, result)
                        self._handleClassSetUp(test, result)
                        result._previousTestClass = test.__class__

                        if (getattr(test.__class__, '_classSetupFailed', False) or
                            getattr(result, '_moduleSetUpFailed', False)):
                            return True


                    def updateResult(*args):
                        # Check which tests have finished to avoid waiting for all
                        # the test to finish to now if something went wrong
                        for i in range(len(self.asyncResults)):
                            if self.asyncResults[i][1].ready():
                                oneresult = self.asyncResults[i][1].get()
                                # Accumulate their result, and reconnect to IO
                                oneresult._original_stderr = sys.stderr
                                oneresult._original_stdout = sys.stdout
                                oneresult.stream = sys.stderr
                                result.accumulateOne(self.asyncResults[i][0], oneresult)
                                # And stop caring about them!
                                self.asyncResults.pop(i)
                                return

                    self.asyncResults.append( (test, pool.apply_async(_runOne, (test,), callback=updateResult) ) )

                    if self._cleanup:
                        self._removeTestAtIndex(index)

                pool.close()
                pool.join()
                # We need to call this again for the last test that finished
                updateResult()
        else:
            print("Debug not yet supported!")

        if topLevel:
            self._tearDownPreviousClass(None, result)
            self._handleModuleTearDown(result)
            result._testRunEntered = False
        return
