from pykdgrav3_utils import units
import matplotlib
import os
if os.environ.get('REMOTE_PLOT')!=None:
    print ("Plotting in remote enviroment")
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
u = units.units(1.,1.)


def comoving_soundwave_solution(T0, gamma, k, Nperiods=1, verbose=False):
    if gamma != 5./3.:
        print ('Not implemented')

    cs = np.sqrt(gamma*u.dGasConst*T0 )



    # Comoving integration

    # We assume Omega_M = 1 and Omega_L = 0, such that the time is:
    # t = 2/(3 H0) a**1.5
    # In code units, 
    H0 = np.sqrt(np.pi*8/3) # such that rho_crit = 1 when a=1

    # Now, if we want a period of the wave, 1/cs in code units, and at t_f a=1
    dt = 1./cs
    t_f = 2./(3.*H0) 
    t_0 = t_f - Nperiods*dt

    a_init = (3.*H0/2 * t_0)**(2/3.)
    z_init = 1./a_init - 1.

    """
    a_init = ( 1. - 3.*H0*Nperiods/cs/2.)**(2./3.)
    z_init = 1./a_init - 1.

    t_f = 2./(3.*H0) * 1.**(3./2.)
    t_0 = 2./(3.*H0) * a_init**(3./2.)
    """

    u_0 = cs*cs /gamma/(gamma-1.)
    cs_f = np.sqrt(u_0 * (t_f/t_0)**(-2*(gamma-1))*gamma*(gamma-1.) )
    if verbose:
        print('Initial a:', a_init)
        print('Initial z:', z_init)
        print('Initial H:', 2./3./t_0)
        print('Final H:', 2./3./t_f)
        print ('Initial time (code units): ', t_0)
        print ('Final time (code units): ', t_0+1./cs, t_f)
        print ('Initial/final cs: ', cs, cs_f)
        print ('Final temperature: ', cs_f*cs_f/gamma/u.dGasConst)

    tt = np.linspace(t_0, t_f, 20)


    noexp_theta = (tt-t_0)*k*cs 

    theta = cs*t_0**(2./3.)/(3.*H0/2.)**(2./3.) * 3.* (t_0**(-1./3.) - tt**(-1./3.)) * k
    return tt, theta, noexp_theta



def plot_phases(gamma, k):
    # Example plot of the solution for \theta(t) for different background temperatures
    f, ax = plt.subplots(1, figsize=(5,4.4))

    N = 5
    log0 = -1
    logf = 3

    logT = np.linspace(-1, 3, 5)
    T = 10**logT

    cmap = matplotlib.cm.plasma
    c = (logT-log0)/(logf - log0) * 0.9


    for i in range(len(T)):
        tt, noexp_theta, theta = comoving_soundwave_solution(T[i], gamma, k, verbose=False, Nperiods=1.)

        ax.plot(noexp_theta, noexp_theta-theta, c=cmap(c[i]), ls='-', label=r'$T=10^{%d}$'%logT[i])


    ax.axhline(0., ls='--', c='k', alpha=0.3)
    ax.axvline(2.*np.pi, ls='--', c='k', alpha=0.3)
    ax.set_xlabel(r'$\theta^{\mathrm{no-exp}}$')
    ax.set_ylabel(r'$\theta^{\mathrm{no-exp}} - \theta$')
    ax.legend(loc=0, fancybox=True)

    plt.tight_layout()

    plt.show()


if __name__=='__main__':
    plot_phases(5./3., 2.*np.pi)
