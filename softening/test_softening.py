from pkdtest import PKDGRAV3TestCase
import h5py
import numpy as np


class SofteningTest(PKDGRAV3TestCase):
    title = "Check correct handling of the softening"
    description = 'This test does not have figures, it just uses asserts'
    plots = []

    schemes = ["MFM"]
    __name__ = "Softening"
    folder_name = "softening"

    compile_flags = {'ENTROPY_SWITCH':True}
    max_threads = 1

    def test_softening(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/cosmology.par'
        self.set_scheme(self.schemes[0])
        self.compile()

        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)

        # Default behaviour: constant comoving softening
        self.output_file = self.output_dir + '/const_soft'
        self.test_args = '-tf '+self.pkdgrav3_test_dir+'/euclid_z0_transfer_combined.dat'
        self.run_pkdgrav3()
        self.check_const()

        # Change to physical given a constant factor (the scale-factor at transition)
        self.dMaxPhysicalSoft   = 0.5
        self.output_file = self.output_dir + '/maxphysmul_soft'
        self.test_args = '+SMM -MaxPhysicalSoft '+str(self.dMaxPhysicalSoft)+' -tf '+self.pkdgrav3_test_dir+'/euclid_z0_transfer_combined.dat'
        self.run_pkdgrav3()
        self.check_maxphys()

        # Use directly physical softening and set a relative maximum for high redshift
        self.dSoftMax = 2.0
        self.output_file = self.output_dir + '/maxmul_soft'
        self.test_args = '+SMM +PhysSoft -eMax '+str(self.dSoftMax)+' -tf '+self.pkdgrav3_test_dir+'/euclid_z0_transfer_combined.dat'
        self.run_pkdgrav3()
        self.check_softmax()

    def get_partsoft(self, runtype):
        import glob

        snapnames = sorted(glob.glob(self.output_dir+'/'+runtype+'_soft.*.0'))

        softs = np.empty(len(snapnames))
        zz = np.empty(len(snapnames))
        for i, snapname in enumerate(snapnames):
            snap = h5py.File(snapname)
            soft = snap['PartType0/Softening'][...]
            self.assertEqual( np.std(soft), 0.0, msg='There should not be softening variations')

            softs[i] = soft[0]
            zz[i] = snap['Header'].attrs['Redshift']

        return zz, softs

    def check_softmax(self):
        zz, softs = self.get_partsoft('maxmul')
        aa = 1./(zz + 1.)

        self.assertGreater( np.std(softs) , 0.0, msg='The softening should change when bPhysicalSoft is set')

        ztrans = self.dSoftMax - 1.
        self.assertAlmostEqual( np.std(softs[zz > ztrans]) , 0.0, msg='The comoving softening at high redshift should be constant')
        self.assertAlmostEqual( np.std((softs*aa)[zz < ztrans]), 0.0, msg='The physical softening at low redshift should be constant')

    def check_maxphys(self):
        zz, softs = self.get_partsoft('maxphysmul')
        aa = 1./(zz + 1.)

        self.assertGreater( np.std(softs) , 0.0, msg='The softening should change when dMaxPhysicalSoft is set')

        ztrans = 1./self.dMaxPhysicalSoft - 1.
        self.assertAlmostEqual( np.std(softs[zz > ztrans]) , 0.0, msg='The comoving softening at high redshift should be constant')
        self.assertAlmostEqual( np.std((softs*aa)[zz < ztrans]), 0.0, msg='The physical softening at low redshift should be constant')


    def check_const(self):
        _, softs = self.get_partsoft('const')
        self.assertEqual( np.std(softs) , 0.0, msg='The softening should be constant in comoving coordinates by default')


    def plot(self, scheme):
        from softening.plot_cosmo import run
        run(self.output_file+'.00100.0', self.pkdgrav3_test_dir + '/softening_%s_%s.png'%(scheme, self.identifier))
