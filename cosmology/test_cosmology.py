from pkdtest import PKDGRAV3TestCase


class CosmologyTest(PKDGRAV3TestCase):
    title = "IC generation with gas"
    description = "Test for generation of initial conditions with a gaseous component."
    plots = [{"file": "cosmology_MFM",
              "caption": "Summary of the simulation at the last snapshot"}]

    schemes = ["MFM"]
    __name__ = "Cosmology"
    folder_name = "cosmology"

    compile_flags = {'ENTROPY_SWITCH':True}

    def test_cosmology(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/cosmology.par'
        for scheme in self.schemes:
            self.set_scheme(scheme)
            self.compile()

            self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_%s'%(scheme, self.identifier)
            self.output_file = self.output_dir + '/cosmology'
            self.test_args = '-tf '+self.pkdgrav3_test_dir+'/euclid_z0_transfer_combined.dat'
            self.run_pkdgrav3()

            self.plot(scheme)


    def plot(self, scheme):
        from cosmology.plot_cosmo import run
        run(self.output_file+'.00100.0', self.pkdgrav3_test_dir + '/cosmology_%s_%s.png'%(scheme, self.identifier))
