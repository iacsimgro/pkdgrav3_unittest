from pkdtest import PKDGRAV3TestCase


class RiemannTest(PKDGRAV3TestCase):
    title = "Riemann problem"
    description = ("The code should be able to correclty recover all the states "
                    "in the Riemann solution. "
                    "However, some overshooting is expected due to the use of limiters.")
    plots = [{"file": "riemann",
              "caption": "Riemann solution"}]

    schemes = ["MFM", "MFV"]
    __name__ = "Riemann"
    folder_name = "riemann"


    def generateIC(self):
        import numpy as np
        from pykdgrav3_utils import hdf5


        nx = 200
        ny = 4
        Lx = 1.0
        N=nx*ny**2 

        # Variable mass
        #x = np.linspace(-Lx/2, Lx/2, num=nx, endpoint=False)
        #y = np.linspace(-Ly/2, Ly/2, num=ny, endpoint=False)
        #mesh = np.meshgrid(x,y,y)


        # Constant mass
        x = np.concatenate( (np.linspace(-Lx/2, 0., num=4*nx//5, endpoint=False), 
                             np.linspace(0, Lx/2, num=1*nx//5, endpoint=False) ) )


        # The width is computed such that neither sides has too low/high number of ngbs
        dx_mean = 0.5*(x[1]-x[0]+ x[-1]-x[-2]) 
        Ly = dx_mean * ny

        y = np.linspace(-Ly/2, Ly/2, num=ny, endpoint=False)
        mesh = np.meshgrid(x,y,y)

        x = mesh[0].flatten()
        y = mesh[1].flatten()
        z = mesh[2].flatten()
        del mesh

        P0 = 1.
        T0 = 1.
        mass0 = (Lx/2.)*Ly*Ly/(4*N/5)
        # variable mass
        #mass0 = Lx*Ly*Ly/N # Mass for rho=1

        cs = np.sqrt(1.4)

        maskR = x>0.
        maskL = x<=0.


        mass = np.ones_like(x)*mass0
        # Variable mass
        #mass[maskL] = 1.0
        #mass[maskR] = 0.25
        #mass *= mass0

        dens = np.ones_like(x) # This should not change anything
        dens[maskL] = 1.0
        dens[maskR] = 0.25

        P = np.ones_like(x)*P0
        P[maskL] = 1.0
        P[maskR] = 0.1795

        gamma = 1.4
        Uint = P/dens / (gamma-1.)

        vx = np.ones_like(x)
        vx[maskL] = 0.0
        vx[maskR] = 0.0

        vx = vx.flatten()
        pos = np.stack((x, y, z), axis=1)

        gas_dict = {'Coordinates': pos,
                    'Masses': mass.flatten(),
                    'Velocities': np.stack([vx, np.zeros_like(vx), np.zeros_like(vx)], axis=1),
                    'InternalEnergy':Uint.flatten(),
                    'SmoothingLength': np.zeros_like(x)} # Will be computed by PKDGRAV3


        hdf5.save({'PartType0':gas_dict}, self.input_file)
        return Ly



    def test_riemann(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/riemann.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        L = self.generateIC()
        for scheme in self.schemes:
            self.set_scheme(scheme)
            self.compile()

            self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_%s'%(scheme, self.identifier)
            self.output_file = self.output_dir + '/riemann'
            self.test_args = '-L 1.,%f,%f'%(L,L)
            self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from riemann.plot import run
        run(self.identifier, self.pkdgrav3_test_dir)

        #from soundwaves.plotSolution import run
        #run(self.identifier)



