import os
import sys

def run(ident, path):
    import numpy as np
    from riemann import sod
    import matplotlib
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from pykdgrav3_utils import hdf5


    gamma = 1.4
    snap_file  = 'riemann.00001.0'
    t = 0.13


    ###  MFM

    snap = hdf5.read_single(path+'/snaps_MFM_'+ident+'/'+snap_file, return_units=False)
    gas = snap['PartType0']

    mfm_x = gas['Coordinates'][:,0]
    mfm_vx = gas['Velocities'][:,0]
    mfm_y = gas['Coordinates'][:,1]
    mfm_z = gas['Coordinates'][:,2]
    mfm_rho = gas['Density'][...]
    mfm_p = gas['InternalEnergy'][...]*gas['Density'][...]*(gamma-1)
    mfm_mass = gas['Masses'][...]
    mfm_h = gas['SmoothingLength'][...]


    ### VARIABLE MASS
    snap = hdf5.read_single(path+'/snaps_MFV_'+ident+'/'+snap_file, return_units=False)
    gas = snap['PartType0']

    mfv_x = gas['Coordinates'][:,0]
    mfv_vx = gas['Velocities'][:,0]
    mfv_y = gas['Coordinates'][:,1]
    mfv_z = gas['Coordinates'][:,2]
    mfv_rho = gas['Density'][...]
    mfv_p = gas['InternalEnergy'][...]*gas['Density'][...]*(gamma-1)
    mfv_mass = gas['Masses'][...]
    mfv_h = gas['SmoothingLength'][...]

    ### ANALYTICAL SOLUTION

    positions, regions, values = sod.solve(left_state=(1, 1, 0), right_state=(0.1795, 0.25, 0.),
                                           geometry=(-0.5, 0.5, 0.), t=t, gamma=1.4, npts=500)

    #ax[0].plot(values['x'], values['rho'], linewidth=1.5, color='r')
    #ax[1].plot(values['x'], values['p'], linewidth=1.5, color='r')
    #ax[2].plot(values['x'], values['u'], linewidth=1.5, color='r')


    ### PLOTS

    mfv_marker = 'kv'
    mfm_marker = 'b.'

    mfv_ms = 3
    mfm_ms = 4

    mew = 0.5

    f, axs = plt.subplots(2,2, figsize=(8,8))
    [axs[j][i].set_xlim(-.25, .25) for i in [0,1] for j in [0,1] ]
    [axs[j][i].set_xlabel(r'$x$') for i in [0,1] for j in [0,1] ]

    ## Density
    axs[0][0].plot(values['x'], values['rho'], 'r')
    axs[0][0].plot(mfm_x, mfm_rho, mfm_marker, ms=mfm_ms, label='MFM')
    axs[0][0].plot(mfv_x, mfv_rho, mfv_marker, ms=mfv_ms, mew=mew, label='MFV')

    axs[0][0].set_ylabel(r'$\rho$')
    axs[0][0].legend(fancybox=True, fontsize=16)

    # Pressure
    axs[1][0].plot(values['x'], values['p'], 'r')
    axs[1][0].plot(mfm_x, mfm_p, mfm_marker, ms=mfm_ms)
    axs[1][0].plot(mfv_x, mfv_p, mfv_marker, ms=mfv_ms, mew=mew)

    axs[1][0].set_ylabel(r'$p$')

    # Velocity
    axs[0][1].plot(values['x'], values['u'], 'r')
    axs[0][1].plot(mfm_x, mfm_vx, mfm_marker, ms=mfm_ms)
    axs[0][1].plot(mfv_x, mfv_vx, mfv_marker, ms=mfv_ms, mew=mew)

    axs[0][1].set_ylabel(r'$v_x$')
    axs[0][1].set_ylim(-0.1, 0.75)

    # Smoothing length
    fact = 1./200.
    axs[1][1].plot(mfm_x, mfm_h/fact, mfm_marker, ms=mfm_ms)
    axs[1][1].plot(mfv_x, mfv_h/fact, mfv_marker, ms=mfv_ms, mew=mew)
    axs[1][1].set_ylabel(r'$h \times \frac{N}{L}$')

    plt.tight_layout()
    if path is not None:
        f.savefig(path+'/riemann_%s.png'%ident)
    else:
        plt.show()

if __name__=='__main__':
    if len(sys.argv) > 2:
        run(os.environ.get('IDENTIFIER'), sys.argv[2])
    else:
        run(os.environ.get('IDENTIFIER'))

