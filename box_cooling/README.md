# Box cooling

In this test we directly compare the Wiersma et al. 2009 cooling with the
GRACKLE library.

For this comparison, we set a cosmological box with [OmegaL,Omega0,Omegab] =
[0.7,0.3,0.05], and let it evolve without gravity. The initial gas density
is set equal to the cosmic mean for baryons.

Before reionization, the temperature should decrease as a power law. Afterwards,
all the gas should be around the equilibrium temperature imposed by the UV
background, for the chosen initial density.

The reference run used the `CloudyData_UVB=HM2012.h5` table.

![BoxCooling](eq_ref.png "BoxCooling")

We also test the cooling in the bremsstrahlung regime at redshift 4. For this
case, the initial gas density is set to ~10^4 times the cosmic mean for baryons.
This can be compared with analytical expectations, which can be computed
for high temperatures.

![RateCooling](rate_ref.png "RateCooling")
