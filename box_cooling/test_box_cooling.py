from pkdtest import PKDGRAV3TestCase


class BoxCoolingTest(PKDGRAV3TestCase):
    title = "Box cooling"
    description = ("Test for cooling in cosmological volumes. "
                    "This compares both cooling modules, Wiersma and GRACKLE."
                    "\n"
                    "More details can be found at the README.")
    plots = [{"file": "eq",
              "caption": "Evolution of the equilibrium temperature"},
             {"file": "rate",
              "caption": "Test of the cooling rate in the Bremhstralung regime"}]

    schemes = ["MFM"]
    __name__ = "BoxCooling"
    folder_name = "box_cooling"


    def generateIC(self, zi, T0, rho0):
        import numpy as np
        import h5py
        import matplotlib.pyplot as plt
        from pykdgrav3_utils import hdf5



        n = 10
        L = 1.
        N=n**3

        ai = 1./(zi+1)

        #IC_file.create_group('parameters')
        #IC_file['parameters'].attrs['dTime'] = ai
        #IC_file['parameters'].attrs['dTimeOld'] = ai

        np.random.seed(12345)
        x = np.random.rand(N)-0.5
        y = np.random.rand(N)-0.5
        z = np.random.rand(N)-0.5

        pos = np.column_stack([x, y, z])
        """
        print (pos.shape)

        x = np.linspace(-L/2, L/2, n, endpoint=False)
        mesh = np.array(np.meshgrid(x,x,x))
        pos = mesh.reshape((3,N))
        print (pos.T.shape)
        """

        gamma = 5./3.

        # We set the particle mass to have rho0
        mass0 =  rho0*L*L*L/N
        mass = np.ones_like(x)*mass0

        gas_dict = {'Coordinates': pos,
                    'Velocities': np.zeros_like(pos),
                    'Temperature': np.ones(N)*T0,
                    'Masses': mass}

        hdf5.save({'PartType0':gas_dict}, self.input_file, time=ai)


    def test_box_cooling(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/boxcooling.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.set_scheme('MFM')

        # Two cases:
        #   1. Pure Brehmstralung at high z
        #   2. Equilibrium temperature to z=0
        name =  ["rate","box"]
        z0 =    [4,     40]
        T0 =    [1e8,   1e3]
        rho0 =  [1e3,   0.05]
        zto =   [3,     0]
        n =     [10,    50]
        n10 =   [0,     10]
        for cooling in [True, False]:
            self.compile_flags['COOLING'] = cooling
            self.compile_flags['GRACKLE'] = not cooling
            if cooling:
                modulename = "wiersma"
            else:
                modulename = "grackle"

            self.compile()
            for i in range(2):
                self.generateIC(z0[i], T0[i], rho0[i])
                print(modulename)
                self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_%s_%s'%(self.identifier, name[i], modulename)
                self.output_file = self.output_dir + '/box_cooling'
                self.test_args = '-n %d -nsync %d -z %f -zto %f'%(n[i], n10[i], z0[i], zto[i])
                self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from box_cooling.plot_T_evolution import run
        # all these can be more clean when the plot routines are incorporated in the classes
        run(self.pkdgrav3_test_dir+'/snaps_%s_'%self.identifier, path_eq=self.pkdgrav3_test_dir + '/eq_%s.png'%(self.identifier), \
                              path_rate=self.pkdgrav3_test_dir + '/rate_%s.png'%(self.identifier))


