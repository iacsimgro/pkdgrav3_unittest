
def run(outfile, path_eq=None, path_rate=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import glob
    import numpy as np
    from pykdgrav3_utils import hdf5, units

    def read_evolution(outfile, cooling):
        files = sorted(glob.glob(outfile+cooling+'/box_cooling.?????.0'))

        snap, u = hdf5.read_single(files[0], return_units=True)
        snap.close()

        Nfiles = len(files)
        T = np.zeros(Nfiles)
        Tsigma = np.zeros(Nfiles)
        z = np.zeros(Nfiles)
        time = np.zeros(Nfiles)

        nH = 0.
        nHstd = 0.
        for i in range(Nfiles):
            snap = hdf5.read_single(files[i], return_units=False)

            T[i] = np.median(snap['PartType0/Temperature'])
            Tsigma[i] = np.std(snap['PartType0/Temperature'])
            z[i] = snap['Header'].attrs['Redshift']
            time[i] = snap['Header'].attrs['Time'] * u.dSecUnit
            nH = np.mean(snap['PartType0/Density'][...] * 0.75 / u.MHYDR * u.dGmPerCcUnit)
            snap.close()

        a = 1./(z+1)

        return time, a, nH/a**3, T, Tsigma

    f, ax = plt.subplots(1)

    time, a, nH, T, Tsigma = read_evolution(outfile, 'box_wiersma')
    z = 1./a - 1.
    ax.errorbar(z+1, T, yerr=Tsigma, label='wiersma temperature', fmt='.-')

    time, a, nH, T, Tsigma = read_evolution(outfile, 'box_grackle')
    z = 1./a - 1.
    ax.errorbar(z+1, T, yerr=Tsigma, label='grackle temperature', fmt='.-')

    zi = 30
    Ti = 2e3
    z = np.linspace(zi, 10, 5)
    # Analytical expectation from a matter dominated universe
    ax.plot(z+1, Ti/ ( (zi+1.)/(z+1) )**2, 'k--', alpha=0.5, label=r'$T\propto a^{-2}$')

    ax.semilogx()
    ax.semilogy()

    ax.set_xlabel('z+1')
    ax.set_ylabel('Temperature (K)')
    ax.set_xlim(9e-1, 5e1)
    ax.set_ylim(1e2, 4e4)
    plt.legend()


    if path_eq is not None:
        f.savefig(path_eq)
    else:
        plt.show()

    ax.clear()


    secToMyr = 1/(31557600*1e6)
    time, a, nH, T, Tsigma = read_evolution(outfile, 'rate_wiersma')
    ax.plot(time*secToMyr, T, '.-', label='wiersma temperature')
    time, a, nH, T, Tsigma = read_evolution(outfile, 'rate_grackle')
    ax.plot(time*secToMyr, T, '.-', label='grackle temperature')
    ax.semilogy()

    # Analytical expectation from pure bremsstrahlung
    XH = 0.75
    gamma = 5./3.
    mu = 0.6
    kboltz = 1.38e-16 # erg K-1
    def dTdt(T, nH):
        # eq 12 Dalla Vecchia & Schaye 2012
        Lambda = 1.12e-23 * nH**2 * np.sqrt(T/10**7.5) * (1+XH)*(1+3*XH)/(8*XH**2)
        return Lambda * (gamma - 1.) * mu * XH / nH / kboltz

    tt = np.linspace(time[0], time[-1], 200)
    nHs = np.interp(tt, time, nH)

    T_brem = np.empty_like(tt)
    T_brem[0] = T[0]
    for i in range(1,len(tt)):
        T_brem[i] = T_brem[i-1] - (tt[i]-tt[i-1])*dTdt(T_brem[i-1],nHs[i-1])


    # eq 13 DV & Schaye 2012
    tc = 3.26e1 / nH[0] * np.sqrt(T[0]/10**7.5) * np.sqrt(mu/0.6) # Myr

    ax.plot(tt*secToMyr, T_brem, 'k--', alpha=0.5, label='Pure bremsstrahlung')
    ax.axvline(time[0]*secToMyr+tc, ls='--', c='k', label='Cooling time')

    ax.set_ylabel('Temperature (K)')
    ax.set_xlabel('Cosmic time (Myr)')
    ax.set_xlim(1500,2100)
    ax.set_ylim(1e3,2e8)
    plt.legend()

    if path_rate is not None:
        f.savefig(path_rate)
    else:
        plt.show()
