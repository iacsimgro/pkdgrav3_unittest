import os
import pathlib
import unittest
import subprocess
from paralleltest import ParallelTestSuite


class PKDGRAV3TestSuite(ParallelTestSuite):
    # List of all the possible test, not only those which will be run
    _listTest = []

    def __init__(self,tests=()):
        super().__init__(tests)

    def listTest(self, test):
        self._listTest.append(test)

    def run(self,results, debug=False):
        super().run(results, debug)

    def configure_all(self, cases=[], **kwargs):
        self.identifier = kwargs['identifier']
        self.testdir = kwargs['testdir']
        for t in self._listTest:
            if t.__name__ in cases:
                self.addTest(t)
                t.configure(**kwargs)

    def getTestNames(self):
        names = []
        for t in self._listTest:
            names.append(t.__name__)
        return names

    def generateReport(self):
        import base64
        from jinja2 import Template
        def get_base64(path):
            with open(path, "rb") as img:
                return base64.b64encode(img.read()).decode('utf-8')

        descriptors = {'tests':[]}
        for t in self._listTest:
            if hasattr(t,"title"):
                tmp_descr = {'title': t.title,
                             'description': t.description,
                             'plots': t.plots,
                             'identifier': self.identifier,
                             'folder': self.testdir+'/'+t.folder_name}

                for plot in tmp_descr['plots']:
                    try:
                        plot['ref_base64'] = get_base64(tmp_descr['folder']+'/'+plot['file']+'_ref.png')
                    except:
                        print ("Empty reference for ", t.title)
                        plot['ref_base64'] = ''
                    try:
                        plot['base64'] = get_base64(tmp_descr['folder']+'/'+plot['file']+ '_'+tmp_descr['identifier']+'.png')
                    except:
                        print ("Empty solution for ", t.title, tmp_descr['folder']+'/'+plot['file']+ '_'+tmp_descr['identifier']+'.png')
                        plot['base64'] = ''

                #print(tmp_descr)
                descriptors['tests'].append(tmp_descr)

        with open(self.testdir+'/report.html.j2') as file_:
            template = Template(file_.read())

        out_report = template.render(descriptors)
        with open(self.testdir+"/report_"+self.identifier+".html", "w") as f:
            f.write(out_report)




class PKDGRAV3TestCase(unittest.TestCase):
    name = ''
    folder_name = ''

    gen_build_dir = True
    do_compile = True
    do_run = True

    threads = 1
    max_threads = 1000
    min_threads = 1

    extra_args = ''
    test_args = ''
    identifier = ''
    prepkdcmd = ''

    schemes = ['MFM', 'MFV']

    input_file = None

    default_flags = {
            "USE_MFM":                  True,
            "FORCE_2D":                 False,
            "FORCE_1D":                 False,
            "LIMITER_BARTH":            False,
            "LIMITER_CONDBARTH":        True,
            "ENTROPY_SWITCH":           False,
            "COOLING":                  False,
            "GRACKLE":                  False,
            "STAR_FORMATION":           False,
            "EEOS_JEANS":               False,
            "EEOS_POLYTROPE":           False,
            "FEEDBACK":                 False,
            "HERNQUIST_POTENTIAL":      False,
            "NFW_POTENTIAL":            False,
            "BLACKHOLES":               False,
            "DEBUG_BH_ONLY":            False,
            "DEBUG_BH_NODRIFT":         False,
            "DEBUG_FLUX_NOLIMITER":     False,
            "STELLAR_EVOLUTION":        False}

    compile_flags = {}


    def configure(self, **kwargs):
        self.identifier = kwargs['identifier']
        self.pkdgrav3_test_dir = kwargs['testdir']+'/'+self.folder_name
        self.threads = kwargs['threads']
        if self.threads > self.max_threads:
            self.threads = self.max_threads
        self.grackle_dir = kwargs['grackle_dir']
        self.grackle_table = kwargs['grackle_table']
        self.stev_dir = kwargs['stev_dir']
        self.pkdgrav3_dir = kwargs['pkdgrav3_dir']
        self.extra_args = kwargs['extra_args']
        self.force_configure = kwargs['force_configure']
        self.dry_run = kwargs['dry_run']
        self.plot_only = kwargs['plot_only']
        self.coolingdir = kwargs['coolingdir']
        self.cc = kwargs['cc']
        self.cxx = kwargs['cxx']
        self.fc = kwargs['fc']
        self.fftw = kwargs['fftw']
        self.cmake_args = kwargs['cmake_args']

        if self.plot_only:
            self.do_run = False
            self.do_compile = False

        if kwargs['mfv']:
            if 'MFM' in self.schemes:
                self.schemes.pop(self.schemes.index('MFM'))
        if kwargs['mfm']:
            if 'MFV' in self.schemes:
                self.schemes.pop(self.schemes.index('MFV'))

    def generate_build_dir(self):
        if self.gen_build_dir:
            self.pkdgrav3_build_dir = self.pkdgrav3_dir + "/build/" + self.__name__
        else:
            self.pkdgrav3_build_dir = self.pkdgrav3_dir + "/build"

        pathlib.Path(self.pkdgrav3_build_dir).mkdir(exist_ok=True, parents=True)

    def setUp(self):
        if self.do_compile and self.force_configure:
            self.generate_build_dir()
            from shutil import rmtree
            rmtree(self.pkdgrav3_build_dir)

        # Open the log file
        pathlib.Path(self.pkdgrav3_test_dir).mkdir(exist_ok=True)
        self.logfile = open(self.pkdgrav3_test_dir+'/log_%s_%s'%(self.__name__, self.identifier), "w")


    def tearDown(self):
        self.logfile.close()
        self.logfile = None

    def plot(self):
        pass

    def set_scheme(self,scheme):
        if scheme=='MFM':
            self.compile_flags['USE_MFM'] = True
        elif scheme=='MFV':
            self.compile_flags['USE_MFM'] = False
        else:
            assert KeyError



    def compile(self):
        if self.do_compile:
            self.generate_build_dir()

            # Prepare the cmake call assuming that all the compile-time options
            # are given as COMPILE_OPTION=True/False
            used_flags = self.default_flags.copy()
            for k in self.compile_flags.keys():
                if k in used_flags:
                    used_flags[k] = self.compile_flags[k]
                    print (self.__name__ + " compiling with: "+k+" - " +str(used_flags[k]))
                else:
                    raise KeyError


            opts = 'cmake'
            for k in used_flags:
                if used_flags[k]:
                    opts += " -D"+k+"=on"
                else:
                    opts += " -D"+k+"=off"

            opts += " -DCMAKE_C_COMPILER="+self.cc
            opts += " -DCMAKE_CXX_COMPILER="+self.cxx
            opts += " -DCMAKE_Fortran_COMPILER="+self.fc
            opts += " -DGRACKLE_ROOT="+self.grackle_dir
            opts += " -DFFTW_ROOT="+self.fftw
            opts += " "+self.cmake_args

            opts += " -S %s -B %s"%(self.pkdgrav3_dir, self.pkdgrav3_build_dir)
            if self.dry_run:
                print(opts)
            else:
                cmake_run = subprocess.Popen(opts, shell=True, stderr=subprocess.STDOUT, stdout=self.logfile)
                code = cmake_run.wait()
                self.assertEqual(code, 0)

                make_run = subprocess.Popen("make -C %s -j%d"%(self.pkdgrav3_build_dir, self.threads), shell=True, \
                        stderr=subprocess.STDOUT, stdout=self.logfile)
                code = make_run.wait()
                self.assertEqual(code, 0)

    def run_pkdgrav3(self):
        if self.do_run:
            pathlib.Path(self.output_dir).mkdir(exist_ok=True)

            opts = self.prepkdcmd + self.pkdgrav3_build_dir+'/pkdgrav3 '
            try:
                if self.compile_flags['COOLING']:
                    opts += ' -coolingtables '+self.coolingdir
                if self.compile_flags['GRACKLE']:
                    opts += ' -coolingtables '+self.grackle_table
            except KeyError:
                pass

            try:
                if self.compile_flags['STELLAR_EVOLUTION']:
                    opts += ' -stevtables '+ self.stev_dir
            except KeyError:
                pass

            opts += ' '+self.test_args
            opts += ' '+self.extra_args
            if self.input_file is not None:
                opts += ' '+"-I "+self.input_file
            opts += ' '+"-o "+self.output_file
            opts += ' '+'-sz '+str(self.threads)
            opts += ' '+self.param_file


            if self.dry_run:
                print (opts)
            else:
                pkd_run = subprocess.Popen(opts, shell=True, \
                        stderr=subprocess.STDOUT, stdout=self.logfile)
                code = pkd_run.wait()

                self.assertEqual(code, 0)

    def generateIC(self):
        pass

