# Santa Barbara Cluster

This test uses the initial conditions for the Santa Barbara Cluster comparison project with a resolution
of 64^3 particles.
This test only includes hydrodynamics, gravity and cosmological expansion.

## Output

For comparison purposes, we include in the repository the result of the same simulation performed by AREPO, with its public version. 
We compare different metrics used in the project, namely:
* Temperature profile
* Entropy profile
* Gas fraction profiles
Furthermore, we show the velocity histograms and the projected temperature, density. There are further metrics implemented within `plot_compare.py`, but are not shown for clarity.



![SantaBarbara](comparison_ref.png "SantaBarbara")
