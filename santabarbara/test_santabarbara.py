from pkdtest import PKDGRAV3TestCase


class SantaBarbaraTest(PKDGRAV3TestCase):
    title = "Santa Barbara Cluster"
    description = "Comparison with the AREPO results of the Santa Barbara Cluster project."
    plots = [{"file": "comparison_MFM",
              "caption": "Comparison with AREPO solution with MFM"}]

    schemes = ["MFM"]
    __name__ = "SantaBarbara"
    folder_name = "santabarbara"


    def generateIC(self):
        import h5py
        import numpy as np
        from shutil import copyfile
        from pykdgrav3_utils import units



        IC_filename = self.input_file

        IC_file = h5py.File(IC_filename, 'w')
        u = units.units(9.092913e15/0.5, 32e3/0.5, verbose=True)

        H0 = 50. / u.dKmPerSecUnit * ( u.dKpcUnit / 1e3)
        rho_crit = 3.*H0*H0/(8.*np.pi)
        print ("H0 ", H0, np.sqrt(8.*np.pi/3.))
        print ("rho_crit ", rho_crit)

        # We read the positions, velocities and so on from the gadget IC
        gadgetIC = h5py.File(self.pkdgrav3_test_dir+'/IC_000.hdf5', 'r')

        # We set the initial time
        IC_file.create_group('Header')
        IC_file.create_group('Units')
        IC_file.create_group('Cosmology')
        IC_file.create_group('Parameters')
        IC_file['Header'].attrs['Time'] = gadgetIC['Header'].attrs['Time']
        print (IC_file['Header'].attrs['Time'])

        # First, gas
        gamma = 5./3.
        aFac = 1./(20.+1.)

        pos = gadgetIC['PartType0']['Coordinates'][...] /32e3 - 0.5
        vel = gadgetIC['PartType0']['Velocities'][...] / u.dKmPerSecUnit * np.sqrt(aFac) / aFac
        temp = gadgetIC['PartType0']['InternalEnergy'][...]*(gamma-1.)
        N = temp.shape[0]
        dens = np.ones((N), dtype=np.float32)
        pot  = np.ones((N), dtype=np.float32)
        met  = np.ones((N), dtype=np.float32)
        order = np.arange(N, dtype=np.uint64)


        sphclassArr = np.array([(0, 0., 0.0, 0)], \
              dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        dmclassArr = np.array([(0, 0., 0.0, 0)], \
              dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})

        # We create the different classes
        print('Mass table', gadgetIC['Header'].attrs['MassTable']*1e10/u.dMsolUnit)
        gadTotGasMass =  gadgetIC['PartType0/Coordinates'].shape[0]*gadgetIC['Header'].attrs['MassTable'][0]
        gadTotDmMass  = gadgetIC['PartType1/Coordinates'].shape[0]*gadgetIC['Header'].attrs['MassTable'][1]
        totMass = gadTotGasMass + gadTotDmMass
        print('Total mass in gadget', totMass)


        # We create the different classes
        print('Mass table', gadgetIC['Header'].attrs['MassTable']*1e10/u.dMsolUnit)
        mass = gadgetIC['Header'].attrs['MassTable'][0]/totMass
        sphclassArr['mass'] = mass  * rho_crit
        sphclassArr['soft'] = 40./64000. #(3 * 1.**3 / (4.*np.pi*(2.*N)) )**(1./3.) /30.
        sphclassArr['class'] = 0
        print('soft', sphclassArr['soft'])

        mass = gadgetIC['Header'].attrs['MassTable'][1]/totMass
        dmclassArr['mass'] = mass  * rho_crit
        dmclassArr['soft'] = 40./64000. #(3 * 1.**3 / (4.*np.pi*(2.*N)) )**(1./3.) /30.
        dmclassArr['class'] = 1
        dmclassArr['start'] = order[-1]+1
        classArr = np.append(sphclassArr, dmclassArr) # We double it size, allowing for two classes

        print ("Total mass ", sphclassArr['mass']*N + dmclassArr['mass']*N)

        print ("Creating hdf5 file...")
        IC_file.create_group('PartType0')
        dset = IC_file['PartType0'].create_dataset("classes", data=classArr)
        dset = IC_file['PartType0'].create_dataset("class", data=np.zeros(N, dtype="|u1") )
        #dset = IC_file['PartType0'].create_dataset("mass", data=np.ones(N, dtype="<f4") )
        dset = IC_file['PartType0'].create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = IC_file['PartType0'].create_dataset("Velocities", data=vel)
        dset = IC_file['PartType0'].create_dataset("Temperature", data=np.ones(N)*100., dtype="<f4")
        #dset = IC_file['sph'].create_dataset("potential", data=pot, dtype="<f4")
        #dset = IC_file['sph'].create_dataset("metals", data=met, dtype="<f4")
        dset = IC_file['PartType0'].create_dataset("Density", data=dens, dtype="<f4")
        dset = IC_file['PartType0'].create_dataset("order", data=order, dtype="<u8")

        # Second, DM
        pos = gadgetIC['PartType1']['Coordinates'][...] / 32e3  - 0.5
        vel = gadgetIC['PartType1']['Velocities'][...] / u.dKmPerSecUnit * np.sqrt(aFac) / aFac
        dens = np.ones((N), dtype=np.uint64)

        order = order + order[-1] + 1

        dm_group = IC_file.create_group('PartType1')
        dset = IC_file['PartType1'].create_dataset('classes', data = classArr)
        dset = IC_file['PartType1'].create_dataset("class", data=np.ones(N, dtype="|u1") )
        dset = IC_file['PartType1'].create_dataset("Coordinates", data=pos)
        dset = IC_file['PartType1'].create_dataset("Velocities", data=vel)
        #dset = IC_file['dark'].create_dataset("potential", data=pot)
        dset = IC_file['PartType1'].create_dataset("order", data=order)
        #dset = IC_file['dark'].create_dataset("density", data=dens)


        # And close the file
        IC_file.close()



    def test_santabarbara(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/SB.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        # Reference run to z=0
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/santabarbara'
        self.run_pkdgrav3()
        self.plot()


    def plot(self):
        from santabarbara.plot_compare import run
        run(self.output_file+'.00015.0', self.pkdgrav3_test_dir+'/snaps_arepo/snap_016.hdf5', self.pkdgrav3_test_dir+'/comparison_MFM_%s.png'%self.identifier)

