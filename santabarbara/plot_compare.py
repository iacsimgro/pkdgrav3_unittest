import matplotlib
import os
if os.environ.get('REMOTE_PLOT')!=None:
     print ("Plotting in remote enviroment")
     matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
from pykdgrav3_utils import units




def running_median(x, y, N):
    min_x = np.min(x)
    dx = np.abs(np.max(x) - min_x)/ N
    result_y = np.zeros(N)
    result_x = np.zeros(N)
    result_sigma = np.zeros(N)
    for i in range(N):
        mask = np.logical_and(x<min_x+(i+1)*dx , x>min_x+i*dx)
        result_x[i] = min_x +(i+0.5)*dx
        result_y[i] = np.median(y[mask])
        result_sigma[i] = np.std(y[mask])
    return result_x, result_y, result_sigma




def plot_all(axs, im_axs, color, dmx, dmy, dmz, gasx,gasy,gasz, dmMass, gasMass, gasDens, gasTemp, CMx, CMy, CMz, dmModV, gasModV):
    gasCoordR = np.sqrt( (gasx-CMx)**2 + (gasy - CMy)**2 + (gasz - CMz)**2  ) 
    gasRmask = (gasCoordR > 10.) & (gasTemp > 0.)
    dmCoordR = np.sqrt( (dmx-CMx)**2 + (dmy - CMy)**2 + (dmz - CMz)**2  ) 
    dmRmask = dmCoordR >  10.


    ## General statistics
    Ngas = gasx.shape[0]
    Ndm = dmx.shape[0]
    print ("Total number of particles: %d \t gas: %d \t dm: %d"%(Ngas+Ndm, Ngas, Ndm))
    print ("Total mass in gas: %e \t  dm: %e"%(np.sum(gasMass), np.sum(dmMass)) )

    
    """
    ###################################
    ##### Radial density profiles #####
    ###################################
    H, xedges = np.histogram(np.log10(dmCoordR[dmRmask]), bins=30)# , range=np.log10([10, 10000]), bins=14)
    volume = 4.0/3.0 * np.pi * ((10**xedges[1:])**3 - (10**xedges[:-1])**3)
    dmRrhoity = H / volume * dmMass * (1000)**3 # de kcp3 a Mpc3
    middle = (xedges[1:] + xedges[:-1]) / 2
    axs[0].plot(10**middle/1000., dmRrhoity, color)

    H, xedges = np.histogram(np.log10(gasCoordR[gasRmask]), bins=30) #, range=np.log10([50, 10000]), bins=14)
    volume = 4.0/3.0 * np.pi * ((10**xedges[1:])**3 - (10**xedges[:-1])**3)
    gasRrhoity = H / volume * gasMass * (1000)**3 # de kcp3 a Mpc3
    middle = (xedges[1:] + xedges[:-1]) / 2
    axs[0].plot(10**middle/1000., gasRrhoity, color+'--')


    axs[0].set_xlabel('r [Mpc]')
    axs[0].set_ylabel(r'$\rho \; [M_\odot / Mpc^3]$')
    axs[0].set_xlim(1e-2,10)
    axs[0].semilogy()
    axs[0].semilogx()
    """
   


    """
    #################################
    #### Temperature histogram  #####
    #################################

    H, edges = np.histogram(np.log10(gasTemp[gasTemp>0]), bins=100, density=True)
    #H, edges = np.histogram(np.log10(gasDens), bins=100, density=True)

    axs[0].plot( 10**(0.5*(edges[1:]+edges[:-1])), H  , color)
    #axs[0].text( 0.1, 0.6, 'gas: %.1e '%(10**edges[np.argmax(H)]), transform=axs[0].transAxes)
    axs[0].axvline(10**edges[np.argmax(H)], color=color, linestyle='--')
#    axs[0].set_xlim(0.01, 1e9)
#    axs[0].set_ylim(0.1, 1e5)
    axs[0].loglog()
    """
    
    

    ###################################
    #####    Gas Fraction profile #####
    ###################################

    gasSortedR = np.sort(gasCoordR)
    gasCumMass = np.cumsum( gasSortedR*gasMass )
    dmSortedR = np.sort(dmCoordR)
    dmCumMass = np.cumsum( dmSortedR*dmMass)

    dmCumMassInterp = np.interp(gasSortedR, dmSortedR, dmCumMass)
    gasFraction = gasCumMass/(0.1*(gasCumMass+dmCumMassInterp))

    axs[0].plot(gasSortedR/1000., gasFraction, color)
    axs[0].axhline(1, linestyle=':', color='r')
    axs[0].set_xlim(0.01, 10)
    axs[0].set_ylim(0, 1.5)
    axs[0].semilogx()
    axs[0].set_xlabel('r [Mpc]')
    axs[0].set_ylabel('Gas Fraction: $M_{gas}/(\Omega_b M_{tot})$ ($<r$)')


    ###################################
    #####  Temperature  profile   #####
    ###################################

    x, y, sig = running_median( np.log10(gasCoordR[gasRmask]), gasTemp[gasRmask], 23)
    axs[2].errorbar(10**x/1000.,y, yerr=sig, c=color)
    axs[2].set_xlim(1e-2,10)
    axs[2].semilogx()
    axs[2].set_xlabel('r [Mpc]')
    axs[2].set_ylabel('Temperature [K]')
    axs[2].semilogy()


    
    ###################################
    #####   Entropy     profile   #####
    ###################################

    gasEntropy = np.log( gasTemp*(gasDens )**(-2./3.) ) # rhoity in Mo/kpc3
    x, y, sig = running_median( np.log10(gasCoordR[gasRmask]), gasEntropy[gasRmask], 23)
    axs[1].errorbar(10**x/1000.,y, yerr=sig, c=color)
    axs[1].set_xlim(0.01, 10)
    #axs[3].set_ylim(5e3, 1e8)
    axs[1].semilogx()
    axs[1].set_xlabel('r [Mpc]')
    axs[1].set_ylabel('Entropy')
    #axs[3].semilogy()
    
    ###################################
    ###  Velocity histograms   ########
    ###################################

    H, edges = np.histogram(np.log10(dmModV), bins=100, density=True)
    axs[3].plot( 10**(0.5*(edges[1:]+edges[:-1])), H , color+':' )
    axs[3].text( 0.7, 0.7, 'dm: %.1f km/s'%(10**edges[np.argmax(H)]), transform=axs[3].transAxes)
#    axs[3].axvline(10**edges[np.argmax(H)], color=color, linestyle='-')

    H, edges = np.histogram(np.log10(gasModV), bins=100, density=True)
    axs[3].plot( 10**(0.5*(edges[1:]+edges[:-1])), H  , color+'-')
    axs[3].text( 0.7, 0.6, 'gas: %.1f km/s'%(10**edges[np.argmax(H)]), transform=axs[3].transAxes)
#    axs[3].axvline(10**edges[np.argmax(H)], color=color, linestyle='--')

    axs[3].loglog()


    ###################################
    #####  Projected temperature  #####
    ###################################
    Nbins = 70

    H, xedges, yedges = np.histogram2d(gasx, gasy, bins=Nbins)
    Htemp, _, _ = np.histogram2d(gasx, gasy, bins=[xedges, yedges], weights=gasTemp)
    im = im_axs[0].imshow(np.log10(Htemp/H).T, origin='lower', extent=(-32,32,-32,32), vmax=8, vmin=3)
    #im = im_axs[0].imshow(np.log10(H).T, origin='lower')
    #_,_,_,im = im_axs[0].hist2d(gasx, gasy, bins=50, vmax=vmax, vmin=0)
    plt.colorbar(im, ax=im_axs[0], label='log T')
    im_axs[0].set_xlabel('x')
    im_axs[0].set_xlabel('y')
    im_axs[0].plot(CMx/1e3, CMy/1e3, 'r.')

    H, xedges, yedges = np.histogram2d(gasz, gasy, bins=Nbins)
    Htemp, _, _ = np.histogram2d(gasz, gasy, bins=[xedges, yedges], weights=gasTemp)
    im = im_axs[1].imshow(np.log10(Htemp/H).T, origin='lower', extent=(-32,32,-32,32), vmax=8, vmin=3)
    #_,_,_,im = im_axs[1].hist2d(gasz, gasy, bins=100, vmax=vmax, vmin=0) #, range=[[-histSize+gasCM[2],histSize+gasCM[2]],[-histSize+gasCM[1],histSize+gasCM[1]]], vmax=vmax, vmin=0)
    plt.colorbar(im, ax=im_axs[1], label='log T')
    im_axs[1].set_xlabel('z')
    im_axs[1].plot(CMz/1e3, CMy/1e3, 'r.')


    ###################################
    #####  Projected  density     #####
    ###################################

    H, xedges, yedges = np.histogram2d(gasx, gasy, bins=Nbins)
    im = im_axs[2].imshow(np.log10(H).T, origin='lower', extent=(-32,32,-32,32), vmin=0.5, vmax=3.6)
    #_,_,_,im = im_axs[2].hist2d(dmx, dmy, bins=100, vmax=vmax, vmin=0) #, range=[[-histSize+gasCM[0],histSize+gasCM[0]],[-histSize+gasCM[1],histSize+gasCM[1]]], vmax=vmax, vmin=0)
    plt.colorbar(im, ax=im_axs[2], label=r'log $\rho_{gas}$')
    im_axs[2].set_xlabel('x')
    im_axs[2].plot(CMx/1e3, CMy/1e3, 'r.')

    H, xedges, yedges = np.histogram2d(gasz, gasy, bins=Nbins)
    im = im_axs[3].imshow(np.log10(H).T, origin='lower', extent=(-32,32,-32,32),  vmin=0.5, vmax=3.6)
    #_,_,_,im = im_axs[3].hist2d(dmz, dmy, bins=100, vmax=vmax, vmin=0) #, range=[[-histSize+gasCM[2],histSize+gasCM[2]],[-histSize+gasCM[1],histSize+gasCM[1]]], vmax=vmax, vmin=0)
    plt.colorbar(im, ax=im_axs[3], label=r'log $\rho_{gas}$')
    im_axs[3].set_xlabel('z')
    im_axs[3].plot(CMz/1e3, CMy/1e3, 'r.')


















def run(pkdfile, arepofile, path=None):
    fig, [axs,axs2,axs3] = plt.subplots(3,4, sharex=False, figsize=(15,10))

    fig.suptitle('z = -0.0196 \n Black: pkdgrav3 ; Blue: arepo \n Second row: pkdgrav3 ; Third row: arepo')
    fig.subplots_adjust(top=0.9, left=0.05, right=0.95, wspace=0.2, hspace=0.2)


    ##############################
    #PKDGRAV3
    ##############################
    KBOLTZ =   1.38e-16
    MHYDR =  1.67e-24
    KPCCM =  3.085678e21
    MSOLG =  1.99e33
    mu = 0.53 # Fully ionized primordial abundances

    import h5py
    snap = h5py.File(pkdfile, 'r')
    #snap = h5py.File('snaps_virus_0102_fixedNeighs/sb.00015.0')
    gas = snap['PartType0']
    dark = snap['PartType1']


    a = snap['Header'].attrs['Time']
    u = units.units(snap['Units'].attrs['MsolUnit'], snap['Units'].attrs['KpcUnit'])

    dmx= dark['Coordinates'][:,0]*u.dKpcUnit
    dmy= dark['Coordinates'][:,1]*u.dKpcUnit
    dmz= dark['Coordinates'][:,2]*u.dKpcUnit

    gasx = gas['Coordinates'][:,0]*u.dKpcUnit
    gasy = gas['Coordinates'][:,1]*u.dKpcUnit
    gasz = gas['Coordinates'][:,2]*u.dKpcUnit

    mask = np.abs(gasz) < 2000.
    gasMass = gas['Masses'][0]*u.dMsolUnit
    gasDens = gas['Density']/a**3 * (u.dMsolUnit) / (u.dKpcUnit/1000.)**3 
    gasTemp = gas['InternalEnergy'][...]  / u.dGasConst * mu* (5./3.-1.)
    minPhi = np.argmin(dark['Potential'][mask])
    #minPhi = np.argmax(dark['density'][mask])
    CMx = dmx[mask][minPhi]
    CMy = dmy[mask][minPhi]
    CMz = dmz[mask][minPhi]

    dmModV = np.sqrt(dark['Velocities'][:,0]**2 + dark['Velocities'][:,1]**2  + dark['Velocities'][:,2]**2) * u.dKmPerSecUnit
    gasModV = np.sqrt(gas['Velocities'][:,0]**2 + gas['Velocities'][:,1]**2  + gas['Velocities'][:,2]**2) * u.dKmPerSecUnit

    dmMass = dark['Masses'][0]*u.dMsolUnit

    plot_all(axs, axs2, 'k',dmx, dmy, dmz, gasx, gasy, gasz, dmMass, gasMass, gasDens, gasTemp, CMx, CMy, CMz, dmModV, gasModV)

    del dmx, dmy, dmz, gasx, gasy, gasz, gasMass, gasDens, gasTemp, gasModV, dmModV


    ################################
    # AREPO REFERENCE
    ################################

    f = h5py.File(arepofile, 'r')

    # DM
    dmpos = f['PartType1']['Coordinates'][...]/0.5 - 32e3

    gasMass = f['Header'].attrs['MassTable'][0]*1e10/0.5
    gasMass = f['PartType0/Masses'][...]*1e10/0.5
    dmMass = f['Header'].attrs['MassTable'][1]*1e10/0.5

    minPhi = np.argmin(f['PartType1/Potential'][...])
    CMx = dmpos[minPhi,0]
    CMy = dmpos[minPhi,1]
    CMz = dmpos[minPhi,2]

    # Gas
    gaspos = f['PartType0']['Coordinates'][...]/0.5 - 32e3
    gasDens = f['PartType0']['Density'][...]/a**3 *(1e10/0.5) / (1/0.5 / 1000.)**3
    gasTemp = f['PartType0']['InternalEnergy'][...]*(1e5)**2*(5./3. - 1.)*mu/KBOLTZ*MHYDR

    dmModV = np.sqrt(f['PartType1/Velocities'][:,0]**2 + f['PartType1/Velocities'][:,1]**2 + f['PartType1/Velocities'][:,2]**2 ) 
    gasModV = np.sqrt(f['PartType0/Velocities'][:,0]**2 + f['PartType0/Velocities'][:,1]**2 + f['PartType0/Velocities'][:,2]**2 ) 

    plot_all(axs, axs3, 'b',dmpos[:,0], dmpos[:,1], dmpos[:,2],gaspos[:,0], gaspos[:,1], gaspos[:,2], dmMass, gasMass, gasDens, gasTemp, CMx, CMy, CMz, dmModV, gasModV)


    if path is not None:
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], 'snaps_arepo/snap_016.hdf5', path=sys.argv[2])
    else:
        run(sys.argv[1], 'snaps_arepo/snap_016.hdf5')

