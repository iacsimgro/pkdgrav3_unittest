from pkdtest import PKDGRAV3TestCase


class EvrardTest(PKDGRAV3TestCase):
    title = "Evrard collapse"
    description = ("This test describes the collapse of "
                      "a gaseous sphere with negligible "
                      "initial internal energy. "
                      "A strong shock propagating outwards "
                      "appears, with its characteristic  "
                      "entropy increase. "
                      "\n"
                      "The energy will not be conserved to "
                      "machine accuracy due to the approximated "
                      "gravity solver, but shold be bounded and "
                      "close to zero.")
    plots = [{"file": "evrard_MFM",
              "caption": "Evrard solution with MFM"},
             {"file": "evrard_MFV",
              "caption": "Evrard solution with MFV"},
             {"file": "evrard_energy",
              "caption": "Conservation properties of the Evrard collapse"}]

    schemes = ["MFM", "MFV"]
    __name__ = "Evrard"
    folder_name = "evrard"


    def generateIC(self):
        import h5py,shutil


        # We took the ICs from Hopkins repo
        f = h5py.File(self.pkdgrav3_test_dir+'/evrard_ics.hdf5', 'r+')


        # Force PKDGRAV3 to recompute the softening lenghts
        f['PartType0/SmoothingLength'][...] *= 0.

        f.close()

        shutil.copy(self.pkdgrav3_test_dir+'/evrard_ics.hdf5', self.input_file)


    def test_evrard(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/evrard.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()
        for scheme in self.schemes:
            self.set_scheme(scheme)
            self.compile()

            self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_%s'%(scheme, self.identifier)
            self.output_file = self.output_dir + '/evrard'
            self.run_pkdgrav3()

            self.plot(scheme)

        self.plot_energy()


    def plot(self, scheme):
        from evrard.plot import run
        run(self.output_file+'.00016.0', path=self.pkdgrav3_test_dir + '/evrard_%s_%s.png'%(scheme, self.identifier))

    def plot_energy(self):
        from evrard.energy import run
        folders = [self.pkdgrav3_test_dir + '/snaps_%s_%s'%(scheme, self.identifier) for scheme in self.schemes]
        run(folders, path=self.pkdgrav3_test_dir+'/evrard_energy_%s.png'%(self.identifier) )


