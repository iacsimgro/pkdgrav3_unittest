def run(snapname, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    from pykdgrav3_utils import hdf5


    snap = hdf5.read_single(snapname, return_units=False)
    gas = snap['PartType0']

    gamma = 5./3.

    x = gas['Coordinates'][:,0]
    y = gas['Coordinates'][:,1]
    z = gas['Coordinates'][:,2]
    dens = gas['Density'][...]
    entropy = gas['InternalEnergy'][...]*dens*(gamma-1)/dens**(5./3.)

    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arccos(z/r)
    phi = np.arctan2(y, x)
    v_r = np.sin(theta) * np.cos(phi) * gas['Velocities'][:,0] + np.sin(theta) * np.sin(phi) * gas['Velocities'][:,1]   + np.cos(theta) * gas['Velocities'][:,2]

    size = .1

    f,axs = plt.subplots(1,3, sharex=False, figsize=(14,5))

    mask = r > 1e-3
    print (np.polyfit(np.log10(r[mask]), np.log10(dens[mask]), 1))

    ext_r,ext_rho = np.loadtxt('/'.join(snapname.split('/')[:-2])+'/extracted_rho.csv', unpack=True)

    axs[0].scatter(r[mask], (dens[mask]), marker='.', s=size, c='k')
    axs[0].plot(ext_r, ext_rho, 'r--')
    axs[0].set_xlabel('r')
    axs[0].semilogx()
    axs[0].semilogy()
    axs[0].set_ylabel(r'$\rho$')


    ext_r,ext_ent = np.loadtxt('/'.join(snapname.split('/')[:-2])+'/extracted_entropy.csv', unpack=True)
    axs[1].scatter(r[mask], entropy[mask], marker='.', s=size, c='k')
    #axs[1].scatter(r[mask], gas['mass'][mask], marker='.', s=size, c='k')
    axs[1].plot(ext_r, ext_ent, 'r--')
    axs[1].set_xlabel('r')
    axs[1].semilogx()
    axs[1].set_ylabel(r'Entropy')

    ext_r,ext_vr = np.loadtxt('/'.join(snapname.split('/')[:-2])+'/extracted_vr.csv', unpack=True)


    intEnergy = ( gas['InternalEnergy'][...]*gas['Masses'][...] )
    kinEnergy = ( 0.5*gas['Masses'][...]*(gas['Velocities'][:,0]**2 + gas['Velocities'][:,1]**2 + gas['Velocities'][:,2]**2) )
    gravEnergy = 0.5*( gas['Potential'][...]*gas['Masses'][...] )

    #axs[2].scatter(r[mask], intEnergy[mask], marker='.', s=size, c='b')
    #axs[2].scatter(r[mask], kinEnergy[mask], marker='.', s=size, c='g')
    #axs[2].scatter(r[mask], gravEnergy[mask], marker='.', s=size, c='r')
    #axs[2].scatter(r[mask], intEnergy[mask]/kinEnergy[mask], marker='.', s=size, c='k')
    #axs[2].semilogy()

    #axs[2].scatter(r[mask], gas['mass'][mask], marker='.', s=size, c='k')
    print (np.min(gas['Masses']))
    axs[2].scatter(r[mask], v_r[mask], marker='.', s=size, c='k')
    axs[2].plot(ext_r, ext_vr, 'r--')
    axs[2].semilogx()
    axs[2].set_ylabel(r'$v_r$')
    axs[2].set_xlabel('r')

    # We show a density 'cut' near x=0
    #mask = np.abs(z) < 1./32
    #im = axs[3].tricontourf(x[mask], y[mask], (gas['temp'][mask])) #np.log10(temp[mask]))
    #plt.colorbar(im, ax=axs[3], label=r'$\log_{10} (p)$')
    #axs[3].scatter(x[mask], y[mask], c='k', s=0.1, marker='.')

    plt.tight_layout()

    if path is not None:
        print (path)
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], sys.argv[2])
    else:
        run(sys.argv[1])
