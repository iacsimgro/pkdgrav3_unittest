from pkdtest import PKDGRAV3TestCase
from evrard.test_evrard import EvrardTest


class dhMinOverSoftTest(EvrardTest):
    title = "dhMinOverSoft test"
    description = ("Test the correct handling of dhMinOverSoft")
    plots = [{"file": "dhMinOverSoft",
              "caption": "Behaviour of dhMinOverSoft"}]

    schemes = ["MFM"]
    __name__ = "dhMinOverSoft"
    folder_name = "evrard"

    soft = 0.05
    dhMinOverSoft = 0.5

    def test_dhMinOverSoft(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/evrard.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()
        self.set_scheme('MFM')
        self.compile()

        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/evrard'
        self.extra_args = '-n 0 -e 0.05 -hmin 0.5'
        self.run_pkdgrav3()

        self.plot()

    def plot(self):
        from pykdgrav3_utils import hdf5
        import numpy as np
        import matplotlib.pyplot as plt

        snap = hdf5.read_single(self.output_file+'.00000.0', return_units=False)
        gas = snap['PartType0']
        x = gas['Coordinates'][:,0]
        y = gas['Coordinates'][:,1]
        z = gas['Coordinates'][:,2]
        r = np.sqrt(x**2 + y**2 + z**2)

        smooth = gas['SmoothingLength'][...]

        size = 1

        f,ax = plt.subplots(1, figsize=(5,4))

        ax.axhline(self.soft, ls='--', c='r', label='dSoft')
        ax.axhline(self.soft*self.dhMinOverSoft, ls=':', c='r', label=r'dSoft $\times$ dhMinOverSoft')

        ax.scatter(r, smooth, marker='.', s=size, c='k', rasterized=True)

        plt.legend()
        ax.set_xlabel(r'$r$')
        ax.set_ylabel(r'Smoothing length')

        plt.savefig(self.pkdgrav3_test_dir + '/dhMinOverSoft_%s.png'%(self.identifier))
        plt.show()


