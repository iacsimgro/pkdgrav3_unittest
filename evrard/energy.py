def run(folders, path=None):
    import numpy as np
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import glob
    from pykdgrav3_utils import hdf5


    import matplotlib
    import matplotlib.ticker as mticker


    ls = ['-.', ':', '--']


    fig,ax = plt.subplots(1,2, figsize=(11,5))

    f=0
    for folder in folders:

        files = sorted(glob.glob(folder+'/evrard.00???.0'))

        Nfiles = len(files)

        totalMass = np.zeros(Nfiles)
        intEnergy = np.zeros(Nfiles)
        kinEnergy = np.zeros(Nfiles)
        hydEnergy = np.zeros(Nfiles)
        gravEnergy = np.zeros(Nfiles)
        angMom = np.zeros(Nfiles)
        linMom = np.zeros(Nfiles)
        totalEnergy = np.zeros(Nfiles)
        t = np.zeros(Nfiles)

        gamma = 5./3.
        dt = 5e-2

        i = 0
        for fn in files:
           t[i] = float(fn.split('.')[-2]) * dt
           snap = hdf5.read_single(fn, return_units=False)
           gas = snap['PartType0']
           if "DM" in fn:
               del gas
               gas = dark


           x = gas['Coordinates'][:,0]
           y = gas['Coordinates'][:,1]
           z = gas['Coordinates'][:,2]
           vx = gas['Velocities'][:,0]
           vy = gas['Velocities'][:,1]
           vz = gas['Velocities'][:,2]
           totalMass[i] = np.sum(gas['Masses'])

           if ("DM" in fn) or ("noflux" in fn):
               intEnergy[i] = 0.0
           else:
               intEnergy[i] = np.sum( gas['InternalEnergy'][...]*gas['Masses'][...] )
           kinEnergy[i] = np.sum( 0.5*gas['Masses'][...]*(vx**2 + vy**2 + vz**2) )
           gravEnergy[i] = 0.5*np.sum( gas['Potential'][...]*gas['Masses'][...] )  


           r = np.sqrt(x**2 + y**2)
           theta = np.arctan2(y,x)

           vphi = -np.sin(theta)*vx + np.cos(theta)*vy
           angMom[i] = np.sum(gas['Masses'][...]*r*vphi)
           linMom[i] = np.sum(gas['Masses'][...]*(vx+vy+vz))


           i += 1

        if (intEnergy[0] == intEnergy[1]):
            intEnergy *= 0.
            print ("noflux")

        totalEnergy = kinEnergy+gravEnergy+intEnergy


        #print (totalMass)

        # Relative change
        totalMass /= totalMass[0]
        #intEnergy /= intEnergy[0]
        #kinEnergy /= kinEnergy[0]
        deltaTotalEnergy = (totalEnergy-totalEnergy[0])/np.abs(totalEnergy[0])


        ax[0].plot(t, totalMass, c='m',linestyle=ls[f])

        ax[0].plot(t, gravEnergy, c='r',linestyle=ls[f])

        ax[0].plot(t, kinEnergy, c='g' ,linestyle=ls[f])

        ax[0].plot(t, intEnergy, c='b',linestyle=ls[f])

        ax[0].plot(t, totalEnergy, c='k',linestyle=ls[f])

        ax[1].plot(t, deltaTotalEnergy, 'k',linestyle=ls[f], label=r'%s'%folder.split('/')[-1][6:].replace('_', ' '))

        f+=1



    # Dummy plots for the legends
    ax[0].plot([], [], 'r', label='Potential')
    ax[0].plot([], [], 'g', label='Kinetic')
    ax[0].plot([], [], 'b', label='Internal')
    ax[0].plot([], [], 'k', label='Total')
    ax[0].legend(loc=0, fancybox=True)


    ax[1].legend(loc=0, fancybox=True)



    ax[0].set_ylabel(r'Energy')
    ax[1].set_ylabel(r'$ \Delta E_{tot}/|E_{tot,0}|$')

    ax[0].set_xlabel(r'Time')
    ax[0].set_xlim(0.,2.5)
    ax[1].set_xlabel(r'Time')
    ax[1].set_xlim(0.,2.5)

    plt.tight_layout()

    if path is not None:
        print (path)
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if sys.argv[:-1][:-4]=='.png':
        run(sys.argv[1:-1], path=sys.argv[-1])
    else:
        run(sys.argv[1:])
