import os
import numpy as np

def run(ident,testdir='', ngbs=np.array([32]), N=np.array([16,21,64,128]), path=None, theta=0.0):
    from pykdgrav3_utils import hdf5

    import matplotlib
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker




    f, ax = plt.subplots(1,figsize=(9,5.0))


    A = 1e-3
    gamma = 1.4

    L1 = np.zeros((len(N),len(ngbs)), dtype=np.double)
    L1 = np.zeros((len(N),len(ngbs)), dtype=np.double)

    var = 'Density'

    i=0
    for n in N:
        j=0
        for ngb in ngbs:
            snap = hdf5.read_single(testdir+"/snaps_MFV_N%d_ngb%d_%s/soundwaves.00001.0"%(n, ngb, ident), return_units=False)
            gas = snap['PartType0']
            x = snap['PartType0/Coordinates'][:,0] #- np.min(gas['x'])
            meanrho = np.mean(gas[var]) 
            totN = len(x)

            snap0 = hdf5.read_single(testdir+"/snaps_MFV_N%d_ngb%d_%s/soundwaves.00000.0"%(n, ngb, ident), return_units=False)
            gas0 = snap0['PartType0']
            meanrho0 = np.mean(gas0[var]) 
            rho_an0  = (gas0[var]-meanrho0)

            rho_pert = (gas[var]-meanrho)/meanrho0

            rho_an = (A*np.cos(2.*np.pi*(x) + np.pi/2. - theta) )#*np.sqrt(1.4)
            diff = np.abs(rho_pert-rho_an)
            L1[i,j] = np.sum(diff)/totN

    #        ax.plot(x, rho_an-rho_an0, '.')
            ax.plot(x, rho_pert, 'o', label=r'$N=%d$'%n, ms=3)

            j+=1
        i+=1

    x = np.linspace(-0.5,0.5,100)
    rho_an = (A*np.cos(2.*np.pi*(x) + np.pi/2. - theta) )#*np.sqrt(1.4)
    ax.plot(x, rho_an, 'k--', alpha=0.9)

    plt.legend(loc=0, fancybox=True,fontsize=14, title=r'$N_\mathrm{NGB}$=32')

    ax.set_xticks([-0.5,-0.25,0,0.25,0.5])
    ax.set_ylabel(r'$(\rho_i-\bar{\rho})/\bar{\rho}$')
    ax.set_xlabel(r'x')
    ax.set_ylim(-0.0011,0.0011)

    if path is not None:
        print (path)
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    if len(sys.argv) > 2:
        run('', os.environ.get('IDENTIFIER'), sys.argv[2])
    else:
        run('', os.environ.get('IDENTIFIER'))
