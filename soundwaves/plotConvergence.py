import os
import numpy as np

def run(ident, ngbs=np.array([32]), N=np.array([16,21,64,128]), testdir='', path=None, theta=0.0):

    import matplotlib
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    from pykdgrav3_utils import hdf5

    A = 1e-3
    gamma = 1.4


    var = 'Density'


    def read_computeL1(folder):
        L1 = np.zeros((len(N),len(ngbs)), dtype=np.double)
        i=0
        for n in N:
            j = 0
            for ngb in ngbs:
                snap = hdf5.read_single("%s_N%d_ngb%d_%s/soundwaves.00001.0"%(folder, n, ngb, ident), return_units=False)
                gas = snap['PartType0']
                x = gas['Coordinates'][:,0] #- np.min(gas['x'])
                meanrho = np.mean(gas[var]) 
                totN = len(x)

                snap0 = hdf5.read_single("%s_N%d_ngb%d_%s/soundwaves.00000.0"%(folder, n, ngb, ident), return_units=False)
                gas0 = snap0['PartType0']
                meanrho0 = np.mean(gas0[var]) 
                rho_an0  = (gas0[var]-meanrho0)

                rho_pert = (gas[var]-meanrho)/meanrho0

                rho_an = (A*np.cos(2.*np.pi*(x) + np.pi/2. - theta) )#*np.sqrt(1.4)
                diff = np.abs(rho_pert-rho_an)
                L1[i,j] = np.sum(diff)/totN
                
                j+=1
            i+=1

        return L1

    L1_MFM = read_computeL1(testdir+'/snaps_MFM')

    L1_MFV = read_computeL1(testdir+'/snaps_MFV')









    f, ax = plt.subplots(1,figsize=(6,5.5))

    cs = ['b']
    for i in range(len(ngbs)):
        fit_MFM = np.polyfit(np.log(N), np.log(L1_MFM[:,i]), 1)
        fit_MFV = np.polyfit(np.log(N), np.log(L1_MFV[:,i]), 1)

        ax.plot(N, np.exp(np.polyval(fit_MFM, np.log(N))), 'g--', alpha=0.9)
        ax.plot(N, L1_MFM[:,i], 'g*', ms=10, label=r'MFM$_{\mathrm{no-lim}}$ - $L_1 \propto N^{%.2f}$'%fit_MFM[0])

        ax.plot(N, np.exp(np.polyval(fit_MFV, np.log(N))), 'b--', alpha=0.9)
        ax.plot(N, L1_MFV[:,i], 'b*', ms=10, label=r'MFV$_{\mathrm{no-lim}}$ - $L_1 \propto N^{%.2f}$'%fit_MFV[0])

    # Guide lines
    x_guide = np.array([20.,50.])
    ax.plot(x_guide, 2e-2*x_guide**(-2), 'k:', alpha=0.5)
    ax.text(0.2, 0.3, r'$L_1 \propto N^{-2}$', transform=ax.transAxes, fontsize=17)
    ax.plot(x_guide+20, 1.5e-2*(x_guide+20)**(-1.5), 'k:', alpha=0.5)
    ax.text(0.55, 0.65, r'$L_1 \propto N^{-1.5}$', transform=ax.transAxes, fontsize=17)
    ax.set_ylim(2e-6, 2e-4)

    ax.loglog()
    ax.set_xticks(N)
    ax.set_xticklabels(['%d'%i for i in N])
    ax.set_xlabel(r"$N$")
    ax.xaxis.set_major_formatter(mticker.ScalarFormatter())
    ax.xaxis.set_minor_formatter(mticker.NullFormatter())

    ax.set_ylabel(r'$L_1 = \frac{1}{N}\sum_i | \frac{\rho_i-\bar{\rho}}{\bar{\rho}} - \rho (x_i) + 1|$')

    ax.legend(loc=0, fancybox=True, fontsize=16)

    plt.tight_layout()

    if path is not None:
        print (path)
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    if len(sys.argv) > 2:
        run(os.environ.get('IDENTIFIER'), path=sys.argv[2])
    else:
        run(os.environ.get('IDENTIFIER'))

