from pkdtest import PKDGRAV3TestCase


class SoundwavesTest(PKDGRAV3TestCase):
    title = "Soundwaves convergence test"
    description = ("The code should be second order accurate, "
                    "however, as we are working in 3D and with "
                    "single precision, the convergence rate can "
                    "be slighlty lower than 2")
    plots = [{"file": "soundwaves_convergence",
              "caption": "Convergence plot"},
             {"file": "soundwaves_solution",
              "caption": "Solution for MFM"}]

    Ns = [16,24,32,48,64,86]
    ngbs = [32]
    schemes = ["MFM", "MFV"]
    __name__ = "Soundwaves"
    folder_name = "soundwaves"

    max_threads = 1
    compile_flags = {'DEBUG_FLUX_NOLIMITER':True}

    def generateIC(self,n):
        import numpy as np
        from pykdgrav3_utils import hdf5
        n2 = 4
        N=n*n2*n2


        L = 1.
        L2 = n2/n * L
        x = np.linspace(-L/2., L/2., num=n, endpoint=False)
        y = np.linspace(-L2/2., L2/2., num=n2, endpoint=False)
        mesh = np.meshgrid(x,y,y)

        Nwave = 1
        phi = 2.*Nwave*np.pi*mesh[0] + np.pi/2
        A = 1e-3

        P0 = 1.
        T0 = (1.)
        mass0 = L*L2*L2/N # Mass for rho=1
        gamma = 1.4
        cs = np.sqrt(gamma)
        mass = np.ones_like(mesh[0])*mass0
        # Small perturbation to the mass, to account for the increase in rho
        mass *= (1.+A*np.cos(phi))
        dens = np.ones_like(mesh[0]) # This should not change anything
        dens *= (1. + A*np.cos(phi))
        P = np.ones_like(mesh[0])*P0
        P *= (1. + gamma*A*np.cos(phi))
        Uint = P/dens / (gamma - 1.)
        vx = cs*A*np.cos(phi)
        vx = vx.flatten()

        pos = np.stack((mesh[0].flatten(), mesh[1].flatten(), mesh[2].flatten()), axis=1)

        gas_dict = {'Coordinates': pos,
                    'Masses': mass.flatten(),
                    'Velocities': np.stack([vx, np.zeros_like(vx), np.zeros_like(vx)], axis=1),
                    'InternalEnergy':Uint.flatten(),
                    'SmoothingLength': np.full( N, np.power(3.*L*L2*L2/(4.*np.pi*N), 1./3.))}

        hdf5.save({'PartType0':gas_dict}, self.input_file)
        return L2


    def test_soundwaves(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/soundwaves.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        for scheme in self.schemes:
            self.set_scheme(scheme)
            self.compile()
            for n in self.Ns:
                L = self.generateIC(n)

                for ngb in self.ngbs:
                    self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_N%d_ngb%d_%s'%(scheme, n, ngb, self.identifier)
                    self.output_file = self.output_dir + '/soundwaves'
                    self.test_args = '-s %d -L 1.,%.10f,%.10f'%(ngb,L,L)
                    self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from soundwaves.plotConvergence import run
        run(self.identifier, ngbs=self.ngbs, N=self.Ns, testdir=self.pkdgrav3_test_dir, path=self.pkdgrav3_test_dir+"/soundwaves_convergence_%s.png"%self.identifier)

        from soundwaves.plotSolution import run
        run(self.identifier, ngbs=self.ngbs, N=self.Ns, testdir=self.pkdgrav3_test_dir, path=self.pkdgrav3_test_dir+"/soundwaves_solution_%s.png"%self.identifier)

