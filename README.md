# Unit testing for the hydrodynamics

This series of tests are devoted to check the correctness of PKDGRAV3 as
more changes are done.

This, in principle, should avoid the silent addition of bugs which could degrade,
or completely destroy, the solution of previously working test cases.

# Usage

All the test must be run using pkdruntest.py, please use `--help` to see the usage information.
In addition, the required submodules must be pulled from their repositories, to do so:

```
git submodule update --init
```

# Important options

Some options are important to understand, thus are described below:

* `-c,--cpus`: Number of CPUs to use per test. Some test may have a hardcoded upper
limit to this quantity, but in general, the more cores the faster the test suite will run.

* `-j,--jobs`: Number of test being run in parallel. Each one of them will have `-c` number of
CPUs being used. For example, in a 16 core machine we could run with `-c 4 -j 4` to use all the
available cores.

* `-i,--identifier`: Name to distinguish this test run. It can be whatever you like, but if will
overwrite previous results with the same identifier!

* `--report`: When active, an HTML report is generated after all the requested tests were run.
In this report, it can be easily compared the test result with a reference solution just hovering
the mouse over the images.


# Verbosity

In general the code will not be very verbose, but all the outputs of cmake/make and pkdgrav3
are appended to files in each test directory with the name `TEST/log_IDENTIFIER`.
For example, with `-i test Soundwaves` we could take a look on the progress of the test with:
```
tail -f soundwaves/log_test
```

