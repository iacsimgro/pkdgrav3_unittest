def run(folder, path=None):
    import numpy as np
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import glob
    from pykdgrav3_utils import hdf5

    files = sorted(glob.glob(folder+'.?????.0'))

    Nfiles = len(files)

    totalMass = np.zeros(Nfiles)
    intEnergy = np.zeros(Nfiles)
    kinEnergy = np.zeros(Nfiles)
    angMom = np.zeros(Nfiles)
    linMom = np.zeros(Nfiles)
    totalEnergy = np.zeros(Nfiles)
    t = np.zeros(Nfiles)

    gamma = 1.4  
    dt = 0.1

    i = 0
    for f in files:
       t[i] = float(f.split('.')[-2]) * dt
       snap = hdf5.read_single(f, return_units=False)
       gas = snap['PartType0']

       #x = (gas['x']-1.*t[i]) % 0.5
       x = gas['Coordinates'][:,0]
       y = gas['Coordinates'][:,1]
       z = gas['Coordinates'][:,2]
       vx = gas['Velocities'][:,0]
       vy = gas['Velocities'][:,1]
       vz = gas['Velocities'][:,2]
       mass = gas['Masses'][...]

       totalMass[i] = np.sum(mass)

       intEnergy[i] = np.sum( gas['InternalEnergy']*mass )
       kinEnergy[i] = np.sum( 0.5*mass*(vx**2 + vy**2 + vz**2) )


       r = np.sqrt(x**2 + y**2)
       theta = np.arctan2(y,x)

       vphi = -np.sin(theta)*vx + np.cos(theta)*vy
       angMom[i] = np.sum(mass*r*vphi)
       linMom[i] = np.sum(mass*(vx+vy+vz))

       snap.close()

       i += 1

    totalEnergy = kinEnergy+intEnergy

    f,ax = plt.subplots(1,4, figsize=(11.5,3))


    # Relative change
    totalMass /= totalMass[0]
    #intEnergy /= intEnergy[0]
    #kinEnergy /= kinEnergy[0]
    #totalEnergy /= totalEnergy[0]




    ax[0].plot(t, totalMass, 'r.-')
    ax[0].set_ylabel('Total Mass')
    ax[0].set_xlabel('Time')

    ax[1].plot(t, linMom, 'g.-')
    ax[1].set_ylabel('Linear momentum')
    ax[1].set_xlabel('Time')

    ax[2].plot(t, angMom, 'b.-')
    ax[2].set_ylabel('Angular momentum')
    ax[2].set_xlabel('Time')

    ax[3].plot(t, totalEnergy, 'k.--')
    ax[3].set_ylabel('Total Energy')
    ax[3].set_xlabel('Time')

    plt.tight_layout()

    if path is not None:
        f.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], path=sys.argv[2])
    else:
        run(sys.argv[1])
