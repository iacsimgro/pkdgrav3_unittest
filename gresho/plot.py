def run(snapname, path=None):
    import numpy as np
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    from pykdgrav3_utils import hdf5


    snap = hdf5.read_single(snapname, return_units=False)
    gas = snap['PartType0']


    gamma = 1.4
    x = gas['Coordinates'][:,0]
    y = gas['Coordinates'][:,1]
    z = gas['Coordinates'][:,2]
    vx = gas['Velocities'][:,0]
    vy = gas['Velocities'][:,1]
    press = gas['InternalEnergy'][...] * gas['Density'] * (gamma - 1.)
    mass = gas['Masses'][...]

    r = np.sqrt(x**2 + y**2)
    theta = np.arctan2(y,x)

    vphi = -np.sin(theta)*vx + np.cos(theta)*vy
    v_r =  np.cos(theta) * vx+  np.sin(theta) * vy  

    r_theory = np.linspace(1e-3, .7, 40)
    vphi_theory = np.zeros_like(r_theory)
    vphi_theory [r_theory<0.4] = 2. - 5.*r_theory[r_theory<0.4]
    vphi_theory [r_theory<0.2] = 5*r_theory[r_theory<0.2]
    p_theory = np.ones_like(r_theory)*(3.+4.*np.log(2.))
    p_theory[r_theory<0.4] = 9 + 12.5*r_theory[r_theory<0.4]**2 - 20.*r_theory[r_theory<0.4] + 4* np.log(5.*r_theory[r_theory<0.4]) 
    p_theory[r_theory<0.2] = 5 + 12.5*r_theory[r_theory<0.2]**2

    f, ax = plt.subplots(2,3, figsize=(12,7))

    ax[0][0].plot(r_theory, vphi_theory, 'r--')
    ax[0][1].plot(r_theory, p_theory, 'r--')


    print (np.mean(gas['Density']))

    alpha = 0.4

    ax[0][0].scatter(r, vphi, s=0.1, c='k', marker='.', alpha=alpha)
    ax[0][0].set_xlabel('r')
    ax[0][0].set_ylabel(r'$v_\varphi$')
    ax[0][0].set_xlim(0.,0.8)
    ax[0][0].set_ylim(-0.1,1.1)

    ax[0][1].scatter(r, press, s=0.1, c='k', marker='.', alpha=alpha)
    ax[0][1].set_xlabel('r')
    ax[0][1].set_ylabel('Pressure')
    ax[0][1].set_xlim(0.,0.8)
    ax[0][1].set_ylim(4.9,5.9)

    v = ax[1][0].tricontourf(x, y, vphi, vmin=-0.1, vmax=1.)
    plt.colorbar(v, ax=ax[1][0])
    ax[1][0].set_title(r'$v_\varphi$')
    ax[1][0].set_xlabel('x')
    ax[1][0].set_ylabel('y')

    p = ax[1][1].tricontourf(x, y, press, vmin=5, vmax=5.9)
    plt.colorbar(p, ax=ax[1][1])
    ax[1][1].set_title('Pressure')
    ax[1][1].set_xlabel('x')
    ax[1][1].set_ylabel('y')

    #ax[0][2].scatter(r, gas['rho'], s=0.1, c='k', marker='.', alpha=alpha)
    ax[0][2].scatter(r, gas['SmoothingLength'], s=0.1, c='k', marker='.', alpha=alpha)
    ax[0][2].set_xlabel('r')
    ax[0][2].set_ylabel(r'$h$')
    ax[0][2].set_xlim(0.,0.8)
    ax[0][2].set_ylim(0.0150,0.0158)

    ax[1][2].scatter(r, mass*1e6, s=0.1, c='k', marker='.', alpha=alpha)
    ax[1][2].set_xlabel('r')
    ax[1][2].set_ylabel(r'Particle mass $\times 10^{6}$')
    ax[1][2].set_xlim(0.,0.8)
    ax[1][2].set_ylim(3.5, 4.3)

    plt.tight_layout()

    if path is not None:
        f.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], path=sys.argv[2])
    else:
        run(sys.argv[1])
