from pkdtest import PKDGRAV3TestCase


class GreshoTest(PKDGRAV3TestCase):
    title = "Gresho vortex"
    description = ("This test is aimed at checking the conservation of "
                    "angular momentum in the vortex. "
                    "Even as the circular velocity is smeared, the total "
                    "angular momentum should be conserved.")
    plots = [{"file": "gresho_solution_MFM",
              "caption": "Gresho solution with MFM"},
             {"file": "gresho_energy_MFM",
              "caption": "Energy conservation of the Gresho vortex with MFM"},
             {"file": "gresho_solution_MFV",
              "caption": "Gresho solution with MFV"},
             {"file": "gresho_energy_MFV",
              "caption": "Energy conservation of the Gresho vortex with MFV"}]



    schemes = ["MFM", "MFV"]
    __name__ = "Gresho"
    folder_name = "gresho"


    def generateIC(self):
        import numpy as np
        from pykdgrav3_utils import hdf5



        nz = 2
        nx = 64
        Lx = 1.
        Lz = nz/nx * Lx
        N = nz*nx**2

        x = np.linspace(-Lx/2, Lx/2, num=nx, endpoint=False)
        z = np.linspace(-Lz/2, Lz/2, num=nz, endpoint=False)
        mesh = np.meshgrid(x,x,z)
        """
        for i in range(nx):
            for j in range(nx):
                for k in range(nz):
                    mesh[0][i,j,k] = np.random.rand() - Lx/2
                    mesh[1][i,j,k] = np.random.rand() - Lx/2
                    mesh[2][i,j,k] = np.random.rand() - Lx/2
        """

        P0 = 1.
        gamma = 1.4
        mass0 = Lx*Lx*Lz/N # Mass for rho=1



        r = np.sqrt((mesh[0])**2 + (mesh[1])**2)
        theta = np.arctan2(mesh[1], mesh[0])


        mass = np.ones_like(mesh[0])*mass0

        dens = np.ones_like(mesh[0]) # This should not change anything

        P = np.ones_like(mesh[0])*P0
        mask1 = r<0.2
        mask2 = (r>=0.2) & (r<0.4)
        mask3 = r>=0.4
        P [mask1] = 5 + 12.5*r[mask1]**2
        P [mask2] = 9 + 12.5*r[mask2]**2 - 20.*r[mask2] + 4* np.log(5.*r[mask2])
        P [mask3] = 3. + 4.*np.log(2.)

        Uint = P/dens / (gamma - 1.)

        vphi = np.ones_like(mesh[0])
        vphi [mask1] = 5*r[mask1]
        vphi [mask2] = 2. - 5.*r[mask2]
        vphi [mask3] = 0.

        vx = -np.sin(theta)*vphi
        vy = np.cos(theta)*vphi



        pos = np.stack((mesh[0].flatten(), mesh[1].flatten(), mesh[2].flatten()), axis=1)
        vel = np.stack((vx.flatten(), vy.flatten(), np.zeros_like(vx.flatten())  ), axis=1)

        gas_dict = {'Coordinates': pos,
                    'Masses': mass.flatten(),
                    'Velocities': vel,
                    'InternalEnergy':Uint.flatten(),
                    'SmoothingLength': np.full( N, np.power(3.*Lx*Lx*Lz/(4.*np.pi*N), 1./3.))}

        hdf5.save({'PartType0':gas_dict}, self.input_file)
        return Lz



    def test_gresho(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/gresho.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        L = self.generateIC()
        for scheme in self.schemes:
            self.set_scheme(scheme)
            self.compile()

            self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_%s'%(scheme, self.identifier)
            self.output_file = self.output_dir + '/gresho'
            self.test_args = "-L 1.,1.,%f"%(L)
            self.run_pkdgrav3()

            self.plot(scheme)


    def plot(self, scheme):
        from gresho.plot import run
        run(self.output_file+'.00030.0', self.pkdgrav3_test_dir + '/gresho_solution_%s_%s.png'%(scheme, self.identifier))

        from gresho.energy import run
        run(self.output_file, self.pkdgrav3_test_dir + '/gresho_energy_%s_%s.png'%(scheme, self.identifier))


