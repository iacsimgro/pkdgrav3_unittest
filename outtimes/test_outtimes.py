from pkdtest import PKDGRAV3TestCase
import h5py
import numpy as np


class outtimesTest(PKDGRAV3TestCase):
    title = "OutTimes test"
    description = 'This test does not have figures, it just uses asserts'
    plots = []

    __name__ = "OutTimes"
    folder_name = "outtimes"

    def test_outtimes(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/cosmology.par'
        self.compile()

        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s/'%self.identifier
        self.output_file = self.output_dir + '/outtimes'
        self.test_args = '-tf '+self.pkdgrav3_test_dir+'/euclid_z0_transfer_combined.dat'
        self.test_args += ' -ot '+self.pkdgrav3_test_dir+'/example.red'
        self.run_pkdgrav3()
        self.check_redshift()


        self.test_args = ''
        self.param_file = self.output_file + '.00003.chk.par'
        self.run_pkdgrav3()
        self.check_redshift()

    def check_redshift(self):
        import glob

        snapnames = sorted(glob.glob(self.output_dir+'/outtimes.?????.0'))
        z = np.empty(len(snapnames))

        for i, snapname in enumerate(snapnames):
            with h5py.File(snapname, 'r') as snap:
                z[i] = snap['Header'].attrs['Redshift']

        ztrue = np.array([40., 30., 2., 1., 0.])
        for a,b in zip(z, ztrue):
            self.assertEqual(a, b, msg='The output and input redshifts do not match')


