from pkdtest import PKDGRAV3TestCase


class IsolatedGalaxyTest(PKDGRAV3TestCase):
    title = "Isolated galaxy"
    description = ("Simulation of an isolated galaxy with star formation and feedback. "
                  "Details can be found at the README file")
    plots = [{"file": "galaxy",
              "caption": "Final snapshot of the simulation"},
             {"file": "KSlaw",
              "caption": "Kennicut-Schimdt law from the simulation and the input law."},
             {"file": "SFH",
              "caption": "Star formation history"}]

    schemes = ["MFM"]
    __name__ = "IsolatedGalaxy"
    folder_name = "isolatedgalaxy"

    compile_flags = {'COOLING':True,'STAR_FORMATION':True,'FEEDBACK':True,'BLACKHOLES':False,'EEOS_POLYTROPE':True,"HERNQUIST_POTENTIAL":True}

    def generateIC(self):
        import numpy as np
        import sys
        import pynbody
        from pykdgrav3_utils import hdf5, units


        in_file = self.pkdgrav3_test_dir+'/IsolatedGalaxy_highres.dat'
        out_file = self.input_file

        fid = pynbody.load(in_file)

        u = units.units(1e10, 1., verbose=False)


        gamma = 5./3.


        h = 0.704

        Ngas = fid.gas['mass'].shape[0]
        Nstar = fid.star['mass'].shape[0]

        totGasMass = np.sum(fid.gas['mass'])/h
        totStarMass =np.sum(fid.star['mass'])/h
        totDMMass =np.sum(fid.dm['mass'])/h
        totMass = totGasMass + totStarMass + totDMMass
        print("Total mass: ", totMass)
        print("Mass in gas: ", totGasMass )
        print("Mass in star: ",totStarMass )
        print("Mass in dm: ",totDMMass )
        print("Fraction of mass in baryons: ", (totGasMass+totStarMass)/totMass)

        print("Gas particle mass: %e"% (fid.gas['mass'][0]/h*u.dMsolUnit) )
        print("Star particle mass: %e"% (fid.star['mass'][0]/h*u.dMsolUnit))

        print ("Creating hdf5 file...")

        gas_dict = {'Coordinates': fid.gas['pos']/u.dKpcUnit/h,
                    'Masses': fid.gas['mass']/h,
                    'Velocities': fid.gas['vel']/u.dKmPerSecUnit,
                    'InternalEnergy': fid.gas['u']*(1e5)**2/u.dErgPerGmUnit,
                    'SmoothingLength': np.full( Ngas, 0.1)}


        star_dict = {'Coordinates': fid.star['pos']/u.dKpcUnit/h,
                    'Masses': fid.star['mass']/h,
                    'Velocities': fid.star['vel']/u.dKmPerSecUnit,
                    'StellarFormationTime': np.full( Nstar, -1.)}


        hdf5.save({'PartType0':gas_dict, 'PartType4':star_dict}, out_file)



    def test_isolatedgalaxy(self,result=None, *args, **kwargs):
        import pathlib, shutil

        self.param_file = self.pkdgrav3_test_dir + '/IsolatedGalaxy.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        # Reference run to z=0
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/isolatedgalaxy'
        self.run_pkdgrav3()
        self.plot()


    def plot(self):
        from isolatedgalaxy.plot import run
        run(self.output_file+'.00500.0', self.pkdgrav3_test_dir+'/galaxy_%s.png'%self.identifier)

        from isolatedgalaxy.SFH import run
        run( [self.output_dir], self.pkdgrav3_test_dir+'/SFH_%s.png'%self.identifier)

        from isolatedgalaxy.KSlaw import run
        run(self.output_file, self.pkdgrav3_test_dir + '/KSlaw_%s.png'%self.identifier)



