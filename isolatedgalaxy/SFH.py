def run(folders, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from pykdgrav3_utils import hdf5, units
    import numpy as np
    import glob

    f, ax = plt.subplots(1)

    def read_SFH_from_finelog(folder):
        filename = glob.glob(folder+'/*.finelog')

        searchfile = open(filename[0], "r")
        # We look for the last simulation period
        lastComment = 0
        i=0
        nLogs = 0
        for line in searchfile:
            if line[0]=='#': 
                lastComment = i
                nLogs=0
            nLogs+=1
            i+=1

        # Now we read the star formation from this point
        searchfile.seek(0)
        dTime = np.zeros(nLogs)
        dStep = np.zeros(nLogs)
        starFormed = np.zeros(nLogs)
        massFormed = np.zeros(nLogs)
        i=0
        logi = 0
        for line in searchfile:
            if i>lastComment:
                off = i-lastComment
                if off%2==1:
                    splits = line.split(' ')
                    dStep[logi] = float(splits[0])
                    dTime[logi] = float(splits[1])
                    starFormed[logi] = int(splits[2])
                    massFormed[logi] = float(splits[3])
                    logi+=1
            i+=1


        return dStep[:logi], dTime[:logi], starFormed[:logi], massFormed[:logi]


    def read_SFH_from_snap(folder):
        filename = sorted(glob.glob(folder+'/*.0'))[-1]

        snap, u = hdf5.read_single(filename)

        # We mask the stars that were part of the IC
        mask = snap['PartType4/StellarFormationTime'][...] > 0

        star_formation_time = snap['PartType4/StellarFormationTime'][mask]*u.dSecUnit/(365*24*3600)/1e6 # Myr
        star_mass = snap['PartType4/Masses'][mask]*u.dMsolUnit

        nbins = 50
        bins = np.linspace(0., np.max(star_formation_time), nbins)
        bin_size =  (bins[1]-bins[0])*1e6 # yr

        SFR, age_bins = np.histogram(star_formation_time, bins=bins, weights=star_mass/bin_size)
        
        return SFR, 0.5*(age_bins[1:]+age_bins[:-1])


    def read_SFR_from_mult_snaps(folder):
        filenames = sorted(glob.glob(folder+'/*.0'))
        nfiles = len(filenames)

        SFR = np.zeros(nfiles)
        time = np.zeros(nfiles)

        i=0
        for filename in filenames:
            snap, u = hdf5.read_single(filename)
            SFR[i] = np.sum(snap['PartType0/StarFormationRate']) * u.dMsolUnit / u.dSecUnit*(365*24*3600)
            time[i] = snap['Header'].attrs['Time'] * u.dSecUnit/(365*24*3600) / 1e6

            i+=1

        return SFR, time

    
    for folder in folders:
        dStep, dTime, starFormed, massFormed = read_SFH_from_finelog(folder)
        u = units.units(1e10, 1.)
        dTime *= u.dSecUnit /(3600*24*365) / 1e6
        SFH = np.diff(massFormed)/np.diff(dTime*1e6) * u.dMsolUnit 
        ax.plot(dTime[1:], SFH, 'k.', label='From finelog')

        SFR, ages = read_SFH_from_snap(folder)
        ax.plot(ages, SFR, 'r-', label='From stellar ages')

        
        SFR, ages = read_SFR_from_mult_snaps(folder)
        ax.plot(ages, SFR, 'ro', label='From SFR in snapshots')

        ax.legend()
        ax.set_ylim(0,50)
        ax.set_xlabel('Time [Myr]')
        ax.set_ylabel(r'SFR [M$_\odot$ yr$^{-1}$]')

    if path is not None:
        f.savefig(path)
        f.clf()
    else:
        plt.show()
    
if __name__=='__main__':
    import sys

    if sys.argv[-1][-3:] == 'png':
        run(sys.argv[1:-1], sys.argv[-1])
    else:
        run(sys.argv[1:])
