import matplotlib
import os
if os.environ.get('REMOTE_PLOT')!=None:
    print ("Plotting in remote enviroment")
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as col
import numpy as np
from pykdgrav3_utils import hdf5, units
import glob
import sys



def plot(axs, gasx, gasy, gasz, gasEkin, gasEint, gasMass, gasT, gasRho, starx, stary, starz, starMass ):

    Nbins = 80
    size = 40. # kpc
    binarea = size**2/Nbins**2 *1e6 # pc-2
    binrange = [[-size/2, size/2],[-size/2, size/2]]

    # Gas density
    ## Face on
    faceonMass,_,_,im = axs[0][0].hist2d(gasx, gasy, bins=Nbins, range=binrange, weights=gasMass/binarea, norm=col.LogNorm(vmin=5, vmax=2e2))
    plt.colorbar(im, ax=axs[0][0], label=r'M$_\odot$ pc$^{-2}$')
    axs[0][0].set_aspect('equal')
    axs[0][0].set_xlabel('x [kpc]')
    axs[0][0].set_ylabel('y [kpc]')
    axs[0][0].set_title('Gas density')

    ## Edge on
    edgeonMass,_,_,im = axs[1][0].hist2d(gasx, gasz, bins=Nbins, range=binrange, weights=gasMass/binarea, norm=col.LogNorm(vmin=5, vmax=2e2))
    plt.colorbar(im, ax=axs[1][0], label=r'M$_\odot$ pc$^{-2}$')
    axs[1][0].set_aspect('equal')
    axs[1][0].set_xlabel('x [kpc]')
    axs[1][0].set_ylabel('z [kpc]')

    # Mass weigthed temperature
    ## Face on
    faceonT,_,_ = np.histogram2d(gasx, gasy, bins=Nbins, range=binrange, weights=gasT*gasMass/binarea)
    faceonT[faceonMass>0] /= faceonMass[faceonMass>0] 
    im = axs[0][1].imshow(faceonT.T, extent=[size/2, -size/2, size/2, -size/2], origin='lower', norm=col.LogNorm(vmin=1e4, vmax=3e6))
    plt.colorbar(im, ax=axs[0][1], label=r'T')
    axs[0][1].set_aspect('equal')
    axs[0][1].set_xlabel('x [kpc]')
    axs[0][1].set_ylabel('y [kpc]')
    axs[0][1].set_title('Projected temperature')

    ## Edge on
    edgeonT,_,_ = np.histogram2d(gasx, gasz, bins=Nbins, range=binrange, weights=gasT*gasMass/binarea)
    edgeonT[edgeonMass>0] /= edgeonMass[edgeonMass>0]
    im = axs[1][1].imshow(edgeonT.T, extent=[size/2, -size/2, size/2, -size/2], origin='lower', norm=col.LogNorm(vmin=1e4, vmax=3e6))
    plt.colorbar(im, ax=axs[1][1], label=r'T')
    axs[1][1].set_aspect('equal')
    axs[1][1].set_xlabel('x [kpc]')
    axs[1][1].set_ylabel('z [kpc]')

    # Phase diagram
    _,_,_,im = axs[0][2].hist2d(np.log10(gasRho), np.log10(gasT), bins=100, norm=col.LogNorm() , range=[[-5,2],[3.5, 7.7]])
    plt.colorbar(im, ax=axs[0][2])
    axs[0][2].set_xlabel('log n$_H$')
    axs[0][2].set_ylabel('log T')

    # Stellar density
    ## Face on
    _,_,_,im = axs[1][2].hist2d(starx, stary, bins=Nbins, range=binrange, weights=starMass/binarea, norm=col.LogNorm(vmin=5, vmax=6e2))
    plt.colorbar(im, ax=axs[1][2], label=r'M$_\odot$ pc$^{-2}$')
    axs[1][2].set_aspect('equal')
    axs[1][2].set_xlabel('x [kpc]')
    axs[1][2].set_ylabel('y [kpc]')
    axs[1][2].set_title('Stellar density')

    # Eint/Etot ratio
    """
    gasEtot = gasEint+gasEkin
    try:
        axs[1][2].hist(np.log10(gasEint/gasEtot), bins=100, cumulative=True, density=True)
    except:
        print("nah")
    axs[1][2].axvline(-2, c='r', ls=':')
    axs[1][2].semilogy()
    axs[1][2].set_xlabel(r'log (U$_{int}$/E$_{tot}$)')
    axs[1][2].set_ylabel(r'Cumulative fraction of particles')
    """

def read_hdf5(f, axs):
    snap, u = hdf5.read_single(f)

    gasx = snap['PartType0/Coordinates'][:,0]*u.dKpcUnit
    gasy = snap['PartType0/Coordinates'][:,1]*u.dKpcUnit
    gasz = snap['PartType0/Coordinates'][:,2]*u.dKpcUnit
    gasMass = snap['PartType0/Masses'][...]*u.dMsolUnit

    starx = snap['PartType4/Coordinates'][:,0]*u.dKpcUnit
    stary = snap['PartType4/Coordinates'][:,1]*u.dKpcUnit
    starz = snap['PartType4/Coordinates'][:,2]*u.dKpcUnit
    starMass = snap['PartType4/Masses'][...]*u.dMsolUnit

    gasRho = np.array(snap['PartType0/Density'][...], dtype=np.double)* 0.75 / u.MHYDR * u.dMsolUnit * u.MSOLG * (u.dKpcUnit*u.KPCCM)**(-3)
    gasT = snap['PartType0/Temperature'][...]

    gasEint = snap['PartType0/InternalEnergy'][...]*snap['PartType0/Masses']
    gasEkin = 0.5* snap['PartType0/Masses'][...] * ( snap['PartType0/Velocities'][:,0]**2 +  snap['PartType0/Velocities'][:,1]**2 + snap['PartType0/Velocities'][:,2]**2 )


    plot(axs, gasx, gasy, gasz, gasEint, gasEkin, gasMass, gasT, gasRho, starx, stary, starz, starMass )   


def run(snapname, path=None):
    fig, axs = plt.subplots(2,3, figsize=(13,7))
    read_hdf5(snapname, axs)
    plt.tight_layout()
    if path is not None:
        fig.savefig(path)
    else:
        plt.show()



if __name__=='__main__':
    import sys

    if sys.argv[1].split('.')[-1] != '0': # If the input is a folder, we save all the images
        folder = sys.argv[1]
        files = sorted(glob.glob(folder+'/*.0'))

        for f in files:
            fig, axs = plt.subplots(2,3, figsize=(13,7))
            read_hdf5(f, axs)

            plt.tight_layout()
            fig.savefig('imgs'+folder[5:]+'/'+f.split('.')[-2]+'.png')
            plt.close(fig)
    else:  # Otherwise, we just plot the snapshot

        fig, axs = plt.subplots(2,3, figsize=(13,7))
        read_hdf5(sys.argv[1], axs)
        plt.tight_layout()
        if sys.argv[-1][-3:] == 'png':
            f.savefig(sys.argv[-1])
        else:
            plt.show()


