def run(outfile, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.colors as col
    import numpy as np
    from pykdgrav3_utils import hdf5, units
    import glob

    files = sorted(glob.glob(outfile+'.?????.0'))



    n = -1
    print (files[n])

    rs = np.linspace(1., 6., 70)**2

    snap, u = hdf5.read_single(files[n])

    newStars =  snap['PartType4/StellarFormationTime'][...] > 0.
    print (np.sum(newStars))


    #""" Azimutally averaged KS law
    r = np.sqrt(snap['PartType4/Coordinates'][:,0]**2 + snap['PartType4/Coordinates'][:,1]**2) * u.dKpcUnit

    r = np.sqrt(snap['PartType4/Coordinates'][:,0]**2 + snap['PartType4/Coordinates'][:,1]**2) * u.dKpcUnit
    surfDensitySF_az = np.array([np.sum(snap['PartType4/Masses'][newStars & (r<rs[i+1]) & (r>rs[i])]) \
            for i in range(len(rs)-1)])*u.dMsolUnit / (np.pi*(rs[1:]**2-rs[:-1]**2)*u.dKpcUnit**2 )

    r = np.sqrt(snap['PartType0/Coordinates'][:,0]**2 + snap['PartType0/Coordinates'][:,1]**2) * u.dKpcUnit
    surfDensitySF_gas = np.array([np.sum(snap['PartType0/StarFormationRate'][(r<rs[i+1]) & (r>rs[i])]) \
            for i in range(len(rs)-1)])*u.dMsolUnit/(u.dSecUnit/(365*24*3600) )/ (np.pi*(rs[1:]**2-rs[:-1]**2)*u.dKpcUnit**2 )

    surfDensity = np.array([np.sum(snap['PartType0/Masses'][(r<rs[i+1]) & (r>rs[i])]) \
            for i in range(len(rs)-1)])*u.dMsolUnit /(np.pi*(rs[1:]**2-rs[:-1]**2)*u.dKpcUnit**2*1e6 ) # Mo pc-2
    #"""


    frac_torb = 0.25
    t_orb = 10.48339
    dTime = snap['Header'].attrs['Time']* u.dSecUnit / 3600. / 24 / 365. # yr

    #dDelta = frac_torb*t_orb * u.dSecUnit / 3600. / 24 / 365. # yr
    step = int(files[n].split('.')[-2])
    surfaceSFR_gas = surfDensitySF_gas
    surfaceSFR_az = surfDensitySF_az / (dTime)


    plt.plot(surfDensity, surfaceSFR_gas, 'r.', label="Azimuthally averaged KS law from stellar positions")
    plt.plot(surfDensity, surfaceSFR_az, 'b*', label="Azimuthally averaged KS law from SFR output")
    plt.xlabel(r'$\Sigma_\mathrm{gas}$ [M$_\odot$ pc$^{-2}$]')
    plt.ylabel(r'$\Sigma_\mathrm{SFR}$ [M$_\odot$ yr$^{-1}$ kpc$^{-2}$]')
    plt.title(r'KS law ; $t=%.1f$ Myr'%(dTime/1e6))


    # Expected relation:
    Sigma_g = np.logspace(0, 2, 4) #Mo pc-2
    Sigma_SFR = 1.5e-4 * Sigma_g**1.4 #Mo kpc-2 yr-1
    plt.plot(Sigma_g, Sigma_SFR, 'k--', label=r'Input KS: $1.5 \times 10^{-4}$ M$_\odot$ yr$^{-1}$ kpc$^{-2}$ $\Sigma_{gas}^{1.4}$')
    plt.axvline(7.3, c='k', ls=':', label=r'Input threshold: $7.3$ M$_\odot$ pc$^{-2}$')

    plt.loglog()
    plt.legend(loc=0, fancybox=True)
    plt.xlim(1, 120)
    plt.ylim(4e-6, 1.5e-1)
    plt.tight_layout()
    if path is not None:
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    if len(sys.argv) > 2:
        run(sys.argv[1], sys.argv[2])
    else:
        run(sys.argv[1])
