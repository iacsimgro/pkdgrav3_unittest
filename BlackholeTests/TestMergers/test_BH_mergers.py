from pkdtest import PKDGRAV3TestCase


class BHMergersTest(PKDGRAV3TestCase):

    schemes = ['MFM']
    __name__ = "BHMergers"
    folder_name = "BlackholeTests/TestMergers"

    compile_flags = {'BLACKHOLES':True, 'DEBUG_BH_ONLY':True}

    def generateIC(self):
        import numpy as np
        import h5py
        import os.path


        N_bh = 5000
        mass_bh=1.0

        G=1.0
        L=1.0

        random_state = np.random.RandomState(seed=12345)

        bhClassArr = np.array([(0, 0., 0.0, 0)], \
              dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        bhClassArr['mass'] = mass_bh
        bhClassArr['soft'] = 1./100.
        bhClassArr['class'] = 0
        bhClassArr['start'] = 0
        classArr = bhClassArr

        pos = random_state.random((N_bh, 3))-0.5

        modV = np.sqrt(G*N_bh*mass_bh/L)*10.
        vel = np.zeros((N_bh, 3))

        ##############
        # HDF5
        ##############
        IC_file = h5py.File(self.input_file, 'w')

        bh_group = IC_file.create_group('PartType5')
        dset = bh_group.create_dataset("classes", data=classArr)
        dset = bh_group.create_dataset("class", data=np.zeros(N_bh, dtype="|u1") )
        dset = bh_group.create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = bh_group.create_dataset("Velocities", data=vel, dtype="<f8")
        dset = bh_group.create_dataset("InternalMass", data=np.ones(N_bh, dtype='<f8')*mass_bh, dtype="<f8")

        IC_file.create_group('Header')
        IC_file['Header'].attrs['Time'] = 1.

        IC_file.close()



    def test_BH_mergers(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testMergers.par'
        self.input_file = self.pkdgrav3_test_dir+'/IC.hdf5'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        self.threads = 1
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_sz1'%(self.identifier)
        self.output_file = self.output_dir + '/BH_mergers'
        self.run_pkdgrav3()

        self.threads = 4
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_sz4'%(self.identifier)
        self.output_file = self.output_dir + '/BH_mergers'
        self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from BlackholeTests.TestMergers.plot_compare import run
        run(self.pkdgrav3_test_dir + '/snaps_%s_sz1/BH_mergers'%(self.identifier), self.pkdgrav3_test_dir + '/snaps_%s_sz4/BH_mergers'%(self.identifier), self.pkdgrav3_test_dir+'/merger_comparison_%s.png'%self.identifier)


