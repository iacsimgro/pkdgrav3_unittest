
def run(out_ref, out_mpi, path=None):
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py

    ref_filename = out_ref+'.00001.0'
    mpi_filename = out_mpi+'.00001.0'

    ref_file = h5py.File(ref_filename, 'r')
    mpi_file = h5py.File(mpi_filename, 'r')


    part_data = ref_file['PartType5']
    ref_masses = part_data['Masses'][...]
    ref_int_masses = part_data['InternalMass'][...]
    ref_coordx = part_data['Coordinates'][:,0]
    ref_coordy = part_data['Coordinates'][:,1] 
    ref_coordz = part_data['Coordinates'][:,2] 
    ref_pot = part_data['Potential']


    part_data = mpi_file['PartType5']
    mpi_masses = part_data['Masses'][...]
    mpi_int_masses = part_data['InternalMass'][...]
    mpi_coordx = part_data['Coordinates'][:,0]
    mpi_coordy = part_data['Coordinates'][:,1] 
    mpi_coordz = part_data['Coordinates'][:,2] 
    mpi_pot = part_data['Potential']


    f, ax = plt.subplots(1,2, figsize=(8,4))

    # Comparison of the mass distribution

    ref_label = 'REF N=%d ; M=%d'%(ref_masses.shape[0], np.sum(ref_masses))
    mpi_label = 'MPI N=%d ; M=%d'%(mpi_masses.shape[0], np.sum(mpi_masses))


    ax[0].hist(ref_masses, bins=np.arange(10)-0.5, histtype='step', color='b', label=ref_label)
    ax[0].hist(mpi_masses, bins=np.arange(10)-0.5, histtype='step', color='k', label=mpi_label)

    ax[0].semilogy()
    ax[0].legend()
    ax[0].set_xlabel('Mass')
    ax[0].set_ylabel('N')

    def corr_function(x,y,z):
        r_bins = np.logspace(-3,0,20)
        corr = np.zeros(r_bins.shape[0]-1)

        for i in range(x.shape[0]):
            dists = np.sqrt(  (x[i]-x)**2 + (y[i]-y)**2 + (z[i]-z)**2  )
            h, _ = np.histogram(dists, bins=r_bins)


            corr += h


        return r_bins, corr

        
    r_bins, ref_corr = corr_function(ref_coordx, ref_coordy, ref_coordz)
    r_bins, mpi_corr = corr_function(mpi_coordx, mpi_coordy, mpi_coordz)
        
    ax[1].step(r_bins[:-1], ref_corr, c='b')
    ax[1].step(r_bins[:-1], mpi_corr, c='k')
    ax[1].axvline(1./100., color='r', linestyle=':') # We mark the softening length
    ax[1].loglog()
    ax[1].set_xlabel('Pair distance')
    ax[1].set_ylabel('N')



    if np.sum(ref_masses) != np.sum(mpi_masses):
        raise Exception("Total masses are not equal")
    if np.sum(ref_int_masses) != np.sum(mpi_int_masses):
        raise Exception("Total internal masses are not equal")


    if path is not None:
        f.savefig(path)
    else:
        plt.show()

