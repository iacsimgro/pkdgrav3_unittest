from pkdtest import PKDGRAV3TestCase


class BHIOTest(PKDGRAV3TestCase):

    schemes = ['MFM']
    __name__ = "BHIO"
    folder_name = "BlackholeTests/TestIO"

    compile_flags = {'BLACKHOLES':True, 'DEBUG_BH_ONLY':True}
    max_threads = 1

    def generateIC(self):
        import numpy as np
        import h5py


        N_bh = 50
        mass_bh=1.0

        G=1.0
        L=1.0


        def create_IC(partType):

            IC_filename = self.pkdgrav3_test_dir+'/IC_test1_'+partType
            IC_file = h5py.File(IC_filename, 'w')


            random_state = np.random.RandomState(seed=12345)

            bhClassArr = np.array([(0, 0., 0.0, 0)], \
                  dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
            bhClassArr['mass'] = mass_bh
            bhClassArr['soft'] = 1./5**0.333 / 30.
            bhClassArr['class'] = 0
            bhClassArr['start'] = 0
            classArr = bhClassArr

            pos = random_state.random((N_bh, 3))-0.5

            modV = np.sqrt(G*N_bh*mass_bh/L)*10.
            #vel = random_state.random((N_bh, 3))*(1.-modV)
            vel = np.zeros((N_bh, 3))



            bh_group = IC_file.create_group(partType)
            dset = bh_group.create_dataset("classes", data=classArr)
            dset = bh_group.create_dataset("class", data=np.zeros(N_bh, dtype="|u1") )
            dset = bh_group.create_dataset("Coordinates", data=pos, dtype="<f8")
            dset = bh_group.create_dataset("Velocities", data=vel, dtype="<f8")

            if partType=='PartType5':
                dset = bh_group.create_dataset("InternalMass", data=np.full(N_bh, mass_bh, dtype='<f8'))

            IC_file.create_group('Header')
            IC_file['Header'].attrs['Time'] = 1.

            IC_file.close()

        create_IC('PartType1')
        create_IC('PartType5')



    def test_BH_IO(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testIO.par'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        self.input_file = self.pkdgrav3_test_dir+'/IC_test1_PartType5'
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_PartType5_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_IO'
        self.run_pkdgrav3()

        self.input_file = self.pkdgrav3_test_dir+'/IC_test1_PartType1'
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_PartType1_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_IO'
        self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from BlackholeTests.TestIO.plot import run
        run(self.pkdgrav3_test_dir + '/snaps_PartType1_%s/BH_IO.00001.0'%(self.identifier),self.pkdgrav3_test_dir + '/snaps_PartType5_%s/BH_IO.00001.0'%(self.identifier),self.pkdgrav3_test_dir + '/testIO_%s.png'%(self.identifier))


