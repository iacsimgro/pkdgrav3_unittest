def run(dm_filename, bh_filename, path=None):
    import numpy as np
    import matplotlib.pyplot as plt
    import h5py

    dm_file = h5py.File(dm_filename, 'r')
    bh_file = h5py.File(bh_filename, 'r')


    ## 
    pos_diff = np.mean( np.sum((dm_file['PartType1/Coordinates'][...]-bh_file['PartType5/Coordinates'][...])**2, axis=1) )
    vel_diff = np.mean( np.sum((dm_file['PartType1/Velocities'][...]-bh_file['PartType5/Velocities'][...])**2, axis=1) )

    f, ax = plt.subplots(1)

    ax.plot(dm_file['PartType1/Coordinates'][:,0], dm_file['PartType1/Coordinates'][:,1], 'kx', label='DM')
    ax.plot(bh_file['PartType5/Coordinates'][:,0], bh_file['PartType5/Coordinates'][:,1], 'k+', label='BH')

    ax.set_xlabel('x')
    ax.set_xlabel('y')
    ax.set_xlim(-0.5, 0.5)
    ax.set_ylim(-0.5, 0.5)
    plt.legend(loc=0, fancybox=True)

    plt.tight_layout()

    if path is not None:
        print (path)
        plt.savefig(path)
    else:
        plt.show()

    if pos_diff!=0.0 or vel_diff!=0.0:
        Exception('BlackholeTests/TestIO failed!')

if __name__=='__main__':
    import sys
    if len(sys.argv) > 3:
        run(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        run(sys.argv[1], sys.argv[2])
