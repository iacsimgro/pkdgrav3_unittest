# Black Hole tests

This small, simple tests will check the implementation of the black holes in the code

*NOTE*: The test which do not have gas particles require to manually disable 
the `assert(pLowPot!=NULL)` located at blackhole/drift.c

# Test 1 - Simple I/O + gravity

In this test, we just create a box full of black holes and let them evolve via gravity.

We run this with DM or BH particles. 
If the both set of particles overlap in phase-space after a few iterations,
then the basic IO for BH is ok and the code correctly treats BH as collisionless particles.

![testIO](TestIO/testIO_ref.png "Test IO")

# Test 2 - Mergers

We place BH in a box and let them evolve via gravity, such that they will merge over time.
The test is run in single and multiple MPI domains to asses the difference that it makes,
as right now the BH mergers can not take place across domain boundaries.

All in all, mass *must* be conserved, so that is the main check of this test.
We compare both the mass function and the spatial "autocorrelation" of the BHs.

![testMergers](TestMergers/merger_comparison_ref.png "Test Merger")


# Test 3 - Evrard

We evolve the evrard collapse until a high enough density is reached and a BH is formed.
The mass of the FoF group is tracked to check that the BH is formed when expected.

Furthermore, we add a bulk motion to the whole domain to check that the BH is correctly 
being dragged by the gas particles.

![testEvrard](TestEvrard/evolution_ref.png "Test Evrard")

# Test 4 - Growth

We place a single BH in a constant density background so that it will start to grow and
accrete gas particles.
The expected mass evolution and accretion rate can be computed both analytically and from
the output of the snapshots.
We systematically compare both to check the correctness.

Furthermore, the test is designed such that both Bondi and Eddington accretion rates are used.

![testGrowth](TestGrowth/growth_mpi_ref.png "Test Growth growth")

The conservation of all the relevant properties is also checked in the case of accretion events
across a MPI boundary.

![testGrowthCons](TestGrowth/conservation_mpi_ref.png "Test Growth conservation")

*NOTE*: For this test to work as expected, the dragging of BHs by the gas must be disabled,
as it does not explicitly conserve momentum.

# Test 5 - Feedback

The preceding experiment is repeated but allowing for feedback.
Again, we check that the feedback is correctly handled among MPI domains.
The expected internal energy increase of the gas is also tracked.

![testFeedCons](TestFeedback/conservation_mpi_ref.png "Test Feedback conservation")

We check that the energy injection is done isotropically.

![testFeedIso](TestFeedback/distribution_mpi_ref.png "Test Feedback isotropy")

And lastly we follow the evolution of the BH and check that the accumulated energy does not
surpass the predefined threshold.

![testFeedGrowth](TestFeedback/growth_mpi_ref.png "Test Feedback growth")


*NOTE*: For this test to work as expected, the dragging of BHs by the gas must be disabled,
as it does not explicitly conserve momentum.
