from pkdtest import PKDGRAV3TestCase


class BHGrowthTest(PKDGRAV3TestCase):

    schemes = ['MFM']
    __name__ = "BHGrowth"
    folder_name = "BlackholeTests/TestGrowth"

    compile_flags = {'BLACKHOLES':True, 'DEBUG_BH_NODRIFT':True}

    def generateIC(self, rho0=1, vphi=0):
        import numpy as np
        import h5py
        import os.path


        L=1.0
        N_gas = 5000
        x = np.linspace(-L/2, L/2, num=int(N_gas**(1/3.)), endpoint=False)
        mesh = np.meshgrid(x,x,x)
        pos = np.stack((mesh[0].flatten(), mesh[1].flatten(), mesh[2].flatten()), axis=1)
        N_gas = pos.shape[0]
        N_bh = 1



        mass_gas=rho0*L**3/N_gas
        mass_bh=mass_gas*1e-1



        random_state = np.random.RandomState(seed=12345)

        bhClassArr = np.array([(0, 0., 0.0, 0)], \
              dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        bhClassArr['mass'] = mass_bh
        bhClassArr['soft'] = 1./100.
        bhClassArr['class'] = 0
        bhClassArr['start'] = 0


        gasClassArr = np.array([(0, 0., 0.0, 0)], \
               dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        gasClassArr['mass'] = mass_gas
        gasClassArr['soft'] = 1./100.
        gasClassArr['class'] = 1
        gasClassArr['start'] = N_gas

        classArr = np.append(bhClassArr, gasClassArr) 



        ##############
        # HDF5
        ##############
        IC_file = h5py.File(self.input_file, 'w')



        #pos = random_state.random((N_gas, 3))-0.5
        r = np.sqrt(pos[:,0]**2 + pos[:,1]**2)
        theta = np.arctan2(pos[:,1], pos[:,0])
        vel = np.zeros((N_gas, 3))
        vel[:,0] = -np.sin(theta)*vphi
        vel[:,1] = np.cos(theta)*vphi

        gas_group = IC_file.create_group('PartType0')
        dset = gas_group.create_dataset("classes", data=classArr)
        dset = gas_group.create_dataset("class", data=np.ones(N_gas, dtype="|u1") )
        dset = gas_group.create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = gas_group.create_dataset("Velocities", data=vel, dtype="<f8")
        dset = gas_group.create_dataset("InternalEnergy", data=np.ones(N_gas), dtype="<f8")


        pos = np.zeros((N_bh, 3))
        vel = np.zeros((N_bh, 3))

        bh_group = IC_file.create_group('PartType5')
        dset = bh_group.create_dataset("classes", data=classArr)
        dset = bh_group.create_dataset("class", data=np.zeros(N_bh, dtype="|u1") )
        dset = bh_group.create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = bh_group.create_dataset("Velocities", data=vel, dtype="<f8")

        IC_file.create_group('Header')
        IC_file['Header'].attrs['Time'] = 1.

        IC_file.close()



    def test_BH_growth(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testGrowth.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.set_scheme('MFM')
        self.compile()

        out = []
        alpha = []
        cvisc = []
        phi = []

        ## Alpha-boosted bondi accretion
        self.generateIC()
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_growth_alpha'
        self.run_pkdgrav3()
        self.plot(0, 0, 8e2, mode='alpha')
        out.append(self.output_file)
        alpha.append(8e2)
        cvisc.append(0)
        phi.append(0)

        ## Zero-velocity RG2015 AM-dependent accretion
        self.generateIC(rho0=1)
        self.output_file = self.output_dir + '/BH_growth_Cvisc0'
        self.extra_args = '-BHAccretionCvisc 1e2'
        self.run_pkdgrav3()
        self.plot(0, 1e2, 8e2, mode='Cvisc0')
        out.append(self.output_file)
        alpha.append(8e2)
        cvisc.append(1e2)
        phi.append(0)

        ## AM-suppresion should happen when vphi > cs/cvisc**1/3
        ## We test both regimes now, vphi > 1/2 and vphi 1 < 1/2

        ## RG2015 AM not suppressed acccretion
        self.generateIC(vphi=1)
        self.output_file = self.output_dir + '/BH_growth_Cvisc1'
        self.extra_args = '-BHAccretionCvisc 1e2'
        self.run_pkdgrav3()
        self.plot(1, 1e2, 8e2, mode='Cvisc1')
        out.append(self.output_file)
        alpha.append(8e2)
        cvisc.append(1e2)
        phi.append(1)

        ## RG2015 AM-suppresed accretion
        self.generateIC(vphi=0.1)
        self.output_file = self.output_dir + '/BH_growth_Cvisc01'
        self.extra_args = '-BHAccretionCvisc 1e2'
        self.run_pkdgrav3()
        self.plot(0.1, 1e2, 8e2, mode='Cvisc01')
        out.append(self.output_file)
        alpha.append(8e2)
        cvisc.append(1e2)
        phi.append(0.1)

        from BlackholeTests.TestGrowth.plot_evolution import run_growth,run_conservation
        run_growth(out, phi, cvisc, alpha, self.pkdgrav3_test_dir + '/growth_all_%s.png'%(self.identifier))


    def plot(self, phi, cvisc, alpha, mode=''):
        from BlackholeTests.TestGrowth.plot_evolution import run_growth,run_conservation
        run_growth(self.output_file, phi, cvisc, alpha, self.pkdgrav3_test_dir + '/growth_%s_%s.png'%(mode, self.identifier))
        run_conservation(self.output_file, self.pkdgrav3_test_dir + '/conservation_%s_%s.png'%(mode, self.identifier))


