import numpy as np
import matplotlib.pyplot as plt
import glob
import h5py

def read(folder):
    filenames = sorted(glob.glob(folder+'.?????.0'))

    n_files = len(filenames)

    t = np.zeros(n_files)
    mass = np.zeros(n_files)
    true_mass = np.zeros(n_files)
    accr = np.zeros(n_files)
    m_rho = np.zeros(n_files)
    s_rho = np.zeros(n_files)
    s_v = np.zeros(n_files)
    cs = np.zeros(n_files)

    total_mass = np.zeros(n_files)
    total_momx = np.zeros(n_files)
    total_momy = np.zeros(n_files)
    total_momz = np.zeros(n_files)


    i = 0
    for filename in  filenames:
        f = h5py.File(filename, 'r')

        t[i] = f['Header'].attrs['Time']
        mass[i] = f['PartType5/InternalMass'][0]
        true_mass[i] = f['PartType5/Masses'][0]
        accr[i] = f['PartType5/AccretionRate'][0]

        m_rho[i] = np.mean(f['PartType0/Density'])
        s_rho[i] = np.std(f['PartType0/Density'])

        total_mass[i] = np.sum(f['PartType0/Masses']) + np.sum(f['PartType5/Masses'])
        total_momx[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,0]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,0])
        total_momy[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,1]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,1])
        total_momz[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,2]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,2])

        s_v[i] = np.std(f['PartType0/Velocities'][:,0])

        gamma = 5./3.
        p = np.mean(f['PartType0/InternalEnergy'][...]*m_rho[i]*(gamma-1.))
        cs[i] = np.sqrt(gamma*p/m_rho[i])

        i+=1

    return t, mass, true_mass, accr, m_rho, s_rho, s_v, cs, total_mass, total_momx, total_momy, total_momz

def run_conservation(folder, path=None):

    t, mass, true_mass, accr, m_rho, s_rho, s_v, cs, total_mass, total_momx, total_momy, total_momz = read(folder)

    fig, axs = plt.subplots(1,2, figsize=(8,4))

    axs[0].plot(t, total_mass/total_mass[0]-1., 'x')
    axs[0].set_xlabel('Time')
    axs[0].set_ylabel('Mass/Mass(t=0)-1')

    axs[1].plot(t, total_momx, label='x')
    axs[1].plot(t, total_momy, label='y')
    axs[1].plot(t, total_momz, label='z')
    axs[1].set_xlabel('Time')
    axs[1].legend()
    axs[1].set_ylabel('Momentum')

    if path is not None:
        fig.savefig(path)
    else:
        plt.show()

def run_growth(folder, vphi, cvisc, alpha, path=None):

    if type(vphi)!=list:
        folder = [folder]
        vphi = [vphi]
        cvisc = [cvisc]
        alpha = [alpha]


    nruns = len(folder)

    fig, axs = plt.subplots(1,3, figsize=(12,4))

    for i in range(nruns):
        t, mass, true_mass, accr, m_rho, s_rho, s_v, cs, total_mass, total_momx, total_momy, total_momz = read(folder[i])

        ########
        # Theoretical expectation for the accretion rate
        ########

        cs = np.mean(cs)
        vRel2 = np.mean(s_v)**2
        eff = 0.1
        def accretion_rate(m_bh, t=1):
            prefactor = alpha[i]
            if cvisc[i] > 0 and vphi[i] > 0:
                prefactor *= min((cs/vphi[i])**3/cvisc[i], 1.0)

            bondi = prefactor * 4.* np.pi * m_bh*m_bh * np.mean(m_rho) / (cs*cs + vRel2)**1.5;
            edd = 1e-1 * m_bh / eff

            return min(bondi, edd)

        accretion_exp = np.array([accretion_rate(m) for m in mass])

        # This does not have to match perfectly the measured one from the snapshots, as in that case the
        # accretion rate was not computed just before dumping the snapshot, but rather in the previous drift, with the old mass
        # Furthermore, in the case of bondi accretion, small changes in c_s, vRel and density can bias the accretion rate
        if nruns==1:
            axs[0].plot(t, accr/mass, label='Simulation')
            axs[0].plot(t, accretion_exp/mass, label=r'Accretion rate computed from m$_{BH}$')
        else:
            axs[0].plot(t, accr/mass, label=r'$\alpha=%.1f\,C_\text{visc}=%.1f\,V_\phi=%.1f$'%(alpha[i], cvisc[i], vphi[i]))

        axs[0].set_xlabel('Time')
        axs[0].set_ylabel('Accretion rate/BH mass')
        axs[0].legend(fontsize=6)

        #######
        # Numerical integration of the accretion rates
        #######

        from scipy.integrate import odeint

        mass_exp = odeint(accretion_rate, mass[0], t)

        def interp_wrapper(m_bh, t):
           return np.interp(m_bh, mass, accr*(1.-eff))

        accr[0]=accr[1]/mass[1]**2 * mass[0]**2
        mass_semi_exp = odeint(interp_wrapper, mass[0], t)

        if nruns==1:
            axs[1].plot(t, mass, label='Simulation')
            axs[1].plot(t, true_mass, 'k', label='Simulation gravitational mass')
            axs[1].plot(t, mass_exp, label=r'Integrated with accretion rate computed from m$_{BH}$')
            axs[1].plot(t, mass_semi_exp, label='Integrated with accretion rate from simulations')
        else:
            axs[1].plot(t, mass, label=r'$\alpha=%.1f\,C_\text{visc}=%.1f\,V_\phi=%.1f$'%(alpha[i], cvisc[i], vphi[i]))

        axs[1].semilogy()
        axs[1].set_xlabel('Time')
        axs[1].set_ylabel('BH mass')
        axs[1].legend(fontsize=6)


        axs[2].plot(t, mass/mass_semi_exp[:,0])
        axs[2].set_xlabel('Time')
        axs[2].set_ylabel(r'm$_{BH}$ (Simulation) / m$_{BH}$ (accretion from simulation)', fontsize=8)


    plt.tight_layout()

    if path is not None:
        fig.savefig(path)
    else:
        plt.show()
