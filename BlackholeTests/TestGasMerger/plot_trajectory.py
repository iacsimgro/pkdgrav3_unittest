def run(folder, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    import glob
    import h5py

    f1, ax1 = plt.subplots()
    f2, ax2 = plt.subplots()

    snap_filenames = sorted(glob.glob(folder+'/*.?????.0'))
    stat_filenames = sorted(glob.glob(folder+'/*.?????.fofstats.0'))


    n_stats = len(stat_filenames)
    n_snaps = len(snap_filenames)

    # What we are going to fill
    fof_mass = np.zeros(n_snaps)
    step = np.zeros(n_snaps)
    BH1_r = np.zeros((n_snaps,3))
    BH2_r = np.zeros((n_snaps,3))
    BH3_r = np.zeros((n_snaps,3))
    CM1_r = np.zeros((n_snaps,3))
    CM2_r = np.zeros((n_snaps,3))

    thereAreBH = False

    snap = h5py.File(snap_filenames[0], 'r')
    # Make some mask of the two balls from the IC
    ids1 = snap['PartType0/ParticleIDs'][...][snap['PartType0/Coordinates'][:,0] < 0.]
    ids2 = snap['PartType0/ParticleIDs'][...][snap['PartType0/Coordinates'][:,0] > 0.]

    ax1.plot(snap['PartType0/Coordinates'][:,0], snap['PartType0/Coordinates'][:,1], 'k.', alpha=0.1)


    i_merge = 0
    for i_snap in range(n_snaps):
        snap = h5py.File(snap_filenames[i_snap], 'r')

        im = ax2.scatter(snap['PartType0/Coordinates'][:,0], snap['PartType0/Coordinates'][:,1], c=snap['PartType0/Potential'][...], cmap='viridis', vmin=-40, vmax=-2, alpha=0.4, s=0.5)
        cb = plt.colorbar(im, ax=ax2, label='Potential')

        nBH = snap['Header'].attrs['NumPart_Total'][5]
        if nBH > 1:
            if not thereAreBH:
                BHid1 = snap['PartType5/ParticleIDs'][1]
                BHid2 = snap['PartType5/ParticleIDs'][0]
            thereAreBH = True
            ax2.plot(snap['PartType5/Coordinates'][:,0], snap['PartType5/Coordinates'][:,1], 'rx', alpha=0.5)
            BH1_r[i_snap] = snap['PartType5/Coordinates'][...][ np.argwhere(snap['PartType5/ParticleIDs'][...] == BHid1) ]
            BH2_r[i_snap] = snap['PartType5/Coordinates'][...][ np.argwhere(snap['PartType5/ParticleIDs'][...] == BHid2) ]
            c1 = plt.Circle(( BH1_r[i_snap,0] , BH1_r[i_snap,1] ), 0.025 , fill=False, color='r')
            c2 = plt.Circle(( BH2_r[i_snap,0] , BH2_r[i_snap,1] ), 0.025 , fill=False, color='r')
            ax2.add_artist(c1)
            ax2.add_artist(c2)
        elif nBH==1: # Post merge
            if i_merge == 0:
                i_merge = i_snap
            BH3_r[i_snap] = snap['PartType5/Coordinates'][...]
            ax2.plot(snap['PartType5/Coordinates'][:,0], snap['PartType5/Coordinates'][:,1], 'r+')

        _, _, ind1 = np.intersect1d(ids1, snap['PartType0/ParticleIDs'], return_indices=True, assume_unique=True)
        _, _, ind2 = np.intersect1d(ids2, snap['PartType0/ParticleIDs'], return_indices=True, assume_unique=True)

        CM1_r[i_snap,:] = np.mean( snap['PartType0/Coordinates'][...][ind1], axis=0)
        CM2_r[i_snap,:] = np.mean( snap['PartType0/Coordinates'][...][ind2], axis=0)

        ax2.set_xlim(-0.5,0.5)
        ax2.set_ylim(-0.5,0.5)
        ax2.set_xlabel('x')
        ax2.set_ylabel('y')
        ax2.set_aspect('equal')
        f2.savefig(folder+'/%d.png'%i_snap)
        cb.remove()
        ax2.clear()

        i_snap += 1

    plt.close(f2)
    ax1.plot(snap['PartType0/Coordinates'][:,0], snap['PartType0/Coordinates'][:,1], 'k.', alpha=0.1, mew=0)
    ax1.plot(CM1_r[:, 0], CM1_r[:, 1], 'b', alpha=0.5)
    ax1.plot(BH1_r[:, 0], BH1_r[:, 1], 'b:')
    ax1.plot(CM2_r[:, 0], CM2_r[:, 1], 'r', alpha=0.5)
    ax1.plot(BH2_r[:, 0], BH2_r[:, 1], 'r:')
    ax1.plot(BH3_r[i_merge:, 0], BH3_r[i_merge:, 1], 'g:')

    ax1.set_xlim(-0.5,0.5)
    ax1.set_ylim(-0.5,0.5)
    ax1.set_xlabel('x')
    ax1.set_ylabel('y')
    ax1.set_aspect('equal')

    plt.tight_layout()
    if path is not None:
        f1.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], path=sys.argv[2])
    else:
        run(sys.argv[1])

