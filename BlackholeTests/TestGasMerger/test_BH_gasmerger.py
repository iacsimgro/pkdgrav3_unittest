from pkdtest import PKDGRAV3TestCase


class BHGasMergerTest(PKDGRAV3TestCase):

    schemes = ['MFM']
    __name__ = "BHGasMerger"
    folder_name = "BlackholeTests/TestGasMerger"

    compile_flags = {'BLACKHOLES':True}

    max_threads = 1

    def generateIC(self):
        import numpy as np
        from pykdgrav3_utils import hdf5

        N_gas = 1000
        M_gas = 1.

        # Create two collapsing balls of gas, and put them in a collision course

        def generate_cube(N, m, L, cen, v):
            Nl = int(N**(1./3.))
            x = np.linspace(0, L, Nl) - L/2.
            y = x.copy() + cen[1]
            z = x.copy() + cen[2]
            x += cen[0]

            X, Y, Z = np.meshgrid(x,y,z)
            X = X.flatten()
            Y = Y.flatten()
            Z = Z.flatten()

            pos = np.stack([X,Y,Z], axis=1)
            vel = np.zeros_like(pos)

            vel[:,0] = v[0]
            vel[:,1] = v[1]
            vel[:,2] = v[2]

            mass = np.full(Nl**3, m/Nl**3)

            return pos, vel, mass

        p1, v1, m1 = generate_cube( N_gas, M_gas, 0.1, [-0.4, 0., 0.], [0., .3, 0.] )
        p2, v2, m2 = generate_cube( N_gas, M_gas, 0.1, [0.4, 0., 0.], [0., -.3, 0.] )

        pos = np.append(p1,p2, axis=0)
        vel = np.append(v1,v2, axis=0)
        m = np.append(m1,m2)

        uint = np.full_like(m, 0.01)

        gas_dict = {'Coordinates': pos,
                    'Masses': m,
                    'Velocities': vel,
                    'InternalEnergy': uint}

        hdf5.save({'PartType0':gas_dict}, self.input_file)





    def test_BH_gasmerger(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testGasMerger.par'
        self.input_file = self.pkdgrav3_test_dir+'/IC.hdf5'
        self.generateIC()
        self.set_scheme('MFM')
        self.compile()

        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_gasmerger'
        self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from BlackholeTests.TestGasMerger.plot_trajectory import run
        run(self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier), \
            path=self.pkdgrav3_test_dir+'/trajectory_%s.png'%self.identifier)


