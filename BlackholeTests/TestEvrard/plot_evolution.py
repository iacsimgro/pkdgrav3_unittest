def run(output_file, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    import glob
    import h5py


    tinyGroupTable =  np.dtype([\
        ('rPot_x', '=f4'),('rPot_y', '=f4'), ('rPot_z', '=f4'),\
        ('minPot', '=f4'),\
        ('rcen_x', '=f4'),('rcen_y', '=f4'), ('rcen_z', '=f4'),\
        ('rcom_x', '=f4'),('rcom_y', '=f4'), ('rcom_z', '=f4'),\
        ('vcom_x', '=f4'),('vcom_y', '=f4'), ('vcom_z', '=f4'),\
        ('angular_x', '=f4'),('angular_y', '=f4'), ('angular_z', '=f4'),\
        ('inertia_xx', '=f4'),('inertia_yy', '=f4'), ('inertia_zz', '=f4'),('inertia_xy', '=f4'),('inertia_xz', '=f4'),('inertia_yz', '=f4'),\
        ('sigma', '=f4'),\
        ('rMax', '=f4'),\
        ('fMass', '=f4'),\
        ('fEnvironDensity0', '=f4'),\
        ('fEnvironDensity1', '=f4'),\
        ('rHalf', '=f4'),\
        ('nBH', '=i4'),\
        ('nStar', '=i4'),\
        ('nGas', '=i4'),\
        ('nDM', '=i4'),\
        ('iGlobalGid', '=u8')\
         ])


    snap_filenames = sorted(glob.glob(output_file+'.?????.0'))
    stat_filenames = sorted(glob.glob(output_file+'.?????.fofstats.0'))

    n_stats = len(stat_filenames)
    n_snaps = len(snap_filenames)

    # What we are going to fill
    fof_mass = np.zeros(n_stats)
    step = np.zeros(n_stats)
    BH_r = np.zeros((n_stats,3))
    CM_r = np.zeros((n_stats,3))
    BH_DM_r = np.zeros((n_stats,3))
    n_BHs = np.zeros(n_stats)

    i_snap = 0
    for i in range(n_stats):
        fofstat_file = open(stat_filenames[i], 'rb')
        fofstats = np.fromfile(fofstat_file,dtype=tinyGroupTable,count=900)

         

        step[i] = i 
        if len(fofstats)>0:
            # Select the most massive group
            i_fof = np.argmax(fofstats['fMass'])
            fof_mass[i] = fofstats['fMass'][i_fof]
            CM_r[i,0] = fofstats['rPot_x'][i_fof]
            CM_r[i,1] = fofstats['rPot_y'][i_fof]
            CM_r[i,2] = fofstats['rPot_z'][i_fof]
            
            n_BHs[i] = np.sum(fofstats['nBH'])

        if snap_filenames[i_snap+1].split('.')[-2] == stat_filenames[i].split('.')[-3]:
            i_snap += 1
            # For this step we have both fofstat and snapshot
            snap = h5py.File(snap_filenames[i_snap], 'r')
            n_BHs_snap = snap['Header'].attrs['NumPart_Total'][5]
            if n_BHs_snap>0 :
                BH_r[i] = snap['PartType5/Coordinates'][0,:]
                BH_DM_r[i] = np.sqrt(  np.sum( (BH_r[i]-CM_r[i])**2  ) )

            if n_BHs_snap!=n_BHs[i]:
                print (snap_filenames[i_snap], n_BHs_snap, n_BHs[i])
                raise Exception("Blackhole numbers do not match!")
        


    f, ax = plt.subplots(1,2, figsize=(8,4))

    ax[0].plot(step, fof_mass, 'b.:', label='Group Mass')
    ax[0].plot(step, n_BHs, 'k.:', label='N BH')

    ax[0].axhline(0.05, c='r', label='MhaloMin')
    ax[0].set_xlabel('Time')

    ax[0].legend(loc=0)

    #"""
    modBH_r = np.sqrt( np.sum(BH_r**2, axis=1) )


    ax[1].plot(step[modBH_r>0], BH_DM_r[modBH_r>0], '.')
    ax[1].set_xlim(0,step[-1])
    ax[1].set_ylim(-0.2,0.2)
    ax[1].set_xlabel('Time')
    ax[1].set_ylabel('BH-CM distance')
    #"""



    plt.tight_layout()

    if path is not None:
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], path=sys.argv[2])
    else:
        run(sys.argv[1])
