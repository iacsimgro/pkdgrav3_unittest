from pkdtest import PKDGRAV3TestCase


class BHEvrardTest(PKDGRAV3TestCase):

    schemes = ['MFM']
    __name__ = "BHEvrard"
    folder_name = "BlackholeTests/TestEvrard"

    compile_flags = {'BLACKHOLES':True}

    def generateIC(self):
        from pykdgrav3_utils.tipsy import openTipsy
        import numpy as np

        header, gas, dark, star = openTipsy(self.pkdgrav3_test_dir+'/evrard.00000')

        gas['vx'] += 5.0

        f = open(self.input_file, 'w')
        header_type = np.dtype([('time', '>f8'),('N', '>i4'), ('Dims', '>i4'), ('Ngas', '>i4'), ('Ndark', '>i4'), ('Nstar', '>i4'), ('pad', '>i4')])
        header = np.array( (1.0, header['N'], 3, header['Ngas'], 0, 0, 0), dtype=header_type )
        header.tofile(f)
        gas.tofile(f)

        f.close()



    def test_BH_evrard(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testEvrard.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_evrard'
        self.run_pkdgrav3()

        self.plot()


    def plot(self):
        from BlackholeTests.TestEvrard.plot_evolution import run
        run(self.output_file, self.pkdgrav3_test_dir + '/evolution_%s.png'%(self.identifier))


