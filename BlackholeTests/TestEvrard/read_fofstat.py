import numpy as np
import sys

"""
  float rPot[3];
  float minPot;
  float rcen[3];
  float rcom[3];
  float vcom[3];
  float angular[3];
  float inertia[6];
  float sigma;
  float rMax;
  float fMass;
  float fEnvironDensity0;
  float fEnvironDensity1;
  float rHalf;
"""



#not sure about the inertia axes!
tinyGroupTable =  np.dtype([\
    ('rPot_x', '=f4'),('rPot_y', '=f4'), ('rPot_z', '=f4'),\
    ('minPot', '=f4'),\
    ('rcen_x', '=f4'),('rcen_y', '=f4'), ('rcen_z', '=f4'),\
    ('rcom_x', '=f4'),('rcom_y', '=f4'), ('rcom_z', '=f4'),\
    ('vcom_x', '=f4'),('vcom_y', '=f4'), ('vcom_z', '=f4'),\
    ('angular_x', '=f4'),('angular_y', '=f4'), ('angular_z', '=f4'),\
    ('inertia_xx', '=f4'),('inertia_yy', '=f4'), ('inertia_zz', '=f4'),('inertia_xy', '=f4'),('inertia_xz', '=f4'),('inertia_yz', '=f4'),\
    ('sigma', '=f4'),\
    ('rMax', '=f4'),\
    ('fMass', '=f4'),\
    ('fEnvironDensity0', '=f4'),\
    ('fEnvironDensity1', '=f4'),\
    ('rHalf', '=f4'),\
    ('nBH', '=i4'),\
    ('nStar', '=i4'),\
    ('nGas', '=i4'),\
    ('nDM', '=i4')\
     ])

fofstat_file = open(sys.argv[1]+'.fofstats.0', 'rb')
fofstats = np.fromfile(fofstat_file,dtype=tinyGroupTable,count=900)


import h5py
#from units import units
#snap = h5py.File(sys.argv[1]+'.0', 'r')
#u = units(snap['Units'].attrs['MsolUnit'],snap['Units'].attrs['KpcUnit'], verbose=True )

print("Masses: ",fofstats['fMass'])

#print(fofstats['rHalf']*u.dKpcUnit)

print(fofstats['rPot_x'],fofstats['rPot_y'],fofstats['rPot_z'])

print(fofstats['nBH'])

import matplotlib.pyplot as plt
#plt.plot(fofstats['fMass']*u.dMsolUnit, fofstats['rMax']*u.dKpcUnit, 'k.')

f, ax = plt.subplots()

ax.plot(fofstats['rPot_x'], fofstats['rPot_y'], 'rx')
#if snap['Header'].attrs['NumPart_Total'][3]>0:
#    plt.plot(snap['PartType5/Coordinates'][:,0],snap['PartType5/Coordinates'][:,1], 'k+')

# Look for gas lowest potential
dm_min_pot = np.argmin(snap['PartType0/Potential'])
#plt.plot(snap['PartType0/Coordinates'][dm_min_pot,0], snap['PartType0/Coordinates'][dm_min_pot,1], 'g.')
#ax.set_xlim(-1,1)
#ax.set_ylim(-1,1)

for x,y,r in zip( fofstats['rPot_x'], fofstats['rPot_y'], fofstats['rMax']  ):
    ax.add_artist( plt.Circle((x,y), r, color='r', alpha=0.8, fill=False)  )







plt.show()
