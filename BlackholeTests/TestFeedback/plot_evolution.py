import numpy as np
import matplotlib.pyplot as plt
import glob
import sys
import h5py


def read(folder):
    filenames = sorted(glob.glob(folder+'.?????.0'))

    n_files = len(filenames)

    t = np.zeros(n_files)
    mass = np.zeros(n_files)
    true_mass = np.zeros(n_files)
    accr = np.zeros(n_files)
    m_rho = np.zeros(n_files)
    s_rho = np.zeros(n_files)
    s_v = np.zeros(n_files)
    cs = np.zeros(n_files)

    total_mass = np.zeros(n_files)
    total_momx = np.zeros(n_files)
    total_momy = np.zeros(n_files)
    total_momz = np.zeros(n_files)
    total_ene = np.zeros(n_files)

    feed = np.zeros(n_files)
    acc = np.zeros(n_files)

    i = 0
    for filename in  filenames:
        f = h5py.File(filename, 'r')

        t[i] = f['Header'].attrs['Time']
        mass[i] = f['PartType5/InternalMass'][0]
        true_mass[i] = f['PartType5/Masses'][0]
        accr[i] = f['PartType5/AccretionRate'][0]

        feed[i] = f['PartType5/FeedbackRate'][0]
        acc[i] = f['PartType5/AccumulatedFeedbackEnergy'][0]

        m_rho[i] = np.mean(f['PartType0/Density'])
        s_rho[i] = np.std(f['PartType0/Density'])

        total_mass[i] = np.sum(f['PartType0/Masses']) + np.sum(f['PartType5/Masses'])
        total_momx[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,0]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,0])
        total_momy[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,1]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,1])
        total_momz[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,2]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,2])

        total_ene[i] = np.sum(f['PartType0/InternalEnergy'][...]*f['PartType0/Masses'][...])

        s_v[i] = np.std(f['PartType0/Velocities'][:,0])

        gamma = 5./3.
        p = np.mean(f['PartType0/InternalEnergy'][...]*m_rho[i]*(gamma-1.))
        cs[i] = np.sqrt(gamma*p/m_rho[i])

        i+=1

    return t, mass, true_mass, accr, feed, acc, m_rho, s_rho, total_mass, total_momx, total_momy, total_momz, total_ene, s_v, cs 

def run_distribution(folder, path=None):

    fig, axs = plt.subplots(1,1, figsize=(5,4))
    dz = 0.05

    f = h5py.File(folder+'.00050.0', 'r')
    print(f['PartType5/AccumulatedFeedbackEnergy'][0])
    x_bh = 0.0
    y_bh = 0.0
    z_bh = 0.0
    mask = np.abs(f['PartType0/Coordinates'][:,2]-z_bh) < dz
    im = axs.tricontourf(f['PartType0/Coordinates'][...][mask,0]-x_bh, f['PartType0/Coordinates'][...][mask,1]-y_bh, f['PartType0/InternalEnergy'][...][mask], 20)

    axs.plot(f['PartType5/Coordinates'][0,0], f['PartType5/Coordinates'][0,1], 'rx')
    plt.colorbar(im, ax=axs, label='Internal Energy')
    axs.set_xlabel('x')
    axs.set_ylabel('y')

    plt.tight_layout()
    if path is not None:
        fig.savefig(path)
    else:
        plt.show()


def run_conservation(folder, path=None):
    t, mass, true_mass, accr, feed, acc, m_rho, s_rho, total_mass, total_momx, total_momy, total_momz, total_ene, s_v, cs = read(folder)
    fig, axs = plt.subplots(1,3, figsize=(12,4))

    axs[0].plot(t, total_mass/total_mass[0]-1., 'x')
    axs[0].set_xlabel('Time')
    axs[0].set_ylabel('Mass/Mass(t=0) - 1')

    axs[1].plot(t, total_momx, label='x')
    axs[1].plot(t, total_momy, label='y')
    axs[1].plot(t, total_momz, label='z')
    axs[1].set_xlabel('Time')
    axs[1].legend()
    axs[1].set_ylabel('Momentum')


    # Expected energy injection from the feedback rate from the snapshot
    ene_exp = total_ene[0] + np.cumsum(feed[1:]*np.diff(t))


    axs[2].plot(t, total_ene, label='Measured from gas')
    axs[2].plot(t[1:], ene_exp, label='Expected from BH feedback rate')
    axs[2].legend()
    axs[2].set_xlabel('Time')
    axs[2].set_ylabel('Total internal energy')

    plt.tight_layout()
    if path is not None:
        fig.savefig(path)
    else:
        plt.show()

def run_growth(folder, path=None):
    t, mass, true_mass, accr, feed, acc, m_rho, s_rho, total_mass, total_momx, total_momy, total_momz, total_ene, s_v, cs = read(folder)

    ########
    # Theoretical expectation for the accretion rate
    ########
    fig, axs = plt.subplots(1,3, figsize=(12,4))

    cs = np.mean(cs)
    vRel2 = np.mean(s_v)**2
    eff = 0.1
    def accretion_rate(m_bh, t=1):
        bondi = 8e2 * 4.* np.pi * m_bh*m_bh * np.mean(m_rho) / (cs*cs + vRel2)**1.5;
        edd = 1e-1 * m_bh / eff

        return min(bondi, edd)

    accretion_exp = np.array([accretion_rate(m) for m in mass])

    # This does not have to match perfectly the measured one from the snapshots, as in that case the
    # accretion rate was not computed just before dumping the snapshot, but rather in the previous drift, with the old mass
    # Furthermore, in the case of bondi accretion, small changes in c_s, vRel and density can bias the accretion rate
    axs[0].plot(t, accr/mass, label='Simulation')
    axs[0].plot(t, accretion_exp/mass, label=r'Accretion rate computed from m$_{BH}$')

    axs[0].set_xlabel('Time')
    axs[0].set_ylabel('Accretion rate/BH mass')
    axs[0].legend(fontsize=6)

    #######
    # Numerical integration of the accretion rates
    #######

    from scipy.integrate import odeint

    mass_exp = odeint(accretion_rate, mass[0], t)

    def interp_wrapper(m_bh, t):
       return np.interp(m_bh, mass, accr*(1.-eff))

    accr[0]=accr[1]/mass[1]**2 * mass[0]**2
    mass_semi_exp = odeint(interp_wrapper, mass[0], t)

    #print(mass.shape, mass_semi_exp[:,0].shape)
    axs[1].plot(t, mass, label='Simulation')
    axs[1].plot(t, true_mass, 'k', label='Simulation gravitational mass')
    axs[1].plot(t, mass_exp, label=r'Integrated with accretion rate computed from m$_{BH}$')
    axs[1].plot(t, mass_semi_exp, label='Integrated with accretion rate from simulations')

    axs[1].semilogy()
    axs[1].set_xlabel('Time')
    axs[1].set_ylabel('BH mass')
    axs[1].legend(fontsize=6)


    ######
    # Feedback stuff
    ######

    # We can compute the expected feedback from the accretion rate

    eps_f = 0.000000015
    c = 299792458. / 1e3 # km/sec
    def feedback_rate(m_bh, t=1):
        accr = accretion_rate(m_bh)
        feed = accr*eff*(1.-eff)*eps_f * c**2
        return feed

    deltaT = 10.0
    Ecrit = (1./(5./3. -1.)/0.58) * deltaT * (1./5000.)
    #print (Ecrit)

    #feed_ode = odeint(feedback_rate, mass[0], t)

    feed_rate = [feedback_rate(m) for m in mass]
    acc_exp = np.cumsum(feed_rate)*(t[1]-t[0])

    axs[2].plot(t, acc, label='Accumulated energy')
    axs[2].plot(t, feed, label='Feedback rate')
    axs[2].plot(t, feed_rate, label='Analytical feedback rate')
    axs[2].plot(t, acc_exp, label='Analytical accumulated energy')
    axs[2].axhline(Ecrit, color='b', linestyle=':')
    axs[2].set_xlabel('Time')
    axs[2].set_ylabel('Energy or Energy/time', fontsize=8)
    axs[2].legend(fontsize=8)
    axs[2].semilogy()


    plt.tight_layout()

    if path is not None:
        fig.savefig(path)
    else:
        plt.show()
