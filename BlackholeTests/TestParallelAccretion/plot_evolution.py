import numpy as np
import matplotlib.pyplot as plt
import glob
import h5py

def read(folder):
    filenames = sorted(glob.glob(folder+'.?????.0'))

    f = h5py.File(filenames[0], 'r')
    nbh  = len(f['PartType5/InternalMass'])
    n_files = len(filenames)

    t = np.zeros(n_files)
    mass = np.zeros((nbh,n_files))
    true_mass = np.zeros((nbh,n_files))
    accr = np.zeros((nbh,n_files))
    m_rho = np.zeros(n_files)
    s_rho = np.zeros(n_files)
    s_v = np.zeros(n_files)
    cs = np.zeros(n_files)

    total_mass = np.zeros(n_files)
    total_momx = np.zeros(n_files)
    total_momy = np.zeros(n_files)
    total_momz = np.zeros(n_files)


    i = 0
    for filename in  filenames:
        f = h5py.File(filename, 'r')

        sort = np.argsort(f['PartType5/ParticleIDs'][...])

        t[i] = f['Header'].attrs['Time']
        mass[:,i] = f['PartType5/InternalMass'][...][sort]
        true_mass[:,i] = f['PartType5/Masses'][...][sort]
        accr[:,i] = f['PartType5/AccretionRate'][...][sort]

        m_rho[i] = np.mean(f['PartType0/Density'])
        s_rho[i] = np.std(f['PartType0/Density'])

        total_mass[i] = np.sum(f['PartType0/Masses']) + np.sum(f['PartType5/Masses'])
        total_momx[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,0]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,0])
        total_momy[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,1]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,1])
        total_momz[i] = np.sum(f['PartType0/Masses']*f['PartType0/Velocities'][:,2]) + np.sum(f['PartType5/Masses']*f['PartType5/Velocities'][:,2])

        s_v[i] = np.std(f['PartType0/Velocities'][:,0])

        gamma = 5./3.
        p = np.mean(f['PartType0/InternalEnergy'][...]*m_rho[i]*(gamma-1.))
        cs[i] = np.sqrt(gamma*p/m_rho[i])

        i+=1

    return t, mass, true_mass, accr, m_rho, s_rho, s_v, cs, total_mass, total_momx, total_momy, total_momz

def run_conservation(folder, path=None):

    t, mass, true_mass, accr, m_rho, s_rho, s_v, cs, total_mass, total_momx, total_momy, total_momz = read(folder)

    fig, axs = plt.subplots(1,2, figsize=(8,4))

    axs[0].plot(t, total_mass*5000. -5050, 'x')
    axs[0].set_xlabel('Time')
    axs[0].set_ylabel('Mass change [gas particle mass]')

    axs[1].plot(t, total_momx, label='x')
    axs[1].plot(t, total_momy, label='y')
    axs[1].plot(t, total_momz, label='z')
    axs[1].set_xlabel('Time')
    axs[1].legend()
    axs[1].set_ylabel('Momentum')

    if path is not None:
        fig.savefig(path)
    else:
        plt.show()

def run_growth(folder, path=None):

    t, mass, true_mass, accr, m_rho, s_rho, s_v, cs, total_mass, total_momx, total_momy, total_momz = read(folder)
    fig, ax = plt.subplots(1, figsize=(4.5,4))


    for i in range(mass.shape[0]):
        ax.plot(t, true_mass[i,:] * 5000, 'k', alpha=0.1)

    ax.semilogy()
    ax.set_xlabel('Time')
    ax.set_ylabel('BH mass [gas particle mass]')

    plt.tight_layout()

    if path is not None:
        fig.savefig(path)
    else:
        plt.show()
