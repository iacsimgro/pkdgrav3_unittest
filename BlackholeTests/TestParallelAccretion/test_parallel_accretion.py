from pkdtest import PKDGRAV3TestCase


class BHParallelAccretionTest(PKDGRAV3TestCase):

    schemes = ['MFM']
    __name__ = "BHParallelAccretion"
    folder_name = "BlackholeTests/TestParallelAccretion"

    compile_flags = {'BLACKHOLES':True, 'DEBUG_BH_NODRIFT':True}

    def generateIC(self):
        import numpy as np
        import h5py
        import os.path


        N_gas = 5000
        N_bh = 500

        G=1.0
        L=1.0


        mass_gas=L**3/N_gas
        mass_bh=mass_gas*1e-1



        random_state = np.random.RandomState(seed=12345)

        bhClassArr = np.array([(0, 0., 0.0, 0)], \
              dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        bhClassArr['mass'] = mass_bh
        bhClassArr['soft'] = 1./100.
        bhClassArr['class'] = 0
        bhClassArr['start'] = 0


        gasClassArr = np.array([(0, 0., 0.0, 0)], \
               dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        gasClassArr['mass'] = mass_gas
        gasClassArr['soft'] = 1./100.
        gasClassArr['class'] = 1
        gasClassArr['start'] = N_gas

        classArr = np.append(bhClassArr, gasClassArr)



        ##############
        # HDF5
        ##############
        IC_file = h5py.File(self.input_file, 'w')

        pos = random_state.random((N_bh, 3))-0.5
        vel = np.zeros((N_bh, 3))

        bh_group = IC_file.create_group('PartType5')
        dset = bh_group.create_dataset("classes", data=classArr)
        dset = bh_group.create_dataset("class", data=np.zeros(N_bh, dtype="|u1") )
        dset = bh_group.create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = bh_group.create_dataset("Velocities", data=vel, dtype="<f8")


        pos = random_state.random((N_gas, 3))-0.5
        vel = np.zeros((N_gas, 3))

        gas_group = IC_file.create_group('PartType0')
        dset = gas_group.create_dataset("classes", data=classArr)
        dset = gas_group.create_dataset("class", data=np.ones(N_gas, dtype="|u1") )
        dset = gas_group.create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = gas_group.create_dataset("Velocities", data=vel, dtype="<f8")
        dset = gas_group.create_dataset("InternalEnergy", data=np.ones(N_gas), dtype="<f8")



        IC_file.create_group('Header')
        IC_file['Header'].attrs['Time'] = 1.

        IC_file.close()



    def test_BH_parallel_accretion(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/testParallelAccretion.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        ## Serial
        self.threads = 1
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_parallel_accretion'
        self.run_pkdgrav3()
        self.plot('serial')


        ## Multiple threads
        self.threads = 4
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_threads_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_parallel_accretion'
        self.run_pkdgrav3()
        self.plot('threads')

        ## MPI+threads
        self.prepkdcmd = 'mpirun -np 4 '
        self.threads = 4
        self.output_dir = self.pkdgrav3_test_dir + '/snaps_mpi_%s'%(self.identifier)
        self.output_file = self.output_dir + '/BH_parallel_accretion'
        self.run_pkdgrav3()
        self.plot('mpi')


    def plot(self, mode):
        from BlackholeTests.TestParallelAccretion.plot_evolution import run_growth,run_conservation
        run_growth(self.output_file, self.pkdgrav3_test_dir + '/parallel_accretion_%s_%s.png'%(mode,self.identifier))
        run_conservation(self.output_file, self.pkdgrav3_test_dir + '/conservation_%s_%s.png'%(mode,self.identifier))


