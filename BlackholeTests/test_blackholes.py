from pkdtest import PKDGRAV3TestCase

class BlackholeTestsDummy(PKDGRAV3TestCase):
    title = "Black hole tests"
    description = "Tests of the blackhole module. Details can be found at the README"
    plots = [ {"file": "TestIO/testIO",
                "caption": "I/O test to check basic gravity behaviour. Crosses should match."},
              {"file": "TestMergers/merger_comparison",
               "caption": "Merger tests"},
              {"file": "TestEvrard/evolution",
               "caption": "Black hole formation from evard collapse"},
              {"file": "TestGasMerger/trajectory",
               "caption": "Merge of two gas balls with seeded BHs"},
              {"file": "TestGrowth/growth_all",
               "caption": "Black hole growth through accretion using different models"},
              {"file": "TestGrowth/conservation_alpha",
               "caption": "Conservation whilst accretting"},
              {"file": "TestFeedback/growth",
               "caption": "Simulated and analytical expected growth"},
              {"file": "TestFeedback/conservation",
               "caption": "Consevation whilst doing feedback"},
              {"file": "TestFeedback/distribution",
               "caption": "Projected internal energy"},
              {"file": "TestParallelAccretion/conservation_mpi",
               "caption": "Conservation under accretion by multiple BH using MPI"},
              {"file": "TestParallelAccretion/parallel_accretion_mpi",
               "caption": "Accretion history of all the blackholes"}]

    folder_name = "BlackholeTests"

    def test_dummy(self,result=None, *args, **kwargs):
        pass
