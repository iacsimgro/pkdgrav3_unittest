from pkdtest import PKDGRAV3TestCase


class SedovTest(PKDGRAV3TestCase):
    title = "Sedov explosion"
    description = ("This test is aimed at checking the conservation of energy. "
                   "Furthermore, it is also a good test for the spherical symmetry of the shell.")
    plots = [{"file": "sedov_solution_MFM",
              "caption": "Sedov solution with MFM"},
             {"file": "sedov_energy_MFM",
              "caption": "Energy conservation of the Sedov vortex with MFM"},
             {"file": "sedov_solution_MFV",
              "caption": "Sedov solution with MFV"},
             {"file": "sedov_energy_MFV",
              "caption": "Energy conservation of the Sedov vortex with MFV"}]

    schemes = ["MFM", "MFV"]
    __name__ = "Sedov"
    folder_name = "sedov"


    def generateIC(self):
        import numpy as np
        from pykdgrav3_utils import hdf5

        # Size of the box
        L = 1.

        # The particles are equally distributed along the three axis, from -0.5 up to 0.5
        n = 64
        N=n**3

        x = np.linspace(-0.5, 0.5, num=n, endpoint=False)
        mesh = np.meshgrid(x,x,x)

        x = mesh[0].flatten()
        y = mesh[1].flatten()
        z = mesh[2].flatten()
        del mesh

        gamma = 5./3.  # gas adiabatic constant
        rho0 = 1.
        mass0 =  rho0*L*L*L/N

        # Durier & Dalla Vecchia
        rho0 = 1.
        E0 = 1e-5 # Total
        E = 1./mass0
        R = 1.
        mu = 1. # m =1.67e-24   # cgs hydrogen masss
        kb = 1. #1.38e-16 # cm2 g s-2 K-1
        Uint0 = E0*(gamma-1.)/N/mass0
        print (Uint0)
        c_s = np.sqrt(gamma*R*Uint0*(gamma-1.)/mu)  # km/s


        # Position of the blast
        offsetX =  0. #0.05
        offsetY =  0. #-0.13
        offsetZ =  0  #0.08

        print ('c_s =', c_s)

        # We set the particle mass to have rho0
        mass = np.ones_like(x)*mass0

        print ('E*mass0: ', E*mass0)
        print ('E: ', E)


        Uint = np.ones(N)*Uint0


        r = np.sqrt((x-offsetX)**2 + (y-offsetY)**2+ (z-offsetZ)**2)
        pos = np.stack((x, y, z), axis=1)
        vel = np.zeros_like(pos)

        N_center = 7
        ord_r = np.argsort(r)
        mask = ord_r[:N_center]

        h = 0.015625*0.01

        fac = 1./np.pi
        h1 = 1. / h
        q = r * h1
        fac = fac * h1 * h1 * h1
        tmp2 = 2. - q
        val = np.zeros_like(q)
        mask = q<2.
        val[mask] = 0.25 * tmp2[mask] * tmp2[mask] * tmp2[mask]
        mask = q<1.
        val[mask] = 1 - 1.5 * q[mask] * q[mask] * (1 - 0.5 * q[mask])
        w = val*fac
        print("Kicking %d particles"%np.sum(w>0))

        #"""
        # Thermal energy injection
        norm = w/np.sum(w)
        print ("Background temperature: ", np.mean(Uint*(gamma-1.)))
        E_perP = E*norm
        Uint += E_perP
        print ("Central Temperatures: ", Uint[mask]*(gamma-1.))

        print ("Thermal energy: ", np.sum(Uint))
        #"""


        """
        # Kinetic energy injection
        norm = w/np.sum(w[r>0])
        Ekin_perP = E*norm
        print(norm)
        v_perP = np.sqrt(2.*Ekin_perP)
        mask = r>0
        for i in np.arange(3):
            vel[mask,i] = v_perP[mask]*pos[mask,i]/r[mask]
        print ("Kinetic energy: ", np.sum(0.5*mass*np.sum(vel**2,axis=1)))
        """

        """
        # Single particle kinetic kick
        Ekin_perP = E
        v_perP = np.sqrt(2.*Ekin_perP)
        vel[r==0, 0] = v_perP
        print ("Kinetic energy: ", np.sum(0.5*mass*np.sum(vel**2,axis=1)))
        """




        gas_dict = {'Coordinates': pos,
                    'Masses': mass.flatten(),
                    'Velocities': vel,
                    'InternalEnergy':Uint.flatten(),
                    'SmoothingLength': np.full( N, np.power(3./(4.*np.pi*N), 1./3.))}

        hdf5.save({'PartType0':gas_dict}, self.input_file)



    def test_sedov(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/sedov.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()
        for scheme in self.schemes:
            self.set_scheme(scheme)
            self.compile()

            self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s_%s'%(scheme, self.identifier)
            self.output_file = self.output_dir + '/sedov'
            self.run_pkdgrav3()

            self.plot(scheme)


    def plot(self, scheme):
        from sedov.plot import run
        run(self.output_file+'.00016.0', self.identifier, self.pkdgrav3_test_dir + '/sedov_solution_%s_%s.png'%(scheme, self.identifier))

        from sedov.energy import run
        run(self.output_file, self.pkdgrav3_test_dir+'/sedov_energy_%s_%s.png'%(scheme, self.identifier) )


