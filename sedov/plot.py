import sys, os

def run(snapname, ident, path=None):
    import matplotlib
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    from sedov.sedov_solution import sedov_solution
    from pykdgrav3_utils import hdf5


    # We first get the center of the explosion, which may be offset if using glass IC
    #snap = hdf5.read_single(snap[:-5]+"000.0", return_units=False)
    #gas = snap['PartType0']
    #p_exp = np.argmax(gas['InternalEnergy'])
    off_x = 0.0 #gas['Coordinates'][p_exp,0]
    off_y = 0.0 #gas['Coordinates'][p_exp,1]
    off_z = 0.0 #gas['Coordinates'][p_exp,2]

    #snap.close()


    gamma = 5./3.


    snap = hdf5.read_single(snapname, return_units=False)
    gas = snap['PartType0']

    p_exp = np.argmax(gas['InternalEnergy'])
    mask = gas['InternalEnergy'][...] > np.mean(gas['InternalEnergy'][...])
    off_x = np.mean(gas['Coordinates'][...][mask,0])
    off_y = np.mean(gas['Coordinates'][...][mask,1])
    off_z = np.mean(gas['Coordinates'][...][mask,2])


    x = gas['Coordinates'][:,0]-off_x
    y = gas['Coordinates'][:,1]-off_y
    z = gas['Coordinates'][:,2]-off_z
    dens = gas['Density'][...]
    temp = gas['InternalEnergy'][...]*(gamma-1.)

    vx = gas['Velocities'][:,0]
    vy = gas['Velocities'][:,1]
    vz = gas['Velocities'][:,2]

    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arccos(z/r)
    phi = np.arctan2(y, x)
    v_r = np.sin(theta) * np.cos(phi) * vx + np.sin(theta) * np.sin(phi) * vy   + np.cos(theta) * vz
    mass = gas['Masses'][...]
          
    size = .1

    f,axs = plt.subplots(1,4, sharex=False, figsize=(18,5))




    dt = 5e-3
    t = float(snapname.split('.')[-2]) * dt
    r_sol, rho_sol = sedov_solution(t)
    axs[0].plot(r_sol, rho_sol, 'r')

    axs[0].scatter(r, dens, marker='.', s=size, c='k')
    #axs[0].set_xlim(0,0.5)
    axs[0].set_xlabel('r')
    axs[0].set_ylabel('Density')


    axs[1].scatter(r, dens*temp, marker='.', s=size, c='k')
    axs[1].set_xlabel('r')
    #axs[1].set_ylim(0,5e-6)
    axs[1].set_ylabel(r'Pressure')


    axs[2].scatter(r, mass, marker='.', s=size, c='k')
    axs[2].set_ylabel(r'$v_r$')
    axs[2].set_xlabel('r')

    # We show a density 'cut' near x=0
    mask = np.abs(z) < 1./32

    #im = axs[3].tricontourf(x[mask], y[mask], np.log10(temp[mask]))
    #plt.colorbar(im, ax=axs[3], label=r'$\log_{10}$ (Temperature)')
    axs[3].scatter(x[mask], y[mask], c='k', s=0.1, marker='.')

    # We draw a circle we the shock should be
    theta = np.linspace(0., 2.*np.pi, 50)
    axs[3].plot(r_sol[-2]*np.cos(theta), r_sol[-2]*np.sin(theta), 'r')
    axs[3].set_xlabel('x')
    axs[3].set_ylabel('y')
    plt.tight_layout()

    if path is not None:
        print (path)
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    if len(sys.argv) > 2:
        run(sys.argv[1], os.environ.get('IDENTIFIER'), sys.argv[2])
    else:
        run(sys.argv[1], os.environ.get('IDENTIFIER'))
