def run(snapname, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    from pykdgrav3_utils import hdf5
    from glob import glob

    files = sorted(glob(snapname))


    def plot_orbit_plane(ax1, ax2, files, coord_p, coord_v):
        coord_p2 = 0
        if coord_p2 == coord_p or coord_p2 == coord_v:
            coord_p2 += 1
        if coord_p2 == coord_p or coord_p2 == coord_v:
            coord_p2 += 1

        s0 = hdf5.read_single(files[0], return_units=False)
        idsx = s0['PartType1']['ParticleIDs'][s0['PartType1']['Coordinates'][:,coord_p] != 0]
        s0.close()

        xs = np.empty((len(files), len(idsx)))
        ys = np.empty((len(files), len(idsx)))
        zs = np.empty((len(files), len(idsx)))
        for i, file in enumerate(files):
            snap = hdf5.read_single(file, return_units=False)
            _, _, ind = np.intersect1d(idsx, snap['PartType1/ParticleIDs'], return_indices=True, assume_unique=True)
            xs[i,:] = snap['PartType1']['Coordinates'][:,coord_p][ind]
            ys[i,:] = snap['PartType1']['Coordinates'][:,coord_v][ind]
            zs[i,:] = snap['PartType1']['Coordinates'][:,coord_p2][ind]
            snap.close()

        for i in range(xs.shape[1]):
            ax1.plot(xs[:,i], ys[:,i], '.-', alpha=0.4)
            ax2.plot(xs[:,i], zs[:,i], '.-', alpha=0.4)

        labels = ['x', 'y', 'z']
        ax1.set_xlabel(labels[coord_p])
        ax2.set_xlabel(labels[coord_p])
        ax1.set_ylabel(labels[coord_v])
        ax2.set_ylabel(labels[coord_p2])
        ax1.set_xlim(-200,200)
        ax1.set_ylim(-200,200)
        ax2.set_xlim(-200,200)
        ax2.set_ylim(-0.5,0.5)

    f, axs = plt.subplots(2,3, figsize=(11,8))
    plot_orbit_plane(axs[0][0], axs[1][0],files, 0, 2)
    plot_orbit_plane(axs[0][1], axs[1][1],files, 1, 0)
    plot_orbit_plane(axs[0][2], axs[1][2],files, 2, 1)
    plt.tight_layout()
    if path is not None:
        f.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], sys.argv[2])
    else:
        run(sys.argv[1])
