from pkdtest import PKDGRAV3TestCase


class NFWDMTest(PKDGRAV3TestCase):
    title = "NFW orbit test"
    description = ("Test of the external NFW acceleration. "
                   "Dark matter is placed in circular orbits around the center "
                   "of the halo. "
                   "Trajectories should be circular and coplanar.")
    plots = [{"file": "nfw",
              "caption": "Orbital and perpendicular planes"}]

    schemes = ["MFM"]
    __name__ = "NFWDM"
    folder_name = "nfw_halo"

    compile_flags = {"NFW_POTENTIAL":True}
    max_threads = 1

    def generateIC(self):
        import numpy as np
        from pykdgrav3_utils import hdf5

        R = 200 #kpc
        M = 1e-10
        n = 20
        N=n**3

        side = np.linspace(1., R, num=n, endpoint=False)
        zero = np.zeros_like(side)
        x = np.concatenate([side, zero, zero])
        y = np.concatenate([zero, side, zero])
        z = np.concatenate([zero, zero, side])

        r2 = side*side

        dMsolUnit = 1e10
        dKpcUnit = 1.0
        H0 = 70.4
        Omega_m = 0.3

        c = 5
        Delta_h = 200
        M_h = 1e12
        eps = 5

        from pykdgrav3_utils import units
        u = units.units(dMsolUnit, dKpcUnit)
        H0code = H0/ u.dKmPerSecUnit * ( dKpcUnit / 1e3)
        rho_crit = 3. * H0code * H0code / ( 8. * np.pi)
        rho_mean = rho_crit * Omega_m

        ## Add a softnening to avoid divergence at r->0 */
        epsilon =  eps/dKpcUnit
        epsilon2 = epsilon*epsilon

        ## Assume centred in [0,0,0] */
        rr = np.sqrt(r2 + epsilon2)

        ## Eq. 7.136
        rho_h = Delta_h * rho_crit * Omega_m
        r_h = np.power( 3. * M_h/dMsolUnit / (4. * np.pi * rho_h ), 1/3. )
        ## Eq. 7.140
        r_s = r_h / c

        ## Eq. 7.141
        den = np.log(1.+c) - c/(1.+c)
        delta_char = Delta_h / 3. * ( c * c * c )/den

        ## Eq. 7.139
        xx = rr/r_h
        cx = c * xx
        enclosed_mass = 4. * np.pi * rho_mean * delta_char * \
                        r_s * r_s * r_s * ( np.log(1 + cx) - cx/(1+cx) )

        term = - enclosed_mass  / (rr*rr*rr)

        ## Eq. 11.24
        V_c = np.sqrt( enclosed_mass / rr )

        vx = np.concatenate([zero,V_c , zero])
        vy = np.concatenate([zero,zero, V_c ])
        vz = np.concatenate([ V_c,zero, zero])

        pos = np.stack((x, y, z), axis=1)
        vel = np.stack((vx, vy, vz), axis=1)
        mass = np.full_like(x, M/x.shape[0])


        dm_dict =  {'Coordinates': pos,
                    'Masses': mass,
                    'Velocities': vel}

        hdf5.save({'PartType1':dm_dict}, self.input_file)



    def test_nfw(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/nfw.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()
        self.compile_flags = {"NFW_POTENTIAL":True}
        self.compile()

        self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_file = self.output_dir + '/nfw'
        self.run_pkdgrav3()

        self.plot()

    def plot(self):
        from nfw_halo.plot import run
        run(self.output_file+'.0*.0', path=self.pkdgrav3_test_dir + '/nfw_%s.png'%(self.identifier))


