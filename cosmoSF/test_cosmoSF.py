from pkdtest import PKDGRAV3TestCase


class CosmoSFTest(PKDGRAV3TestCase):
    title = "Small cosmological box"
    description = ("Simulation of a cosmological box "
                      "with all the galaxy formation physics."
                      "\n"
                      "We compare with the Madau & Dickinson SFH, "
                      "but due to the small box and low resolution "
                      "a very close match is not really expected.")
    plots = [{"file": "plot",
              "caption": "Summary of the simulation at the last snapshot"},
             {"file": "rhoT",
              "caption": "Phase diagram evolution for checking cooling and eEOS"},
             {"file": "SFH",
              "caption": "Star formation history"}]

    schemes = ["MFM"]
    __name__ = "CosmoSF"
    folder_name = "cosmoSF"

    compile_flags = {'COOLING':True,'STAR_FORMATION':True,'FEEDBACK':True,'BLACKHOLES':True,'EEOS_POLYTROPE':True}

    def generateIC(self):
        import pynbody
        import numpy as np
        from pykdgrav3_utils import hdf5, units

        print ("## Starting IC conversion to hdf5")

        KBOLTZ =   1.38e-16
        MHYDR =  1.67e-24
        MSOLG =  1.99e33
        GCGS = 6.67e-8
        KPCCM =  3.085678e21
        SIGMAT=  6.6524e-25
        LIGHTSPEED = 2.9979e10

        ### Load IC and header information
        IC = pynbody.load(self.pkdgrav3_test_dir+'/ics_2lptic')
        L = IC.header.BoxSize #Mpc h-1
        z = IC.header.redshift
        a = IC.header.time
        h_par = IC.header.HubbleParam

        N_gas = IC.header.npart[0]
        N_dm = IC.header.npart[1]
        N_tot = N_gas + N_dm

        mass_gas = IC.header.mass[0]
        mass_dm = IC.header.mass[1]


        mass_tot = mass_gas*N_gas + mass_dm*N_dm  # 1e10 Msol


        ## We set the unit system such that rho_crit=1, G=1
        # This means that the matter density in this system is Omega_m
        rho_m = IC.header.Omega0

        u = units.units(mass_tot*1e10/h_par/rho_m, L*1e3/h_par, verbose=True)

        ## We do some safety checks
        H0 = h_par*100. / u.dKmPerSecUnit * ( u.dKpcUnit / 1e3)
        rho_crit = 3.*H0*H0/(8.*np.pi)

        if np.abs(H0-np.sqrt(8.*np.pi/3.))/H0 > 0.01:
            print ("\n\nERROR *#*#*# The hubble constant in this unit system is not OK\n\n")
            print ("H0 is ", H0, " but should be ", np.sqrt(8.*np.pi/3.))

        if np.abs(rho_crit-1)>0.01:
            print ("\n\nERROR *#*#*# The critical density in this unit system is not OK\n\n")
            print ("rho_crit is ", rho_crit, "but should be unity")


        soft = (3 * L**3 / (4.*np.pi*(N_dm)) )**(1./3.) /30.
        soft = (3 * 1**3 / (4.*np.pi*(N_dm)) )**(1./3.) /30.  * u.dKpcUnit
        print ("Softening: %.2f kpc (%.2e)"%(soft, soft/u.dKpcUnit))
        print ("Softening phys: 1.5 kpc (%.2e)"%(1.5/u.dKpcUnit))

        ### GAS
        gamma = 5./3.


        pos = np.zeros((N_gas, 3), dtype=np.float32)
        pos[:,0] = IC.gas['x']/L  - 0.5
        pos[:,1] = IC.gas['y']/L  - 0.5
        pos[:,2] = IC.gas['z']/L  - 0.5
        vel = np.zeros((N_gas, 3), dtype=np.float32)
        vel[:,0] = IC.gas['vx']  * np.sqrt(a) / a / u.dKmPerSecUnit
        vel[:,1] = IC.gas['vy']  * np.sqrt(a) / a / u.dKmPerSecUnit
        vel[:,2] = IC.gas['vz']  * np.sqrt(a) / a / u.dKmPerSecUnit
        mass = np.full(N_gas, mass_gas*1e10 / h_par / u.dMsolUnit)
        uint =  np.full(N_gas, 100 * u.dGasConst / (gamma-1.) )



        gas_dict = {'Coordinates': pos,
                    'Velocities': vel,
                    'InternalEnergy': uint,
                    'Metallicity': np.full(N_gas, 0.02),
                    'Masses': mass}

        ### DM
        pos = np.zeros((N_dm, 3), dtype=np.float32)
        pos[:,0] = IC.dm['x']/L  - 0.5
        pos[:,1] = IC.dm['y']/L  - 0.5
        pos[:,2] = IC.dm['z']/L  - 0.5
        vel = np.zeros((N_dm, 3), dtype=np.float32)
        vel[:,0] = IC.dm['vx']  * np.sqrt(a) / a / u.dKmPerSecUnit
        vel[:,1] = IC.dm['vy']  * np.sqrt(a) / a / u.dKmPerSecUnit
        vel[:,2] = IC.dm['vz']  * np.sqrt(a) / a / u.dKmPerSecUnit
        mass = np.full(N_dm, mass_dm*1e10 / h_par / u.dMsolUnit)

        dm_dict = {'Coordinates': pos,
                   'Masses': mass,
                   'Velocities': vel}

        hdf5.save({'PartType0':gas_dict, 'PartType1':dm_dict}, self.input_file, time=IC.header.time)



    def test_cosmoSF(self,result=None, *args, **kwargs):
        import pathlib, shutil, glob

        self.param_file = self.pkdgrav3_test_dir + '/cosmoSF_L6.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()

        self.set_scheme('MFM')
        self.compile()

        # Reference run to z=0
        base_dir = self.pkdgrav3_test_dir + '/snaps_%s'%(self.identifier)
        self.output_dir = base_dir
        self.output_file = self.output_dir + '/cosmoSF'
        self.run_pkdgrav3()
        self.plot()
        # Save this as _full
        if not self.plot_only and not self.dry_run:
            shutil.rmtree(base_dir+'_full', ignore_errors=True)
            shutil.copytree(base_dir, base_dir+'_full')
        self.output_dir = base_dir+'_full'
        self.output_file = self.output_dir + '/cosmoSF'


        # Restart from a checkpoint
        self.param_file = self.output_dir + '/cosmoSF.00007.chk'
        # Initially will be saved in the base_dir
        if not self.plot_only and not self.dry_run:
            shutil.rmtree(base_dir, ignore_errors=True)
            pathlib.Path(base_dir).mkdir(exist_ok=True)
            # But leave behind the checkpoints
            for chk in glob.glob(base_dir+'_full/*chk*'):
                shutil.copy(chk, base_dir+'/')
            self.run_pkdgrav3()
            # And the moved as _checkpoint
            shutil.rmtree(base_dir+'_checkpoint', ignore_errors=True)
            shutil.copytree(base_dir, base_dir+'_checkpoint')

        self.output_dir = base_dir+'_checkpoint'
        self.output_file = self.output_dir + '/cosmoSF'
        self.plot()

        # Restart from a snapshot
        # We can directly run in the restart folder, no need to copy stuff
        self.param_file = self.pkdgrav3_test_dir + '/cosmoSF_L6.par'
        self.output_dir = base_dir+'_restart'
        pathlib.Path(self.output_dir).mkdir(exist_ok=True)
        self.input_file = self.output_file+'.00008.0'
        self.test_args = '-nstart 8 -z 0.222876'
        self.output_file = self.output_dir + '/cosmoSF'
        self.run_pkdgrav3()
        self.plot()

        self.plot_SFH()
        self.plot_rhoT()


    def plot(self):
        from cosmoSF.plot_cosmoSF import run
        run(self.output_file+'.00010.0', self.output_dir.replace('snaps', 'plot')+'.png')

    def plot_SFH(self):
        from cosmoSF.SFH import run
        run([self.pkdgrav3_test_dir + '/snaps_%s_full'%(self.identifier),       \
            self.pkdgrav3_test_dir + '/snaps_%s_checkpoint'%(self.identifier), \
            self.pkdgrav3_test_dir + '/snaps_%s_restart'%(self.identifier)],   \
            self.pkdgrav3_test_dir+'/SFH_%s.png'%self.identifier)

    def plot_rhoT(self):
        from cosmoSF.plot_rhoT import run
        run(self.pkdgrav3_test_dir + '/snaps_%s_full'%(self.identifier), \
            self.pkdgrav3_test_dir + '/rhoT_%s.png'%self.identifier)



