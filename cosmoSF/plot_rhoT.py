def run(folder, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from pykdgrav3_utils import hdf5, units
    import numpy as np
    import glob


    # We just take some high-z snapshots
    files = sorted(glob.glob(folder+"/cosmoSF.0000[1,2,5].0"))

    f, axs = plt.subplots(3,2, sharex=True, sharey=False, figsize=(6.5,8.5))


    for i in range(len(files)):
        print(files[i])

        snap, u = hdf5.read_single(files[i])
        z = snap['Header'].attrs['Redshift']
        a = 1./(z+1)
        rhoCrit_60 = np.double(60.)*0.048* 0.75 / u.MHYDR * u.dGmPerCcUnit / a**3

        gas = snap['PartType0']
        gasDens = gas['Density'][...] * 0.75 / u.MHYDR * u.dGmPerCcUnit / a**3
        gasSFR = gas['StarFormationRate'] * u.dMsolUnit / u.dSecUnit*(365*24*3600)
        gasTemp = gas['Temperature'][...]
        gasMass = gas['Masses'][...] * u.dMsolUnit
        gasP = gas['InternalEnergy'][...]*gas['Density'][...] * u.dErgPerGmUnit * u.dGmPerCcUnit

        #################################
        ####  rho vs T phase diagram ####
        #################################
        bins = 50
        xed = np.linspace(-8,0,bins)
        yed = np.linspace(3,7,bins)
        H, xedges, yedges = np.histogram2d(np.log10(gasDens), np.log10(gasTemp), bins=[xed,yed])
        axs[i][0].imshow(np.log10(H.T), extent=(xedges[0], xedges[-1], yedges[0], yedges[-1]), aspect='auto', origin='lower')

        # Thresholds
        axs[i][0].axvline(np.log10(rhoCrit_60), color='r', linestyle=':', label="SF overdensity threshold")
        axs[i][0].axvline(np.log10(0.1), color='g', linestyle='-', label="SF nH threshold")

        # Highlight the high density particles to easily spot the eEOS
        mask_high = gasDens>1e-3
        axs[i][0].plot(np.log10(gasDens[mask_high]), np.log10(gasTemp[mask_high]), 'k.')


        axs[i][0].set_ylabel(r'log T')
        axs[i][0].set_xlabel(r'log n$_H$')
        axs[i][0].set_xlim(-8, 2)
        axs[i][0].set_ylim(3, 7)
        axs[i][0].set_title(r'$z=%.2f$'%z)


        #####################################
        ####  SFR-rho compared with S&DV ####
        #####################################
        # Expectation from eq 20 of Schaye&Dalla Vecchia 2007 and PKDGRAV3 defaults
        SFR = 5.99e-10 * gasMass * ( 0.3 * gasP/u.KBOLTZ * 1e-3 / a**3)**0.2
        axs[i][1].plot(np.log10(gasDens), np.log10(SFR), 'k,', label='From S\&DV07 eq 20')
        axs[i][1].plot(np.log10(gasDens), np.log10(gasSFR), 'r.', label='From gas SFR')
        axs[i][1].axvline(np.log10(0.1), color='g', linestyle='-')
        axs[i][1].set_ylabel(r'SFR [M$_\odot$ yr$^{-1}$]')
        axs[i][1].set_xlabel(r'log n$_H$')
        axs[i][1].set_ylim(-3, -0.5)


        snap.close()


    axs[0][0].legend()
    axs[0][1].legend()

    plt.tight_layout()

    if path is not None:
        plt.savefig(path)
    else:
        plt.show()


if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], sys.argv[2])
    else:
        run(sys.argv[1])
