import sys

def run(filename, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from pykdgrav3_utils import hdf5, units
    import numpy as np



    def running_median(x, y, N):
        min_x = np.min(x)
        dx = np.abs(np.max(x) - min_x)/ N
        result_y = np.zeros(N)
        result_x = np.zeros(N)
        result_sigma = np.zeros(N)
        for i in range(N):
            mask = np.logical_and(x<min_x+(i+1)*dx , x>min_x+i*dx)
            result_x[i] = min_x +(i+0.5)*dx
            result_y[i] = np.median(y[mask])
            result_sigma[i] = np.std(y[mask])
        return result_x, result_y, result_sigma




    snap, u = hdf5.read_single(filename)
    gas = snap['PartType0']
    dark = snap['PartType1']
    star = snap['PartType4']
    bh = snap['PartType5']

    a = 1./(snap['Header'].attrs['Redshift']+1)


    print (snap['Header'].attrs['NumPart_Total'])
    print ('Total mass %e'%(np.sum(gas['Masses'][...])+np.sum(dark['Masses'][...])))
    print ('mgas %e'% (gas['Masses'][0]*u.dMsolUnit) )
    print ('mdm %e'% (dark['Masses'][0]*u.dMsolUnit) )

    dmx= dark['Coordinates'][:,0]*u.dKpcUnit
    dmy= dark['Coordinates'][:,1]*u.dKpcUnit
    dmz= dark['Coordinates'][:,2]*u.dKpcUnit

    gasx = gas['Coordinates'][:,0]*u.dKpcUnit
    gasy = gas['Coordinates'][:,1]*u.dKpcUnit
    gasz = gas['Coordinates'][:,2]*u.dKpcUnit

    bhx = bh['Coordinates'][:,0]*u.dKpcUnit
    bhy = bh['Coordinates'][:,1]*u.dKpcUnit
    bhz = bh['Coordinates'][:,2]*u.dKpcUnit

    rhoCrit_60 = np.double(60.)*0.048* 0.75 / u.MHYDR * u.dGmPerCcUnit / a**3
    gasDens = np.array(gas['Density'][...],dtype=np.double) * 0.75 / u.MHYDR * u.dGmPerCcUnit / a**3
    gasTemp = gas['Temperature'][...] 
    gasIntEnergy = gas['InternalEnergy'][...]

    minPhi = np.argmin(gas['Potential'])
    CMx = gasx[minPhi]
    CMy = gasy[minPhi]
    CMz = gasz[minPhi]

    size = .1

    fig, [axs,axs2] = plt.subplots(2,4, sharex=False, figsize=(15,7))

    fig.suptitle(r'$a = %.5f \; z = %.3f $'%(a, 1./a - 1.))
    fig.subplots_adjust(top=0.9, left=0.05, right=0.95, wspace=0.2, hspace=0.2)


    #################################
    #### Temperature histogram  #####
    #################################

    H, edges = np.histogram(np.log10(gasTemp), bins=100)
    axs[0].plot( 10**(0.5*(edges[1:]+edges[:-1])), H  , 'b')
    axs[0].axvline(10**edges[np.argmax(H)], color='b', linestyle='--')
    axs[0].set_xlabel(r'T')
    axs[0].set_ylabel(r'Number of particles')
    axs[0].set_xlim(1e3, 1e7)
    axs[0].set_ylim(0.8, 7e2)
    axs[0].loglog()



    #################################
    #### Density histogram  #########
    #################################
    bins = 50
    H, edges = np.histogram(np.log10(gasDens), bins=bins)
    axs[1].plot( 10**(0.5*(edges[1:]+edges[:-1])), H  , 'b')
    axs[1].axvline(rhoCrit_60, color='r', linestyle=':')
    axs[1].axvline(rhoCrit_60/60., color='b', linestyle=':')
    axs[1].set_xlabel(r'n$_H$')
    axs[1].set_ylabel(r'Number of particles')
    axs[1].set_xlim(1e-8, 1e-2)
    axs[1].set_ylim(0.8, 3e3)
    axs[1].loglog()

    #################################
    ####  rho vs T phase diagram ####
    #################################
    bins = 70
    xed = np.linspace(-8,-2,bins)
    yed = np.linspace(3,7,bins)
    H, xedges, yedges = np.histogram2d(np.log10(gasDens), np.log10(gasTemp), bins=[xed,yed])
    axs[2].imshow(np.log10(H.T), extent=(xedges[0], xedges[-1], yedges[0], yedges[-1]), aspect='auto', origin='lower')
    axs[2].axvline(np.log10(rhoCrit_60), color='r', linestyle=':')
    axs[2].axvline(np.log10(rhoCrit_60/60.), color='b', linestyle=':')
    axs[2].set_ylabel(r'log T')
    axs[2].set_xlabel(r'log n$_H$')
    axs[2].set_xlim(-8, -2)
    axs[2].set_ylim(3, 7)



    #"""
    #################################
    ##### Velocity histograms #######
    #################################

    dmModV = np.sqrt(dark['Velocities'][:,0]**2+ dark['Velocities'][:,1]**2 + dark['Velocities'][:,2]**2)*u.dKmPerSecUnit 
    H, edges = np.histogram(np.log10(dmModV), bins=1000)
    axs[3].plot( 10**(0.5*(edges[1:]+edges[:-1])), H , 'k' )
    axs[3].text( 0.7, 0.7, 'dm: %.1f km/s'%(10**edges[np.argmax(H)]), transform=axs[3].transAxes)
    axs[3].axvline(10**edges[np.argmax(H)], color='k', linestyle='--')

    gasModV = np.sqrt(gas['Velocities'][:,0]**2+ gas['Velocities'][:,1]**2 + gas['Velocities'][:,2]**2)*u.dKmPerSecUnit 
    H, edges = np.histogram(np.log10(gasModV), bins=1000)
    axs[3].plot( 10**(0.5*(edges[1:]+edges[:-1])), H  , 'b')
    axs[3].text( 0.7, 0.6, 'gas: %.1f km/s'%(10**edges[np.argmax(H)]), transform=axs[3].transAxes)
    axs[3].axvline(10**edges[np.argmax(H)], color='b', linestyle='--')

    starModV = np.sqrt(star['Velocities'][:,0]**2+ star['Velocities'][:,1]**2 + star['Velocities'][:,2]**2)*u.dKmPerSecUnit 
    H, edges = np.histogram(np.log10(starModV), bins=100)
    axs[3].plot( 10**(0.5*(edges[1:]+edges[:-1])), H  , 'g')
    axs[3].text( 0.7, 0.5, 'star: %.1f km/s'%(10**edges[np.argmax(H)]), transform=axs[3].transAxes)
    axs[3].axvline(10**edges[np.argmax(H)], color='g', linestyle='--')

    axs[3].set_xlim(1, 1e3)
    axs[3].set_ylim(0.8, 2e2)
    axs[3].loglog()
    #"""

    """
    #################################
    ###  Mean molecular weight  #####
    #################################


    mu = u.dGasConst * gasTemp/gasIntEnergy / (5./3. - 1.)
    h = gas['SmoothingLength']*u.dKpcUnit
    bins = np.arange(np.max(h)+1)-0.5
    axs[3].hist(h, bins=bins, density=True)
    axs[3].set_xlabel(r'Smoothing length [log kpc]')

    axs[3].semilogy()
    """




    ###########################
    ## Projected temperature ##
    ###########################

    nbins = 100
    extent = (-u.dKpcUnit*0.5, u.dKpcUnit*0.5,-u.dKpcUnit*0.5, u.dKpcUnit*0.5)


    H, xedges, yedges = np.histogram2d(gasx, gasy, bins=nbins)
    Htemp, _, _ = np.histogram2d(gasx, gasy, bins=[xedges, yedges], weights=gasTemp)
    im = axs2[0].imshow(np.log10(Htemp/H).T, origin='lower', extent=extent, vmin=3.5, vmax=7.5)
    plt.colorbar(im, ax=axs2[0])
    axs2[0].set_xlabel('x')
    axs2[0].set_xlabel('y')
    axs2[0].plot(CMx, CMy, 'ro', alpha=0.8)
    axs2[0].plot(bhx, bhy, 'rx', alpha=0.8)

    H, xedges, yedges = np.histogram2d(gasz, gasy, bins=nbins)
    Htemp, _, _ = np.histogram2d(gasz, gasy, bins=[xedges, yedges], weights=gasTemp)
    im = axs2[1].imshow(np.log10(Htemp/H).T, origin='lower', extent=extent, vmin=3.5, vmax=7.5)
    plt.colorbar(im, ax=axs2[1])
    axs2[1].set_xlabel('z')
    axs2[1].plot(CMz, CMy, 'ro', alpha=0.8)
    axs2[1].plot(bhz, bhy, 'rx', alpha=0.8)


    ###########################
    ## Projected DM density  ##
    ###########################

    H, xedges, yedges = np.histogram2d(dmx, dmy, bins=nbins)
    im = axs2[2].imshow(np.log10(H).T, origin='lower', extent=extent)
    plt.colorbar(im, ax=axs2[2])
    axs2[2].set_xlabel('x')
    axs2[2].plot(CMx, CMy, 'ro', alpha=0.8)
    axs2[2].plot(bhx, bhy, 'rx', alpha=0.8)

    H, xedges, yedges = np.histogram2d(dmz, dmy, bins=nbins)
    im = axs2[3].imshow(np.log10(H).T, origin='lower', extent=extent)
    plt.colorbar(im, ax=axs2[3])
    axs2[3].set_xlabel('z')
    axs2[3].plot(CMz, CMy, 'ro', alpha=0.8)
    axs2[3].plot(bhz, bhy, 'rx', alpha=0.8)



    if path is not None:
        plt.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    if len(sys.argv) > 2:
        run(sys.argv[1], sys.argv[2])
    else:
        run(sys.argv[1])
