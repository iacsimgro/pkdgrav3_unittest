try:
    import yt
except:
    print ("YT could not be imported, skipping plot")
    exit()

import matplotlib
import os
if os.environ.get('REMOTE_PLOT')!=None:
    print ("Plotting in remote enviroment")
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
from pykdgrav3_utils import hdf5, units
import numpy as np
import sys, struct

filename = sys.argv[1]
name = sys.argv[2]

snap, u = hdf5.read_single(filename)
gas = snap['PartType0']

kpc = snap['Units'].attrs['KpcUnit']
Msun = snap['Units'].attrs['MsolUnit']
kms = snap['Units'].attrs['KmPerSecUnit']

#coords = gas['Coordinates'] + 0.5 # To have the (0,0,0) at the corner

snap.close()

unit_base = {
    'length': (kpc, 'kpc'),
    'velocity': (kms, 'km/s'),
    'mass': (Msun, 'Msun')
}
bbox = [[-0.5, 0.5],[-0.5, 0.5],[-0.5, 0.5]]

a = yt.load(filename, unit_base=unit_base, bounding_box=bbox)
#a.domain_left_edge=[(-0.5, 'code_length'),(-0.5,'code_length'),(-0.5,'code_length')]
#a.domain_right_edge=[(0.5, 'code_length'),(0.5,'code_length'),(0.5,'code_length')]


ad = a.all_data()
cen = ad[("PartType0", "Coordinates")].mean(axis=0)
p = yt.ProjectionPlot(a, "z", ("deposit", "PartType0_smoothed_density"), \
        weight_field=("deposit", "PartType0_density")).save(name)


p = yt.ProjectionPlot(a, "z", ("deposit", "PartType0_smoothed_temperature"), \
        weight_field=("deposit", "PartType0_density")).save(name)
"""
f = yt.ProjectionPlot(a, 1, fields=("PartType0", "Temperature"), \
                      weight_field=("PartType0", "Density"),     \
                      width=(3000, 'kpc'), center=cen).save('test')

f = yt.ProjectionPlot(a, 1, fields=("PartType0", "Density"),     \
                      weight_field=("PartType0", "Density"),     \
                      width=(3000, 'kpc'), center=cen).save('test')
"""
