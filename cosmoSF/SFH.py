def run(folders, path=None):
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from pykdgrav3_utils import hdf5, units
    import numpy as np
    import glob


    # L = 6 Mpc
    u = units.units(2.747719e13, 6.0e3, verbose=False)

    if folders[-1][-3:] == 'png':
        folders.pop()
    f, ax = plt.subplots(1)

    def read_SFH_from_finelog(folder):
        filename = glob.glob(folder+'/*.finelog')

        searchfile = open(filename[0], "r")
        # We look for the last simulation period
        lastComment = 0
        i=0
        nLogs = 0
        for line in searchfile:
            if line[0]=='#':
                lastComment = i
                nLogs=0
            nLogs+=1
            i+=1

        # Now we read the star formation from this point
        searchfile.seek(0)
        dTime = np.zeros(nLogs)
        dStep = np.zeros(nLogs)
        starFormed = np.zeros(nLogs)
        massFormed = np.zeros(nLogs)
        i=0
        logi = 0
        for line in searchfile:
            if i>lastComment:
                off = i-lastComment
                if off%2==1:
                    splits = line.split(' ')
                    dStep[logi] = float(splits[0])
                    dTime[logi] = float(splits[1])
                    starFormed[logi] = int(splits[2])
                    massFormed[logi] = float(splits[3])
                    logi+=1
            i+=1


        return dStep[:logi], dTime[:logi], starFormed[:logi], massFormed[:logi]


    def read_SFH_from_snap(folder):
        filename = sorted(glob.glob(folder+'/cosmoSF.?????.0'))[-1]

        snap, u = hdf5.read_single(filename)

        # We mask the stars that were part of the IC
        mask = snap['PartType4/StellarFormationTime'][...] > 0

        star_formation_time = snap['PartType4/StellarFormationTime'][mask]*u.dSecUnit/(365*24*3600)/1e9 # Gyr
        star_mass = snap['PartType4/Masses'][mask]*u.dMsolUnit

        nbins = 10
        bins = np.linspace(0., np.max(star_formation_time), nbins)
        bin_size =  (bins[1]-bins[0])*1e9 # yr

        SFR, age_bins = np.histogram(star_formation_time, bins=bins, weights=star_mass/bin_size)

        return SFR, 0.5*(age_bins[1:]+age_bins[:-1])

    def read_SFR_from_mult_snaps(folder):
        filenames = sorted(glob.glob(folder+'/cosmoSF.?????.0'))
        nfiles = len(filenames)

        SFR = np.zeros(nfiles)
        time = np.zeros(nfiles)

        i=0
        for filename in filenames:
            snap, u = hdf5.read_single(filename)
            SFR[i] = np.sum(snap['PartType0/StarFormationRate']) * u.dMsolUnit / u.dSecUnit*(365*24*3600) / (u.dKpcUnit/1e3)**3
            time[i] = snap['Header'].attrs['Time'] * u.dSecUnit/(365*24*3600) / 1e9

            i+=1

        return SFR, time

    alpha = 1.0
    for folder in folders:
        dStep, dTime, starFormed, massFormed = read_SFH_from_finelog(folder)
        dTime *= u.dSecUnit /(3600*24*365) / 1e9
        SFH = np.diff(massFormed)/np.diff(dTime*1e9) * u.dMsolUnit / (u.dKpcUnit/1e3)**3
        ax.plot(dTime[1:], np.log10(SFH), 'k.', alpha=alpha)

        SFR, ages = read_SFH_from_snap(folder)
        ax.plot(ages, np.log10(SFR/ (u.dKpcUnit/1e3)**3), 'r-', alpha=alpha)

        SFR, ages = read_SFR_from_mult_snaps(folder)
        ax.plot(ages, np.log10(SFR), 'ro', alpha=alpha)
        print(SFR)


        alpha = alpha*0.5


    # Plot of the Madau & Dickinson 2014 SFRD from the SIMBA paper
    MD_time, MD_SFRD = np.loadtxt('/'.join(folders[0].split('/')[:-1])+'/MadauDickinson14_SFRD.txt', unpack=True)
    ax.plot(MD_time, MD_SFRD, 'k', label=r'Madau \& Dickinson 2014')
    ax.plot([],[], 'k.', label='From finelog')
    ax.plot([],[], 'r-', label='From stellar ages')
    ax.plot([],[], 'ro', label='From SFR in snapshots')
    ax.legend()
    ax.set_xlabel('Time [Gyr]')
    ax.set_ylabel(r'SFR [M$_\odot$ yr$^{-1}$]')
    ax.set_ylim(-3, -0.5)

    plt.tight_layout()
    if path is not None:
        f.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys

    if sys.argv[-1][-3:] == 'png':
        run(sys.argv[1:-1], sys.argv[-1])
    else:
        run(sys.argv[1:])
