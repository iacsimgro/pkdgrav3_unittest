#!/usr/bin/env python3
import argparse
import pkdtest, paralleltest

def list_runs(data_dir):
    import subprocess
    from pathlib import Path
    # Get all the files *not* in the git repo
    process = subprocess.Popen(['git', 'ls-files', '-o'], stdout=subprocess.PIPE)
    out = process.communicate()[0].decode().split('\n')

    # At least the  'log' files should be present
    identifiers = []
    for o in out:
        if 'log_' in o:
            ident = o.split('_')[-1]
            if ident not in identifiers:
                identifiers.append(ident)

    # Now get the total memory and testcases
    cases = {ident:[] for ident in identifiers}
    for o in out:
        if 'log_' in o:
            ident = o.split('_')[-1]
            cases[ident].append(o.split('/')[-2])

    memory = {ident:0 for ident in identifiers}
    for o in out:
        for ident in identifiers:
            if ident in o:
                memory[ident]+=Path(o).stat().st_size

    # Convert to GB
    for ident in identifiers:
        memory[ident] *= 1e-9

    # Nice print
    for ident in identifiers:
        print ('IDENTIFIER: ', ident)
        print ('\t Total memory used: %.2f GB'%memory[ident])
        print ('\t Testcases: ', cases[ident])

    exit(0)

def remove_runs(data_dir, identifier):
    import subprocess
    # Get all the files *not* in the git repo
    process = subprocess.Popen(['git', 'ls-files', '-o'], stdout=subprocess.PIPE)
    out = process.communicate()[0].decode().split('\n')

    # Look for all the file matching the specifier
    to_delete = []
    for o in out:
        if identifier in o and not '.py' in o:
            to_delete.append(o)
            print(o)

    if len(to_delete) > 0:
        print("Are you sure that you want to delete all these files? (write 'yes!' if you are are)")
        decision = input()
        if decision == 'yes!':
            print("Erasing...")
            for d in to_delete:
                os.remove(d)
            print("Removing empty directories...")
            process = subprocess.Popen(['find', data_dir, '-type', 'd', '-empty'], stdout=subprocess.PIPE)
            out = process.communicate()[0].decode().split('\n')
            for o in out:
                # Do NOT erase the .git folder stuff!
                if '.git' not in o and o != '':
                    print (o)
                    os.rmdir(o)
            print("Done!")
            exit(0)
        else:
            print("No problem, see ya!")
            exit(0)
    else:
        print("Nothing to delete here...")
        exit(0)

if __name__ == '__main__':
    suite = pkdtest.PKDGRAV3TestSuite()

    import pathlib, inspect, glob
    import importlib.util
    import sys, os
    # Look for all the test definition files in this directory
    basepath = pathlib.Path(__file__).parent.resolve()
    # pathlib.glob does *not* follow symlinks, but glob.glob does...
    testPaths = glob.glob(str(basepath)+'/**/test_*.py', recursive=True)
    for path in testPaths:
        # Load the module and look for classes which have some 'test_*' method
        modulename = str(pathlib.Path(path).relative_to(basepath)).replace('/','.')[:-3]
        spec = importlib.util.spec_from_file_location(modulename, path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        # Add module to sys for pickle to work
        sys.modules[modulename] = module
        for name in dir(module):
            obj = getattr(module, name)
            if inspect.isclass(obj):
                for fname in obj.__dict__:
                    item = getattr(obj, fname)
                    if 'test_' in fname and inspect.isfunction(item):
                        if fname != 'test_dummy':
                            newtest = obj(methodName=fname)
                            suite.listTest(newtest)


    all_cases = suite.getTestNames()
    all_cases.append('ALL')

    parser = argparse.ArgumentParser()

    parser.add_argument('-i','--identifier', default='test', type=str, required=True, help='Identifier string for the current tests')
    parser.add_argument('-p','--pkdgrav-dir', default='', type=str, required=True, help='Base directory for PKDGRAV3')
    parser.add_argument('-d','--data-dir', default='', type=str, required=True, help='Unittest directory, usually the PWD')
    parser.add_argument('-c','--cpus', default=1, type=int, help='Use N cpus in shared memory configuration for the tests')
    parser.add_argument('-j','--jobs', default=1, type=int, help='Run N test at once, each one using --cpus cores')
    parser.add_argument('-a','--extra-args', default='', type=str, help='Extra command-line arguments for PKDGRAV3')
    parser.add_argument('-t','--cooling-dir', default='', type=str, help='Directory where the Wiersma cooling tables are stored')
    parser.add_argument('-T','--grackle-table', default='', type=str, help='GRACKLE cooling table path')
    parser.add_argument('-g','--grackle-dir', default='', type=str, help='GRACKLE root directory')
    parser.add_argument('-e','--stev-dir', default='', type=str, help='Path to the stellar evolution tables root directory')
    parser.add_argument('-C','--cc', default='mpicc', type=str, help='Path to C compiler')
    parser.add_argument('-X','--cxx', default='mpicxx', type=str, help='Path to C++ compiler')
    parser.add_argument('-F','--fc', default='mpifort', type=str, help='Path to FORTRAN compiler')
    parser.add_argument('-f','--fftw', default='', type=str, help='Path to the FFTW install folder')
    parser.add_argument('--cmake-args', default='', type=str, help='Extra parameters for cmake')
    parser.add_argument('--force-configure', action='store_true', help='Force compilation from scratch. Useful when changing compilers')
    parser.add_argument('--dry-run', action='store_true', help='Run everything but PKDGRAV3. Prints the command to be executed for debug')
    parser.add_argument('--plot-only', action='store_true', help='Only make the figures, assuming that the test were previously run')
    parser.add_argument('--remove', action='store_true', help='Remove all the data of the given identifier')
    parser.add_argument('--list', action='store_true', help='List all the present runs and memory usage')
    parser.add_argument('--report', action='store_true', help='Generate a HTML report of the results')
    parser.add_argument('--hydro', action='store_true', help='Run hydro-only testcases')
    parser.add_argument('--mfm', action='store_true', help='Run exclusively using the MFM scheme')
    parser.add_argument('--mfv', action='store_true', help='Run exclusively using the MFV scheme')
    parser.add_argument('cases', nargs='*', choices=all_cases)

    args = parser.parse_args()

    if args.remove:
        remove_runs(args.data_dir, args.identifier)

    if args.list:
        list_runs(args.data_dir)

    if 'ALL' in args.cases:
        cases = suite.getTestNames()
    else:
        cases = args.cases

    os.environ['MATPLOTLIBRC'] = args.data_dir

    suite.configure_all(cases=cases,                                        \
                        identifier=args.identifier,                         \
                        threads=args.cpus,                                  \
                        force_configure=args.force_configure,               \
                        testdir=args.data_dir,                              \
                        coolingdir=args.cooling_dir,                        \
                        grackle_dir=args.grackle_dir,                       \
                        grackle_table=args.grackle_table,                   \
                        stev_dir=args.stev_dir,                             \
                        pkdgrav3_dir=args.pkdgrav_dir,                      \
                        dry_run=args.dry_run,                               \
                        plot_only=args.plot_only,                           \
                        mfm=args.mfm,                                       \
                        mfv=args.mfv,                                       \
                        cc=args.cc,                                         \
                        cxx=args.cxx,                                       \
                        fc=args.fc,                                         \
                        cmake_args=args.cmake_args,                         \
                        fftw=args.fftw,                                     \
                        extra_args=args.extra_args)

    runner = paralleltest.ParallelTextTestRunner(verbosity=3, nworkers=args.jobs)
    runner.run(suite)

    if args.report:
        # We add the dummy BH test accumulator which includes the HTML data
        from BlackholeTests.test_blackholes import BlackholeTestsDummy
        BH_dummy = BlackholeTestsDummy(methodName='test_dummy')
        suite.listTest(BH_dummy)
        suite.generateReport()
