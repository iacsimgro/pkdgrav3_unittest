def run(folder, max_snap=-1, path=None):
    import h5py
    import numpy as np
    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import glob



    filenames = sorted(glob.glob(folder+'/*.0'))[:max_snap]

    n_files = len(filenames)

    snap0 = h5py.File(filenames[0], 'r')

    n_gas = snap0['Header'].attrs['NumPart_Total'][0]

    """
    IDs = snap0['PartType0/ParticleIDs'][...]

    IDs_sorted = np.argsort(IDs)

    plt.plot(IDs[IDs_sorted])
    plt.show()
    exit()
    """

    xs = np.zeros( (n_files, n_gas) )
    ys = np.zeros( (n_files, n_gas) )
    zs = np.zeros( (n_files, n_gas) )

    # We will also measure the number of crossing particles, defined as those with:
    # 1. (x<0 & vx<0) | (x>0 & vx>0)
    # 2. sign(x(z=100)) != sign(x(z))

    n_cross_vx = np.zeros(n_files)
    n_cross_x  = np.zeros(n_files)
    n_cross_both  = np.zeros(n_files)

    from pykdgrav3_utils import units
    u = units.units(snap0['Units'].attrs['MsolUnit'],snap0['Units'].attrs['KpcUnit'], verbose=True)

    x0 = np.zeros(n_gas)
    vx0 = np.zeros(n_gas)
    vx = np.zeros(n_gas)
    IDs = snap0['PartType0/ParticleIDs'][...]
    IDs_sorted = np.argsort(IDs)
    x0[IDs] = snap0['PartType0/Coordinates'][:,0] * u.dKpcUnit /1e3
    vx0[IDs] = snap0['PartType0/Velocities'][:,0] * u.dKmPerSecUnit

    t = np.zeros( (n_files)  )

    snap0.close()

    for i in range(n_files):
        snap = h5py.File(filenames[i], 'r')

        IDs = snap['PartType0/ParticleIDs'][...]
        IDs_sorted = np.argsort(IDs)
        xs[i,IDs] = snap['PartType0/Coordinates'][:,0] * u.dKpcUnit /1e3
        ys[i,IDs] = snap['PartType0/Coordinates'][:,1] * u.dKpcUnit /1e3
        zs[i,IDs] = snap['PartType0/Coordinates'][:,2] * u.dKpcUnit /1e3
        vx[IDs] = snap['PartType0/Velocities'][:,0] * u.dKmPerSecUnit /1e3

        n_cross_vx[i] = np.sum( np.logical_and(xs[i]<0,vx<0) | np.logical_and(xs[i]>0,vx>0) )
        n_cross_x[i] = np.sum( np.sign(x0) != np.sign(xs[i]) )
        n_cross_both[i] = np.sum( np.logical_and(np.logical_and(xs[i]<0,vx<0) | np.logical_and(xs[i]>0,vx>0),  np.sign(x0) != np.sign(xs[i]) )   )


        t[i] = snap['Header'].attrs['Time'] * u.dSecUnit/(3600*24*365) / 1e9 # Gyr



        snap.close()

    xs = xs.T
    ys = ys.T
    zs = zs.T


    subset = np.arange(0, n_gas, 1000)
    subset = np.random.randint(0, n_gas, size=20)
    #subset = np.arange(0, n_gas)

    x_sample = np.linspace(-60, 60, 32)

    subset = np.array([ np.abs(xs[:,0]-x_s).argmin() for x_s in x_sample  ])


    f, axs = plt.subplots(2,1, figsize=(5,8), sharex=True)

    for i in subset:
        axs[0].plot(t, xs[i],alpha=.5)

    axs[0].axhline(0, c='k', ls=':', label='Caustic position')
    #plt.axvline(0.084, c='k', ls='-', label='Caustic formation')

    axs[1].set_xlabel('Time [Gyr]')
    axs[0].set_ylabel('x(t) [Mpc]')

    axs[0].legend()

    #plt.xlim(1e-2, 1e1)
    axs[0].set_ylim(-10, 10)

    n_cross_x /= n_gas
    n_cross_vx /= n_gas
    n_cross_both /= n_gas

    axs[1].plot(t, n_cross_x, 'k--', label=r'sign[x(z=100)] != sign[x(z)]')
    axs[1].plot(t, n_cross_vx, 'k:', label=r'$(x<0 \& v<0) | (x>0 \& v>0)$')
    axs[1].plot(t, n_cross_both, 'k-', label=r'Both')
    axs[1].set_ylabel("Fraction of 'crossing' particles")

    axs[1].legend()

    plt.tight_layout()
    if path is not None:
        f.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], path=sys.argv[2])
    else:
        run(sys.argv[1])
