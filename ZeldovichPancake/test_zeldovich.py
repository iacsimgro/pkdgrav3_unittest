from pkdtest import PKDGRAV3TestCase


class ZeldovichTest(PKDGRAV3TestCase):
    title = "Zeldovich pancake"
    description = ("This cosmological test follows the "
                    "collapse of one mode. "
                    "This produces a pathological configuration "
                    "in which the particle distribution is "
                    "compressed only in one of the three dimensions. "
                    "\n"
                    "In this collapsed region a high-temperature "
                    "region is formed. "
                    "Outside, the temperature should be very low. "
                    "This test illustrates the need for a entropy switch ")
    plots = [ {"file": "z0_MFM",
              "caption": "Zeldovich pancake at z=0 with MFM"},
              {"file": "z0_MFM_entropy",
              "caption": "Zeldovich pancake at z=0 with MFM, with the entropy switch"},
              {"file": "z0_MFV",
              "caption": "Zeldovich pancake at z=0 with MFV"},
              {"file": "z0_MFV_entropy",
              "caption": "Zeldovich pancake at z=0 with MFV, with the entropy switch"},
              {"file": "trajectory_MFM",
              "caption": "Particle trajectories and crossing fraction for MFM"},
              {"file": "trajectory_MFV",
              "caption": "Particle trajectories and crossing fraction for MFV"}]

    schemes = ["MFM", "MFV"]
    __name__ = "Zeldovich"
    folder_name = "ZeldovichPancake"


    def generateIC(self):
        import numpy as np
        import h5py
        from pykdgrav3_utils import units

        h = 0.5 # Bryan et al 1995
        L = 64e3/h # kpc
        H0_phys = 100*h # Mpc / km/s

        # We compute the mass for having rho_c=1 in the domain
        M = L**3 * 8.*np.pi /3

        MSOLG = 1.98847e33
        GCGS = 6.67408e-8
        KPCCM =  3.085678e21

        M = 200 * 8.*np.pi * L**3 / 3e6 * 1e10 * KPCCM / MSOLG / GCGS

        # We want to set M such that H = 100 km/s/Mpc, i.e.:
        #  np.sqrt(8.*np.pi/3.) * u.dKmPerSec / (u.dKpcUnit/1e3) = 100.
        # np.sqrt(8.*np.pi/3.) * np.sqrt(self.GCGS*self.dMsolUnit*self.MSOLG/(self.dKpcUnit*self.KPCCM))/1e5 / (u.dKpcUnit/1e3) = 100
        # 8.*np.pi/3. * self.GCGS*self.dMsolUnit*self.MSOLG/(self.dKpcUnit*self.KPCCM)/1e10 / (u.dKpcUnit/1e3)**2 = 100**2

        M = H0_phys**2 * (L/1e3)**2 * (L*KPCCM) * 1e10 / (8.*np.pi/3. * GCGS * MSOLG)


        #M = 1e10

        z_i = 100.
        z_c = 1.
        a_i = 1./(z_i+1)


        Ti = 1e2 #K

        k = 2.*np.pi # in code units
        phi = 0. #np.pi/4.+0.1

        u = units.units(M,L, verbose=True)

        print (u.dMsolUnit*u.MSOLG, u.dKpcUnit*u.KPCCM)

        H0 = np.sqrt(8.*np.pi/3.)  #70./ u.dKmPerSecUnit * ( u.dKpcUnit / 1e3)
        rho_crit = 3.*H0*H0/(8.*np.pi)
        print ("H0 ", H0, np.sqrt(8.*np.pi/3.))
        print ("rho_crit ", rho_crit)

        print (H0 * u.dKmPerSecUnit / ( u.dKpcUnit / 1e3))


        """
        ## Standard lattice configuration
        x = np.linspace(-1/2., 1/2., num=n, endpoint=False)
        mesh = np.meshgrid(x,x,x)

        x = mesh[0].flatten()
        y = mesh[1].flatten()
        z = mesh[2].flatten()

        del mesh
        """

        ## IC generated with WVTICs
        import pynbody
        glass_snap = pynbody.load(self.pkdgrav3_test_dir+'/IC_WVT_IC')

        N = glass_snap['x'].shape[0]


        x = np.array(glass_snap['x'])- 0.5
        y = np.array(glass_snap['y'])- 0.5
        z = np.array(glass_snap['z'])- 0.5

        # Eqs 40, 41, 42 Hopkins 2015
        rho = rho_crit/( 1 - (1+z_c)/(1+z_i)*np.cos(k*x+phi)  )
        vx = -H0 *(1+z_c)/np.sqrt(1+z_i)*np.sin(k*x+phi)/k
        T = Ti*(  rho/rho_crit  )**(2./3.)

        # Eq 39
        x = x - (1+z_c)/(1+z_i) * np.sin(k*x+phi)/k

        pos = np.stack(  (x, y, z), axis=-1)
        #print (pos.shape)
        vel = np.stack(  (vx, np.zeros_like(vx), np.zeros_like(vx)), axis=-1) / a_i


        order = np.arange(N, dtype=np.uint64)
        IC_file = h5py.File(self.input_file, 'w')

        # We set the initial time
        IC_file.create_group('Header')
        IC_file.create_group('Units')
        IC_file.create_group('Cosmology')
        IC_file.create_group('Parameters')
        IC_file['Header'].attrs['Redshift'] = z_i
        IC_file['Header'].attrs['NumPart_Total'] = np.array([N,0,0,0,0])
        IC_file['Header'].attrs['NumPart_ThisFile'] = np.array([N,0,0,0,0])

        sphclassArr = np.array([(0, 0., 0.0, 0)], \
              dtype={'names':['class','mass','soft','start'], 'formats':['u1','<f8','<f8','<u8'], 'offsets':[0,16,24,8], 'itemsize':40})
        sphclassArr['mass'] = rho_crit/N
        sphclassArr['soft'] = 0. #40./64000. #(3 * 1.**3 / (4.*np.pi*(2.*N)) )**(1./3.) /30.
        sphclassArr['class'] = 0

        IC_file.create_group('PartType0')
        dset = IC_file['PartType0'].create_dataset("classes", data=sphclassArr)
        dset = IC_file['PartType0'].create_dataset("class", data=np.zeros(N, dtype="|u1") )
        dset = IC_file['PartType0'].create_dataset("Coordinates", data=pos, dtype="<f8")
        dset = IC_file['PartType0'].create_dataset("Velocities", data=vel)
        dset = IC_file['PartType0'].create_dataset("Temperature", data=T, dtype="<f4")
        dset = IC_file['PartType0'].create_dataset("Density", data=rho, dtype="<f4")
        dset = IC_file['PartType0'].create_dataset("order", data=order, dtype="<u8")


    def test_zeldovich(self,result=None, *args, **kwargs):
        self.param_file = self.pkdgrav3_test_dir + '/zeldovich.par'
        self.input_file = self.pkdgrav3_test_dir + '/IC.hdf5'
        self.generateIC()
        for scheme in self.schemes:
            for entropy in [True, False]:
                self.compile_flags = {'ENTROPY_SWITCH':entropy}
                if entropy:
                    entropy_str = '_entropy'
                else:
                    entropy_str = ''

                self.set_scheme(scheme)
                self.compile()

                self.output_dir = self.pkdgrav3_test_dir + '/snaps_%s%s_%s'%(scheme, entropy_str, self.identifier)
                self.output_file = self.output_dir + '/zeldovich'
                self.run_pkdgrav3()

                self.plot(scheme, entropy_str)



    def plot(self, scheme, entropy_str):
        from ZeldovichPancake.plot import run

        run(self.output_file+'.00100.0', path=self.pkdgrav3_test_dir + '/z0_%s%s_%s.png'%(scheme, entropy_str, self.identifier))

        from ZeldovichPancake.particleTrajectories import run
        run(self.output_dir, path=self.pkdgrav3_test_dir + '/trajectory_%s%s_%s.png'%(scheme, entropy_str, self.identifier))

