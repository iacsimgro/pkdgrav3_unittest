def run(snapname, path=None):
    import numpy as np
    import h5py

    import matplotlib
    import os
    if os.environ.get('REMOTE_PLOT')!=None:
        print ("Plotting in remote enviroment")
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker



    ## Exact solution at z=0 (from gizmo http://www.tapir.caltech.edu/~phopkins/sims/)
    x_z0, log_rho_z0, log_T_z0, vx_z0 = np.loadtxt('/'.join(snapname.split('/')[:-2])+'/zeldovich_exact.txt', unpack=True)

    x_z0 /= 0.5
    rho_z0 = 10**log_rho_z0
    T_z0 = 10**log_T_z0


    f, axss = plt.subplots(3,2,figsize=(16,10.0))

    axs = axss.T[0]
    axs2 = axss.T[1]

    N = np.array([32]) #,512,1024, 2048, 4096])

    A = 1e-3
    gamma = 5./3.
    T0 = 1e-2
    k = 2.*np.pi


    var = 'rho'


    i=0

    snap = h5py.File(snapname, 'r')
    from pykdgrav3_utils import units
    z = snap['Header'].attrs['Redshift']
    a = 1./(z+1)
    f.suptitle('z=%.2f'%z)

    #u = units(1.,1., verbose=True)
    u = units.units(snap['Units'].attrs['MsolUnit'],snap['Units'].attrs['KpcUnit'])



    x = snap['PartType0/Coordinates'][:,0] * u.dKpcUnit / 1e3 # Mpc
    y = snap['PartType0/Coordinates'][:,1] * u.dKpcUnit / 1e3
    zz = snap['PartType0/Coordinates'][:,2] * u.dKpcUnit / 1e3
    rho = snap['PartType0/Density'][...]
    mass = snap['PartType0/Masses'][...]
    vx = snap['PartType0/Velocities'][:,0] * a * u.dKmPerSecUnit
    vy = snap['PartType0/Velocities'][:,1] * a * u.dKmPerSecUnit
    vz = snap['PartType0/Velocities'][:,2] * a * u.dKmPerSecUnit
    smooth = snap['PartType0/SmoothingLength'][...] * u.dKpcUnit
    T = np.ones_like(x)
    T = snap['PartType0/InternalEnergy'][...]/u.dGasConst*(5./3.-1)

    # Easier to compare directly in code units
    Ekin = 0.5*(mass)*(vx/u.dKmPerSecUnit)**2
    Uint = snap['PartType0/InternalEnergy'][...]* mass

    E_ratio = Uint/(Uint+Ekin)

    index = np.argmax(vx)
    print (vx[index], vy[index], vz[index], Ekin[index], Uint[index])
    print (x[index], y[index], zz[index], rho[index])
    print ("Mass ", np.sum(mass))

    alpha = 0.1
    axs[0].plot(x, rho, 'k.', alpha=alpha)
    axs[1].plot(x, vx, 'k.', alpha=alpha)
    axs[2].plot(x, T, 'k.', alpha=alpha)

    axs2[1].plot(x, E_ratio, 'k.', alpha=alpha)

    #axs2[2].hist2d(y, zz, bins=20, weights=vz)
    #axs2[2].set_aspect('equal')


    k = 2.*np.pi
    phi = 0.0 #np.pi/4.+0.1
    z_c = 1.
    Ti = 1e2
    z_i = 100

    q_t = np.linspace(-0.5,0.5, 200)
    x_t = q_t - (1+z_c)/(1+z) * np.sin(k*q_t+phi)/k

    H0 = np.sqrt(8.*np.pi/3.) #70./ u.dKmPerSecUnit * ( u.dKpcUnit / 1e3)
    rho_crit = 3.*H0*H0/(8.*np.pi)
    if z>z_c:
        rho_t = rho_crit/( 1 - (1+z_c)/(1+z)*np.cos(k*q_t+phi)  )
        grad_rho_t = -rho_crit*(1+z_c)/(1+z)*np.sin(k*q_t+phi)*k /( 1 - (1+z_c)/(1+z)*np.cos(k*q_t+phi)  )**2
        vx_t = -H0 *(1+z_c)/np.sqrt(1+z)*np.sin(k*q_t+phi)/k  * u.dKmPerSecUnit
        grad_vx_t = np.gradient(vx_t/u.dKmPerSecUnit, x_t)
        T_t = Ti*( rho_t/rho_crit  )**(2./3.)
        T_t = Ti*( ((1+z)/(1+z_i))**3 * rho_t/rho_crit  )**(2./3.)

        Ekin_t = 0.5*(vx_t/u.dKmPerSecUnit)**2
        Uint_t = T_t * u.dGasConst/(5./3. -1.)
        E_ratio_t = Uint_t/(Ekin_t+Uint_t)

        x_t *= u.dKpcUnit / 1e3

        axs[0].plot(x_t, rho_t, 'r-')
        axs[1].plot(x_t, vx_t, 'r-')
        axs[2].plot(x_t, T_t, 'r-')

        axs2[1].plot(x_t, E_ratio_t, 'r-')
        axs2[2].plot(x_t, grad_vx_t, 'r-')
        #axs2[2].plot(x_t, grad_rho_t, 'r-')

    else:

        axs[0].plot(x_z0, rho_z0, 'r-')
        axs[1].plot(x_z0, vx_z0, 'r-')
        axs[2].plot(x_z0, T_z0, 'r-')



    #x = np.linspace(-0.5,0.5,100)
    #rho_an = A*np.cos(2.*np.pi*(x) + np.pi/2. - theta[-1])
    #ax.plot(x, rho_an, 'k--', alpha=0.9)

    #plt.legend(loc=0, fancybox=True,fontsize=14, title=r'$N_\mathrm{NGB}$=32', title_fontsize=14)

    #ax.set_xticks([-0.5,-0.25,0,0.25,0.5])
    #ax.set_ylabel(r'$(\rho_i-\bar{\rho})/\bar{\rho}$')
    axs[2].set_xlabel(r'x [Mpc]')
    axs2[2].set_xlabel(r'x [Mpc]')

    axs[0].semilogy()
    axs[0].set_ylabel(r'$\rho/\rho_0$')
    axs[1].set_ylabel(r'v$_x$ [km/s]')
    axs[2].set_ylabel(r'T [K]')
    axs[2].semilogy()
    axs[2].set_ylim(1e-14,1e10)

    axs2[0].set_ylabel(r'$h$ [kpc]')
    axs2[1].set_ylabel(r'U/(U+K)')
    axs2[1].semilogy()
    axs2[1].set_ylim(1e-21,1e3)


    axs2[0].plot(x, smooth, 'k.', alpha=alpha)
    axs2[0].axhline(0.00048*128e3, c='r', ls=':')


    plt.tight_layout()
    if path is not None:
        f.savefig(path)
    else:
        plt.show()

if __name__=='__main__':
    import sys
    if len(sys.argv) > 2:
        run(sys.argv[1], path=sys.argv[2])
    else:
        run(sys.argv[1])
